## -*- coding: utf-8 -*-

## João Martins

#################################################

print('\n')

#################################################

import statistics
from scipy import stats
import numpy as np

a = [4, 36, 45, 50, 75]
b = [1, 2, 2, 3, 4, 7, 9]
c = [6, 3, 9, 6, 6, 5, 9, 9, 3, 1]

print(statistics.mean(a))
print(np.mean(b))
print(np.mean(c))


print('\n')


print(statistics.mode(a))
print(stats.mode(b))
print(stats.mode(c))


print('\n')


print(statistics.median(a))
print(statistics.median(b))
print(np.median(c))


#################################################

print('\n')

#################################################

import statistics
import numpy as np

a = [9, 5, 2, 4, 12, 7, 8, 11, 9, 3, 7, 4, 12, 5, 4, 10, 9, 6, 9, 4]
b = [9, 5, 2, 4, 12, 7]

print(np.mean(a))
print(np.mean(b))


print('\n')


print(np.std(a))
print(statistics.pstdev(a))
print(statistics.stdev(a))


print('\n')


print(np.std(b))
print(statistics.stdev(b))


print('\n')


print(np.var(a))
print(statistics.pvariance(a))
print(statistics.variance(a))


print('\n')


print(np.var(b))
print(statistics.variance(b))


#################################################

print('\n')

#################################################


import matplotlib.pyplot as plt


x = [0, 0.5, 2]
y = [0, 1, 4]

plt.plot(x, y, 'go--')
plt.show()


#################################################

print('\n')

#################################################


import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5]
y= [2, 4, 6, 8, 10]


plt.plot(x, y, 'r--P', label = 'y = 2x')
plt.legend()
plt.title('test')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

print('\n')


x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]

plt.plot(x1, y1, 'b:D', label = 'y = x^2')
plt.legend()
plt.show()


print('\n')


plt.plot(x, y, 'r--P', label = 'y = 2x')
plt.plot(x1, y1, 'b:D', label = 'y = x^2')
plt.show()


#################################################

print('\n')

#################################################


import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5]
y= [2, 4, 6, 8, 10]

x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]


plt.subplot(211)
plt.plot(x, y, 'r--P', label = 'y = 2x')
plt.legend()



plt.subplot(212)
plt.plot(x1, y1, 'b:D', label = 'y = x^2')
plt.legend()

plt.savefig('C:\\Users\\João\\Desktop\\upskill\\plot.png')


#################################################

print('\n')

#################################################

import numpy as np
import matplotlib.pyplot as plt

x = np.arange(14)
y = np.sin(x / 2)

plt.step(x, y + 2, label = 'pre (default)')
plt.plot(x, y + 2, 'o--', color = 'grey', alpha = 0.3)

plt.step(x, y + 1, where = 'mid', label = 'mid')
plt.plot(x, y + 1, 'o--', color = 'grey', alpha = 0.3)

plt.step(x, y, where = 'post', label = 'post')
plt.plot(x, y, 'o--', color = 'grey', alpha = 0.3)


plt.grid(axis = 'x', color = '0.95')
plt.legend(title = 'Parameter where:')
plt.title('plt.step(where = ...)')
plt.show()

#################################################

print('\n')

#################################################

import numpy as np
import matplotlib.pyplot as plt

x1 = np.linspace(0.0, 5.0)
x2 = np.linspace(0.0, 2.0)

y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)
y2 = np.cos(2 * np.pi * x2)

fig, (ax1, ax2) = plt.subplots(2, 1)
fig.suptitle('A tale of 2 subplots')

ax1.plot(x1, y1, 'o-')
ax1.set_ylabel('Damped oscillation')

ax2.plot(x2, y2, '.-')
ax2.set_xlabel('time (s)')
ax2.set_ylabel('Undamped')

plt.show()



#################################################

print('\n')

#################################################

data_x = np.linspace(0.0, 5.0)
data_y = np.cos(2 * np.pi * x1) * np.exp(-x1)

plt.figure(facecolor = 'lightgrey')
plt.subplot(2, 2, 1)
plt.plot(data_x, data_y, 'r-')
plt.subplot(2, 2, 2)
plt.plot(data_x, data_y, 'b-')
plt.subplot(2, 2, 4)
plt.plot(data_x, data_y, 'g-')


data_x = np.linspace(0.0, 2.0)
data_y = np.cos(2 * np.pi * x2)
fig, ax = plt.subplots(2, 2)
fig.set_facecolor('lightgrey')
ax[0, 0].plot(data_x, data_y, 'r-')
ax[1, 0].plot(data_x, data_y, 'b-')
fig.delaxes(ax[1, 0])
ax[1, 1].plot(data_x, data_y, 'g-')
ax[0, 1].plot(data_x, data_y, 'b-')

#################################################

print('\n')

#################################################

import matplotlib.pyplot as plt
import numpy as np

x = np.arange(0.0, 2.0, 0.01)
y = 1 + np.sin(2 * np.pi * x)
fig, ax = plt.subplots()
ax.plot(x, y, label = 'sin($\phi$)')

ax.set(xlabel = 'time(s)', ylabel = 'sin(2\u03C0s)', title = 'Python Course')
ax.grid()
ax.legend()
fig.savefig("test.png")
plt.show()


#################################################

print('\n')

#################################################


import numpy as np
import matplotlib.pyplot as plt

data = np.array([-10, -5, -2, -1, 0, 1, 2, 3, 4])

q1 = np.quantile(data, .25)
print(q1)

print('\n')

q2 = np.quantile(data, .50)
print(q2)

print('\n')

q3 = np.quantile(data, .75)
print(q3)

print('\n')

iqr = q3 - q1
print(iqr)

print('\n')

lv = q1 - 1.5 * iqr
print(lv)

print('\n')

hv = q3 + 1.5 * iqr
print(iqr)

plt.boxplot(data)
plt.show()

plt.boxplot(data)
plt.grid()
plt.show()

#################################################

print('\n')

#################################################

import numpy as np
import matplotlib.pyplot as plt

class1 = np.array([60, 70, 80, 83, 85, 87, 88, 89, 90, 92, 94, 95, 97, 100, 110])

class2 = np.array([130, 143, 150, 158, 160, 170, 175, 182, 185, 188, 190, 200, 210, 280, 300])


plt.boxplot([class1, class2], patch_artist = True)
plt.show()

#################################################

print('\n')

#################################################

import matplotlib.pyplot as plt
import numpy as np

names = ['A', 'B', 'C', 'D', 'E']
values = [5, 10, 8, 3, 2]

plt.bar(names, values, color = 'green')
plt.xlabel('Names')
plt.ylabel('No. of student')
plt.show()


#################################################

print('\n')

#################################################

####  BAR GRAPH  ####

import numpy as np 
import matplotlib.pyplot as plt

x = np.array([-3, -2, -1, 0, 1, 2, 3])
y = np.array([9, 4, 1, 0, 1, 4, 9])

plt.scatter(x, y, c = 'r')
plt.show()

#################################################

print('\n')

#################################################

####  SCATTER GRAPH  ####

import numpy as np 
import matplotlib.pyplot as plt

data = np.array([3, 3, 5, 6, 7, 7, 8, 9, 9, 10, 10, 11, 12, 12, 14, 15, 16, 17, 18, 19, 19, 20])

plt.hist(data, bins = 4, edgecolor = 'black')
plt.xlabel('Value')
plt.ylabel('Frequency')
plt.show()


plt.hist(data, bins = 20, edgecolor = 'black')
plt.xlabel('Value')
plt.ylabel('Frequency')
plt.show()


#################################################

print('\n')

#################################################

####  HISTOGRAM  ####

import numpy as np 
import matplotlib.pyplot as plt
from scipy.stats import norm

x = 100 + 15 * np.random.randn(1000)

n, bins, _ = plt.hist(x, bins = 20, edgecolor = 'black', density = 1)
y = norm.pdf(bins, 100, 15)
plt.plot(bins, y, 'r')
plt.show()

#################################################

print('\n')

#################################################


####  PIE GRAPH  ####


import matplotlib.pyplot as plt

l = ['Python', 'Java', 'C++']
s = [200, 150, 100]
c = ['green', 'red', 'yellow']

plt.pie(s, labels = l,  colors = c)
plt.show()

plt.pie(s, labels = l, colors = c, autopct = '1%.3f%%')
plt.show()

plt.pie(s, labels = l, colors = c, startangle = 120)
plt.show()

plt.pie(s, labels = l, colors = c, explode = (0.1, 0, 0), shadow = True, autopct = '1%.3f%%')
plt.show()

#################################################

print('\n')

#################################################

####  BOX  ####

import pandas as pd

df = pd.DataFrame(np.random.rand(10, 5), columns = ['A', 'B', 'C', 'D', 'E'])
print(df)

df.plot.box(color = {'boxes' : 'Green', 'whiskers' : 'red', 'medians' : 'Blue', 'caps' : 'Gray'})

df.plot.box()



df.plot.box(vert = False, positions = [1, 4, 5, 6, 8])
plt.show()


####  BOXPLOT  ####

df = pd.DataFrame(np.random.rand(10, 5))
df.boxplot()



#################################################

print('\n')

#################################################

####  BAR/BARH GRAPH  ####

import pandas as pd

df = pd.DataFrame(np.random.rand(10, 4), columns = ['a', 'b', 'c', 'd'])
print(df)

df.plot.bar()

df.plot.bar(stacked = True)

df.plot.barh(stacked = True)


#################################################

print('\n')

#################################################

####  SCATTER GRAPH  ####


import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.rand(50, 4), columns = ['a', 'b', 'c', 'd'])
print(df.head())


k = df.plot.scatter(x = 'a', y = 'b', color = 'red', label = 'Group 1')
df.plot.scatter(x = 'c', y = 'd', color = 'blue', label = 'Group 2', ax = k)

df.plot.scatter(x = 'a', y = 'b', color = 'red', label = 'Group 1')

df.plot.scatter(x = 'c', y = 'd', color = 'blue', label = 'Group 2')


#################################################

print('\n')

#################################################

import pandas as pd
import numpy as np


df = pd.DataFrame(np.random.rand(50, 3), columns = ['a', 'b', 'c'])
df['c'] *= 200
df.sort_values(by = 'a', inplace = True)
df.head(7)


df.plot(kind = 'line', x = 'a', y = 'b', color = 'green')


#################################################

print('\n')

#################################################

import pandas as pd
import numpy as np

df = pd.DataFrame(np.random.rand(50, 3), columns = ['A', 'B', 'C'])
df.head(7)

df.plot(kind = 'area')

#################################################

print('\n')

#################################################

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

plt.style.use('ggplot')
df = pd.DataFrame(np.random.rand(50, 3), columns = ['a', 'b', 'c'])
df['c'] *= 200
df.head()

df.plot(kind = 'scatter', x = 'a', y = 'b', color = 'green')

#################################################

print('\n')

#################################################

import pandas as pd
import numpy as np

s = pd.Series(3 * np.random.rand(3), index = ['a', 'b', 'c'], name = 'series')

print(s)

s.plot.pie()
plt.show()

s.plot.pie(labels = ['AA', 'BB', 'CC'], colors = ['b', 'g', 'r'], fontsize = 20, figsize = (6, 6))


df = pd.DataFrame(3 * np.random.rand(4, 2), index = ['a', 'b', 'c', 'd'], columns = ['x', 'y'])

print(df)

df.plot.pie(subplots = True, figsize = (12, 8))

#################################################

print('\n')

#################################################


import numpy as np

x = np.array([1, 2, 3, 4])
y = np.array([7, 8])

a,b = np.meshgrid(x, y)

print(a)

print('\n')

print(b)

print('\n')

print(a.shape)

#################################################

print('\n')

#################################################

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

x = np.arange(-10, 10, 0.5)
y = np.arange(-10, 10, 0.5)
X, Y = np.meshgrid(x, y)
Z = X ** 2 + Y ** 2

fig = plt.figure(figsize = (5, 5))
ax = fig.gca(projection = '3d')

s = ax.plot_surface(X, Y, Z, cmap = plt.cm.rainbow)

cset = ax.contour(X, Y, Z, zdir = 'z', offset = 0, cmap = plt.cm.rainbow)

fig.colorbar(s, shrink = 0.5, aspect = 5)

ax.view_init(50, 35)
fig

#################################################

print('\n')

#################################################

import numpy as np
import matplotlib.pyplot as plt

x = np.arange(1, 11)
y = x.reshape(-1, 1)
h = x * y

cs = plt.contourf(h, levels = [10, 30, 40], colors = ['r', 'b'], extend = 'both')
cs.cmap.set_over('yellow')
cs.cmap.set_under('pink')
cs.changed()
plt.show()

plt.contour(Z)
plt.show()

plt.contourf(Z)








