# -*- coding: utf-8 -*-

############################################


##  start_time = int(round(time.time()1000))
##  end_time = int(round(time.time()1000)
##  print(end_time - start_time)




############################################

import csv as csv_reader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np



# class plotdata():
   
graf1 = 'C:\\Users\\João\\Desktop\\pythoncode\\PythonProject\\850566403_T_ONTIME.csv'
graf2 = 'C:\\Users\\João\\Desktop\\pythoncode\\PythonProject\\L_AIRLINE_ID.csv'
graf3 = 'C:\\Users\\João\\Desktop\\pythoncode\\PythonProject\\L_AIRPORT_ID.csv'

#####################################################################################################################    
    
def plot_grafs(graf1, graf2, graf3):
        
    d = {1 : 'janeiro', 2 : 'fevereiro', 3 : 'março', 4 : 'abril', 5 : 'maio',
         6 : 'junho', 7 : 'julho', 8 : 'agosto', 9 : 'setembro', 10 : 'outubro', 
         11 : 'novembro', 12 : 'dezembro'}

    fig, ax = plt.subplots(2, 2, figsize=(15, 13))
    
    # listNames, listDelays, title, ylabel, year, month = atraso_medio_companhia(graf1, graf2)

    
    #  graf atraso médio companhia
    
    listNames, listDelays, title, ylabel, year, month = atraso_medio_companhia(graf1, graf2)
    
    plt.subplot(2, 2, 1)
    plt.bar(list(listNames), list(listDelays)) 
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 60)
    # plt.legend()
    # plt.show()
    
    
    #figure title
    fig.suptitle(f'Atrasos à chegada na aviação comercial ({d.get(month)} de {year})', fontsize = 15)
    

      
    #  graf atraso médio aeroporto
    
    listNames, listDelays, title, ylabel = atraso_medio_aeroporto(graf1, graf3)
    
    plt.subplot(2, 2, 3)
    plt.bar(list(listNames), list(listDelays))
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 90)
    # plt.legend()
    # plt.show()
    

    #  graf rácio médio companhia   

    names, ratios, title, ylabel = racios_atrasos_companhia(graf1, graf2)
    
    plt.subplot(2, 2, 2)
    plt.bar(list(names), list(ratios)) 
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 0.6)
    # plt.legend()
    # plt.show()
    
    
    
    #  graf rácio médio aeroporto
    
    names, ratios, title, ylabel = racios_atrasos_aeroporto(graf1, graf3)
   
    plt.subplot(2, 2, 4)
    plt.bar(list(names), list(ratios))
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 0.9)
    # plt.legend()
    # plt.show()
    
#####################################################################################################################

def getDF_Atrasos(tId, tabE, tabN):
        
        df = pd.read_csv(tabE)
            
        df_nanFalse = df.iloc[:,:-1]
            
        df_nanFalse = df_nanFalse.replace(0.0, np.nan)
            
        df_nanFalse = df_nanFalse.dropna()
          

        dfByID = df_nanFalse.groupby([tId]).agg({tId : 'unique', 'ARR_DELAY_NEW' : 'mean'})                     
        
        dfByID = dfByID.nlargest(10, "ARR_DELAY_NEW", "last")
        
        return dfByID


#####################################################################################################################




#####################################################################################################################

def getDF_Racio(tId, tabE, tabN):
        
        df = pd.read_csv(tabE)
  
        df_nanFalse = df.iloc[:,:-1]
        
        df_nanFalse = df_nanFalse.dropna()
        
        
        nTotalV = df_nanFalse.groupby([tId]).agg({tId : 'unique'})
        nTotalV['TOTALV'] = df_nanFalse.groupby([tId]).agg({tId : 'count'})
        # print(nTotalV)

            
        df_nanFalse = df_nanFalse.replace(0.0, np.nan)
            
        df_nanFalse = df_nanFalse.dropna()
         
        
        nTotalV['TOTALVA'] = df_nanFalse.groupby([tId]).agg({tId : 'count'})
        

        return nTotalV

#####################################################################################################################




#####################################################################################################################

def get_list(l):
    
    
        ####  ID'S de atraso adicionados a uma lista
        for i in range(len(l)):
            l[i] = int(l[i])
           
        return (l)


#####################################################################################################################





#####################################################################################################################

def get_dict(tabelaNomesCompanhias):
    
    ####  Construção de dicionario com keys(Code(ex: 123))  values(Dest(ex:  US))            
            
    d = {}
            
    with open(tabelaNomesCompanhias, 'r') as file:
        csv_file = csv_reader.DictReader(file)
        for row in csv_file:
                    
            d[list(row.values())[0]] = list(row.values())[1]
      

    return d

#####################################################################################################################





#####################################################################################################################

def names_list(d, ids):
    names = []
    
    
    ####  lista com os nomes das companhias associadas aos ids de atraso
    for i in range(len(ids)):
        names.append(d.get(str(ids[i])))

            
    for i in range(len(ids)):
        names[i] = names[i][0:10]


    return names
#####################################################################################################################





#####################################################################################################################
def list_ids(d, ratio):
    ids = []
    
    
    ####  lista com os nomes das companhias associadas aos ids de atraso
                 
    for i in range(len(ratio)):
        ids.append(d.get(ratio[i]))


    return ids


#####################################################################################################################




#####################################################################################################################

def calc_ratio(l1, l2):
    
    ratio = []
    
    for i in range(len(l1)):
        ratio.append(l2[i] / l1[i])

    return ratio

#####################################################################################################################




#####################################################################################################################

def set_dict(ids, ratio):
    d = {}
    
    for i in range(len(ids)):
        d[ids[i]] = ratio[i]

    return(d)


#####################################################################################################################       




#####################################################################################################################

## Metodo -> 2   ##

def atraso_medio_companhia(tabelaEstatisticas, tabelaNomesCompanhias):
                
        df = pd.read_csv(tabelaEstatisticas)
        
        year = int(df.iloc[0]['YEAR'])
        
        month = int(df.iloc[0]['MONTH'])

    
        ## get dataframe by ids
            
        AirLineByID = getDF_Atrasos("AIRLINE_ID", tabelaEstatisticas, tabelaNomesCompanhias)

        
        ## get id and delays list
        
        ids = AirLineByID["AIRLINE_ID"].tolist()
        delays = AirLineByID["ARR_DELAY_NEW"].tolist()    

        ids = get_list(ids)
        # delays = get_list(delays)

        ####  Construção do dicionario com as keys(ex: 1234)  e values (ex: Columbia)
        d = get_dict(tabelaNomesCompanhias)
        # print(d)
        
        ## get list with names        
        companyNames = names_list(d, ids)    
        
            
        ### dados para plot
            
        title = 'Atraso médio por companhia (top 10)'
        ylabel = 'minutos'
        companyNames.reverse()
        delays.reverse()
            
        return(companyNames, delays, title, ylabel, year, month)          

    
#####################################################################################################################    



#####################################################################################################################
    
##  Metodo -> 3  ##
    
def racios_atrasos_companhia(tabelaEstatisticas, tabelaNomesCompanhias):
        
        
        nTotalV = getDF_Racio("AIRLINE_ID", tabelaEstatisticas, tabelaNomesCompanhias)

        nTotalV = nTotalV.nlargest(14, "TOTALV", "last")
        
        # print(nTotalV)
        
        ## VER AQUI JA RESULTA?!?!?!?!?!?!
        
        # nTotalV['RES'] = nTotalV['TOTALVA'] / nTotalV['TOTALV']
        
        # print(nTotalV)
        
        ids = nTotalV["AIRLINE_ID"].tolist()
        listaVoosTotal = nTotalV["TOTALV"].tolist()
        listaVoosTotalA = nTotalV["TOTALVA"].tolist()
       
        
        ## obter lista com ids
        ids = get_list(ids)
        # print(ids)
        
        
        ####  Construção do dicionario com as keys(ex: 1234)  e values (ex: Columbia)
        d_all = get_dict(tabelaNomesCompanhias)
       

        #### get list with names        
        names = names_list(d_all, ids)   
        
        
        listaVoosTotal.reverse()
        listaVoosTotalA.reverse()
        ids.reverse()
        names.reverse()


        ratio = calc_ratio(listaVoosTotal, listaVoosTotalA)

        
        d_id_ratio = set_dict(ids, ratio)

        ## reverter o dicionário (key passa a values e vice-versa)
        d_id_ratio = {v: k for k, v in d_id_ratio.items()}


        ## obter lista com os 10 valores maiores para o ratio 
        ratio = sorted(ratio)[-10:]
        
        
        
        ids_list = list_ids(d_id_ratio, ratio)
        # print(ids_list)
        
        
        
        names = names_list(d_all, ids_list)

        
        title = 'Voos atrasados por companhia (top 10)'  
        ylabel = 'rácio'          
        return(names, ratio, title, ylabel)
    
    
    
#####################################################################################################################    
    




#####################################################################################################################    
    ##  Metodo -> 4  ##
    
def atraso_medio_aeroporto (tabela_estatisticas, tabela_nomes_aeroportos):

        ## get dataframes by ids        
        AirportByID = getDF_Atrasos("DEST_AIRPORT_ID", tabela_estatisticas, tabela_nomes_aeroportos)
       
      
        ## get id and delays list
        destIds = AirportByID["DEST_AIRPORT_ID"].tolist()     
        destDelays = AirportByID["ARR_DELAY_NEW"].tolist()
        
        
        destIds = get_list(destIds)
        # destDelays = get_list(destDelays) 

        ####  Construção do dicionario com as keys(ex: 1234)  e values (ex: Columbia)
        d = get_dict(tabela_nomes_aeroportos)
        

        #### get list with names        
        destNames = names_list(d, destIds)   
                   

        #### dados para plot          
        destNames.reverse()
        destDelays.reverse()
            
            
        title = 'Atraso médio por aeroporto (top 10)'
        ylabel = 'minutos'             
        return(destNames, destDelays, title, ylabel)
            
        
# atraso_medio_aeroporto (graf1, graf3)
    
#####################################################################################################################  



#####################################################################################################################

    ##  Metodo -> 5  ##
    
def racios_atrasos_aeroporto(tabela_estatisticas, tabela_nomes_aeroportos):


        nTotalV = getDF_Racio("DEST_AIRPORT_ID", tabela_estatisticas, tabela_nomes_aeroportos)

        
        ids = nTotalV["DEST_AIRPORT_ID"].tolist()
        listaVoosTotal = nTotalV["TOTALV"].tolist()
        listaVoosTotalA = nTotalV["TOTALVA"].tolist()
       
        
        ## obter lista com ids
        ids = get_list(ids)
        # print(ids)
        
        
        ####  Construção do dicionario com as keys(ex: 1234)  e values (ex: Columbia)
        d_all = get_dict(tabela_nomes_aeroportos)
       

        #### get list with names        
        names = names_list(d_all, ids)   
        
        
        listaVoosTotal.reverse()
        listaVoosTotalA.reverse()
        ids.reverse()
        names.reverse()


        ratio = calc_ratio(listaVoosTotal, listaVoosTotalA)

        
        d_id_ratio = set_dict(ids, ratio)

        ## reverter o dicionário (key passa a values e vice-versa)
        d_id_ratio = {v: k for k, v in d_id_ratio.items()}


        ## obter lista com os 10 valores maiores para o ratio 
        ratio = sorted(ratio)[-10:]
        
        
        
        ids_list = list_ids(d_id_ratio, ratio)
        # print(ids_list)
        
        
        
        names = names_list(d_all, ids_list)
        

        title = 'Voos atrasados por aeroporto (top 10)'          
        ylabel = 'rácio' 
        return(names, ratio, title, ylabel)
#####################################################################################################################        


plot_grafs(graf1, graf2, graf3)














