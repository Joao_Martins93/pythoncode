# -*- coding: utf-8 -*-

# ## figura com 4 graficos
# import numpy as np
# import matplotlib.pyplot as plt

# x1 = np.linspace(0.0, 5.0)
# x2 = np.linspace(0.0, 2.0)

# # y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)
# # y2 = np.cos(2 * np.pi * x2)

# data_x = np.linspace(0.0, 5.0)
# data_y = np.cos(2 * np.pi * x1) * np.exp(-x1)

# plt.figure(facecolor = 'lightgrey')
# plt.subplot(2, 2, 1)
# plt.plot(data_x, data_y, 'r-')
# plt.subplot(2, 2, 2)
# plt.plot(data_x, data_y, 'b-')
# plt.subplot(2, 2, 4)
# plt.plot(data_x, data_y, 'g-')


# data_x = np.linspace(0.0, 2.0)
# data_y = np.cos(2 * np.pi * x2)
# fig, ax = plt.subplots(2, 2)
# fig.set_facecolor('lightgrey')
# ax[0, 0].plot(data_x, data_y, 'r-')
# ax[1, 0].plot(data_x, data_y, 'b-')
# fig.delaxes(ax[1, 0])
# ax[1, 1].plot(data_x, data_y, 'g-')
# ax[0, 1].plot(data_x, data_y, 'b-')


############################################


##  start_time = int(round(time.time()1000))
##  end_time = int(round(time.time()1000)
##  print(end_time - start_time)




############################################

import csv as csv_reader
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np



# class plotdata():
   
graf1 = 'C:\\Users\\João\\Desktop\\pythoncode\\PythonProject\\850566403_T_ONTIME.csv'
graf2 = 'C:\\Users\\João\\Desktop\\pythoncode\\PythonProject\\L_AIRLINE_ID.csv'
graf3 = 'C:\\Users\\João\\Desktop\\pythoncode\\PythonProject\\L_AIRPORT_ID.csv'

    
    
def plot_grafs(graf1, graf2, graf3):
        
    listNames, listDelays, title = atraso_medio_companhia(graf1, graf2)
    
    plt.title(title)
    plt.bar(list(listNames), list(listDelays))
    plt.xticks(rotation=65)
    plt.ylim(0, 60)
    plt.legend()
    plt.show()
    
      
    
    listNames, listDelays, title = atraso_medio_aeroporto(graf1, graf3)
    
    plt.title(title)
    plt.bar(list(listNames), list(listDelays))
    plt.xticks(rotation=65)
    plt.ylim(0, 60)
    plt.legend()
    plt.show()



#####################################################################################################################

def get_DF(tId, tabE, tabN):
        
        df = pd.read_csv(tabE)
            
        df_nanFalse = df.iloc[:,:-1]
            
        df_nanFalse = df_nanFalse.replace(0.0, np.nan)
            
        df_nanFalse = df_nanFalse.dropna()
          

        AirLineByID = df_nanFalse.groupby([tId]).agg({tId : 'unique', 'ARR_DELAY_NEW' : 'mean'})
                      
            # AirLineByID = AirLineByID.round()
        
        AirLineByID = AirLineByID.nlargest(10, "ARR_DELAY_NEW", "last")
        
        return AirLineByID


#####################################################################################################################

            ## Metodo -> 2   ##
def atraso_medio_companhia(tabelaEstatisticas, tabelaNomesCompanhias):
        
            # df = pd.read_csv(tabelaEstatisticas)
            
            # df_nanFalse = df.iloc[:,:-1]
            
            # df_nanFalse = df_nanFalse.replace(0.0, np.nan)
            
            # df_nanFalse = df_nanFalse.dropna()
          

            # AirLineByID = df_nanFalse.groupby(['AIRLINE_ID']).agg({'AIRLINE_ID' : 'unique', 'ARR_DELAY_NEW' : 'mean'})
                      
            # # AirLineByID = AirLineByID.round()
        
            # AirLineByID = AirLineByID.nlargest(10, "ARR_DELAY_NEW", "last")

            # print(AirLineByID)
            
            
            AirLineByID = get_DF("AIRLINE_ID", tabelaEstatisticas, tabelaNomesCompanhias)
            
            print(AirLineByID)
            
            
            ids = AirLineByID["AIRLINE_ID"].tolist()
            
            
            ####  ID'S de atraso adicionados a uma lista
            for i in range(len(ids)):
                ids[i] = int(ids[i])
            # print(ids)

            ####   valores de atraso adicionados a uma lista  ####
            delays = AirLineByID["ARR_DELAY_NEW"].tolist()
            for i in range(len(delays)):
                delays[i] = int(delays[i])
            # print(delays)
            
            
            
            
            ##  Lista com os ID's e com os respetivos atrasos
            # print(ids)
            # print(delays)
            
            
            ###  Dicionário com keys(ids) e values(delays)
            # d = {}
            # for i in range(len(ids)):
            #     d.update({ids[i] : delays[i]})
            # print(d)
            
            
##########################
            
            
            #### numpy
            
            
            ####  Construção de dicionario com keys(Code(ex: 123))  values(Dest(ex:  US))            
            
            d = {}
            
            with open(tabelaNomesCompanhias, 'r') as file:
                csv_file = csv_reader.DictReader(file)
                for row in csv_file:
                    
                    d[list(row.values())[0]] = list(row.values())[1]
      
                # print(d)
                
                
            
            ########################################
            
            
            ####  lista com os nomes das companhias associadas aos ids de atraso
            
            companyNames = []
            
            for i in range(len(ids)):
                companyNames.append(d.get(str(ids[i])))
                
            
            for i in range(len(companyNames)):
                companyNames[i] = companyNames[i][0:10]
            
            
            
            ####  dicionários com keys(delays) (não pode ser assim pois existem variosatrasos com o mesmo valor logo nao podes ser chave)   e values(companyNames)
            
            # di = {}
            # for i in range(len(ids)):
            #     di[delays[i]] = companyNames[i]
            
          
            # print(di)
            

            ### dados para plot
            
            title = 'Atraso médio por companhia (top 10)'
            companyNames.reverse()
            delays.reverse()
            
            return(companyNames, delays, title)
            
            # plt.title('Atraso médio por companhia (top 10)')
            
            # plt.bar(list(companyNames), list(delays))
            # # plt.axis(xmin= 2002, xmax= 2019, ymin=460, ymax=495)
            # plt.xticks(rotation=65)
            # plt.ylim(0, 60)
            # plt.legend()
            # plt.show()

        
# atraso_medio_companhia(graf1, graf2) 
         
    
#####################################################################################################################    
    
    
    
    
    ##  Metodo -> 3  ##
    
    # def racios_atrasos_companhia(self, tabelaEstatisticas, tabelaNomesCompanhias):
        

        
    ## soma atrasos / nvoos total
    
    
#####################################################################################################################    
    
    
    ##  Metodo -> 4  ##
    
def atraso_medio_aeroporto (tabela_estatisticas, tabela_nomes_aeroportos):
        
        # df = pd.read_csv(tabela_estatisticas)
        
        # df_nanFalse = df.iloc[:,:-1]
        
        # df_nanFalse = df_nanFalse.replace(0.0, np.nan)
        
        # df_nanFalse = df_nanFalse.dropna()
        # # print(df_nanFalse)
        
        
        
        # # AirportByID = df_nanFalse.groupby(['DEST_AIRPORT_ID'])["ARR_DELAY_NEW"].mean()
        
        # AirportByID = df_nanFalse.groupby(['DEST_AIRPORT_ID']).agg({'DEST_AIRPORT_ID' : 'unique', 'ARR_DELAY_NEW' : 'mean'})
        
        # AirportByID = AirportByID.round()
        
        # pd.options.display.float_format = '{:,.0f}'.format
        
        # AirportByID = AirportByID.nlargest(10, "ARR_DELAY_NEW", "last")
        
        # print(AirportByID)
        
        AirportByID = get_DF("DEST_AIRPORT_ID", tabela_estatisticas, tabela_nomes_aeroportos)
        print(AirportByID)
        
        
        ####  Lista com os ID's
        destIds = AirportByID["DEST_AIRPORT_ID"].tolist()
        
        
        for i in range(len(destIds)):
            destIds[i] = int(destIds[i])
        # print(destIds)
        
        
        
        ####  Lista com os atrasos
        
        destDelays = AirportByID["ARR_DELAY_NEW"].tolist()
        
        for i in range(len(destDelays)):
            destDelays[i] = int(destDelays[i])
        # print(destDelays)
        
        
        ###############
        
        
        ####  Construção do dicionario com as keys(ex: 1234)  e values (ex: Columbia)
        
        d = {}
            
        with open(tabela_nomes_aeroportos, 'r') as file:
            csv_file = csv_reader.DictReader(file)
            for row in csv_file:      
                  d[list(row.values())[0]] = list(row.values())[1]
      
            # print(d)
        
        
        
        
        ##############################################
        
          ####  lista com os nomes das companhias associadas aos ids de atraso
            
            destNames = []
            
            for i in range(len(destIds)):
                destNames.append(d.get(str(destIds[i])))
                
            
            for i in range(len(destNames)):
                destNames[i] = destNames[i][0:10]
            
            
            
            ####  dicionários com keys(delays) (não pode ser assim pois existem variosatrasos com o mesmo valor logo nao podes ser chave)   e values(companyNames)
            
            # di = {}
            # for i in range(len(ids)):
            #     di[delays[i]] = companyNames[i]
            
          
            # print(di)
            

            ### dados para plot
            
            destNames.reverse()
            destDelays.reverse()
            
            # print(destDelays)
            
            
            
            title = 'Atraso médio por aeroporto (top 10)'
            
            return(destNames, destDelays, title)
            
            # plt.title('Atraso médio por aeroporto (top 10)')
            
            # plt.bar(list(destNames), list(destDelays))
            # # plt.axis(xmin= 2002, xmax= 2019, ymin=460, ymax=495)
            # plt.xticks(rotation=65)
            # plt.ylim(0, 90)
            # plt.legend()
            # plt.show()
        
        
# atraso_medio_aeroporto (graf1, graf3)
    
#####################################################################################################################  
    
    ##  Metodo -> 5  ##
    
    # def racios_atrasos_aeroporto(tabela_estatisticas, tabela_nomes_aeroportos):

#####################################################################################################################        


plot_grafs(graf1, graf2, graf3)














