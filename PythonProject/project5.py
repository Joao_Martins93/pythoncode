# -*- coding: utf-8 -*-

"""
# -*- coding: utf-8 -*-

@authors: Filipe Têto and João Martins

                            PROJECTO FINAL PYTHON

"""

#----------------------------------------------------------------------------#

#-------------> Time Count - Initializes the timer to see how much time it 
#               takes to run the program.

# import time

# start_time = int(round(time.time()*1000))


#----------------------------------------------------------------------------#


import auxiliarFunctions as aux
import matplotlib.pyplot as plt
import pandas as pd

 
   
statistics_graph = './850566403_T_ONTIME.csv'
airline_graph = './L_AIRLINE_ID.csv'
airport_graph = './L_AIRPORT_ID.csv'


# statistics_graph = input("Enter statistics file: ")
# airline_graph = input("Enter airline file: ")
# airport_graph = input("Enter airport file: ")


#----------------------------------------------------------------------------#

class Figure:

    statistics_table = pd.read_csv(statistics_graph)
        
    year = int(statistics_table.iloc[0]['YEAR'])
        
    month = int(statistics_table.iloc[0]['MONTH'])
        
    month_dict = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May',
         6 : 'June', 7 : 'July', 8 : 'August', 9 : 'September', 10 : 'October', 
         11 : 'November', 12 : 'December'}


    fig, ax = plt.subplots(2, 2, figsize=(15, 13))
    fig.suptitle(f'Delays at Arrivals in Comercial Aviation ({month_dict.get(month)} of {year})', fontsize = 15)
    
        
class Graph:
    
    def graph(self, index, limit, title, ylabel, listNames, listDelays):

        plt.subplot(2, 2, index)
        plt.bar(list(listNames), list(listDelays), color ="darkgrey") 
        
        
        plt.title(title, fontsize = 13)
        plt.ylabel(ylabel, fontsize = 13)
        plt.xticks(rotation=35)
        plt.ylim(0, limit)
        

    
#----------------------------------------------------------------------------#

class DataFrame:
    
    def mean_delay(self, id_filter, statistics_graph, graph):
        
        #-> Get dataframe by IDS;
            
        airline_id = aux.get_df_delays(id_filter, statistics_graph)

        
        #-> Get ID and delays list;
        
        ids = airline_id[id_filter].tolist()
        delays = airline_id["ARR_DELAY_NEW"].tolist()    


        #-> Get a list with IDS;
        
        ids = aux.get_list(ids)


        #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
        
        d = aux.get_dict(graph)

        
        #-> Get list with names and IDS;
        
        companyNames = aux.names_list(d, ids)   
        
        
        companyNames.reverse()
        delays.reverse()
        
        return (companyNames, delays)
    
    
    
    def ratio_delay(self, id_filter, statistics_graph, graph):
        #-> Get dataframe groupby IDS and ordered by top 10 ratio values
        
        total_flights = aux.get_df_ratio(id_filter, statistics_graph)
    
    
        #-> Get lists through DF
        
        ids = total_flights[id_filter].tolist()
        total_flight_list = total_flights["flights_total"].tolist()
        total_delayed_flights = total_flights["delayed_total"].tolist()
        ratio = total_flights["ratio"].tolist()
    
    
        #-> Get list with IDS
        
        ids = aux.get_list(ids)
        
        
        #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
         
        d_all = aux.get_dict(graph)
       
        
        #-> Get list with names      
        
        names = aux.names_list(d_all, ids)   
        
        
        #-> Inverts lists to correct order
        
        total_flight_list.reverse()
        total_delayed_flights.reverse()
        ids.reverse()
        names.reverse()
        ratio.reverse()
        
        return (names, ratio)
    
   
#----------------------------------------------------------------------------#

class AirlineClass(DataFrame):
    
    
    id_filter = "AIRLINE_ID"
     
    
    def mean_delay(self, statistics_graph, airline_graph):
        
        
       
        companyNames, delays = super().mean_delay(self.id_filter, statistics_graph,airline_graph)    
        
        #-> Plotting Data;
        
        
        title = 'Average Delay By Airline (Top 10)'
        ylabel = 'Minutes'

        g.graph(1, 60, title, ylabel, companyNames, delays)

    
#----------------------------------------------------------------------------#    
    
    
    def ratio_delay(self, statistics_graph, airline_graph):
        
        names, ratio = super().ratio_delay(self.id_filter, statistics_graph,airline_graph)
        
        
        #-> Plotting Data;        

        title = 'Delayed Flights By Airline (Top 10)'  
        ylabel = 'Ratio'

        g.graph(2, 0.6, title, ylabel, names, ratio)


#----------------------------------------------------------------------------#

class AirportClass(DataFrame):
    
       
    id_filter = "DEST_AIRPORT_ID"   
    
    
    def mean_delay(self, statistics_graph, airport_graph):

        destNames, destDelays = super().mean_delay(self.id_filter, statistics_graph,airport_graph)
            
        
        #-> Plotting Data;
        
        title = 'Average Delay By Airport (Top 10)'
        ylabel = 'Minutes'             
        
        g.graph(3, 90, title, ylabel, destNames, destDelays)
            
    
#----------------------------------------------------------------------------#
    
    def ratio_delay(self, statistics_graph, airport_graph):


        names, ratio = super().ratio_delay(self.id_filter, statistics_graph,airport_graph)
        
        #-> Plotting Data;
        
        title = 'Delayed Flights By Airport (Top 10)'          
        ylabel = 'Ratio' 

        g.graph(4, 0.9, title, ylabel, names, ratio)


#----------------------------------------------------------------------------#

g = Graph()
airline = AirlineClass()
airline.mean_delay(statistics_graph, airline_graph)
airline.ratio_delay(statistics_graph, airline_graph)


airport = AirportClass()
airport.mean_delay(statistics_graph, airport_graph)
airport.ratio_delay(statistics_graph, airport_graph) 



#----------------------------------------------------------------------------#

#-------------> Time Count - Checks how much time it takes to run the program.


# end_time = int(round(time.time()*1000))
# print(end_time - start_time)








