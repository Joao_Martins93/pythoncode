"""
# -*- coding: utf-8 -*-

@authors: Filipe Têto and João Martins

                            PROJECTO FINAL PYTHON

"""

#----------------------------------------------------------------------------#

#-------------> Time Count - Initializes the timer to see how much time it 
#               takes to run the program.

# import time

# start_time = int(round(time.time()*1000))


#----------------------------------------------------------------------------#


import auxiliary_functions as aux
import matplotlib.pyplot as plt
import pandas as pd

   
statistics_graph = './850566403_T_ONTIME.csv'
airline_graph = './L_AIRLINE_ID.csv'
airport_graph = './L_AIRPORT_ID.csv'


#----------------------------------------------------------------------------#

class Graphs():    
    
    def plot_grafs(self,statistics_graph, airline_graph, airport_graph):
        """
        Formerly Known As: atrasos;
        
        The method plot_grafs receives three csv files as parameters and
        plots four graphs in one figure.
    
        Parameters
        ----------
        statistics_graph : CSV FILE. 
        airline_graph : SECOND CSV FILE.
        airport_graph : THIRD CSV FILE.
    
        Returns
        -------
        RETURNS A FIGURE WITH FOUR BAR GRAPHS.
    
        """
        
        
        statistics_table = pd.read_csv(statistics_graph)
            
        year = int(statistics_table.iloc[0]['YEAR'])
            
        month = int(statistics_table.iloc[0]['MONTH'])
            
        month_dict = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May',
             6 : 'June', 7 : 'July', 8 : 'August', 9 : 'September', 10 : 'October', 
             11 : 'November', 12 : 'December'}
    
    
        fig, ax = plt.subplots(2, 2, figsize=(15, 13))
        fig.suptitle(f'Delays at Arrivals in Comercial Aviation ({month_dict.get(month)} of {year})', fontsize = 15)
        
        
        #----------> Mean Delay By Company
        
        listNames, listDelays, title, ylabel = airline.mean_delay(statistics_graph, airline_graph)
        
        plt.subplot(2, 2, 1)
        plt.bar(list(listNames), list(listDelays)) 
        
        
        plt.title(title, fontsize = 13)
        plt.ylabel(ylabel, fontsize = 13)
        plt.xticks(rotation=35)
        plt.ylim(0, 60)
        
    
        #----------> Mean Delay By Airport
        
        listNames, listDelays, title, ylabel = airport.mean_delay(statistics_graph, airport_graph)
        
        plt.subplot(2, 2, 3)
        plt.bar(list(listNames), list(listDelays))
        
        
        plt.title(title, fontsize = 13)
        plt.ylabel(ylabel, fontsize = 13)
        plt.xticks(rotation=35)
        plt.ylim(0, 90)
        
    
        #----------> Mean Ratio By Company   
    
        names, ratios, title, ylabel = airline.ratio_delay(statistics_graph, airline_graph)
        
        plt.subplot(2, 2, 2)
        plt.bar(list(names), list(ratios)) 
        
        
        plt.title(title, fontsize = 13)
        plt.ylabel(ylabel, fontsize = 13)
        plt.xticks(rotation=35)
        plt.ylim(0, 0.6)
        
        
        #----------> Mean Ratio By Airport
        
        names, ratios, title, ylabel = airport.ratio_delay(statistics_graph, airport_graph)
       
        plt.subplot(2, 2, 4)
        plt.bar(list(names), list(ratios))
        
        
        plt.title(title, fontsize = 13)
        plt.ylabel(ylabel, fontsize = 13)
        plt.xticks(rotation=35)
        plt.ylim(0, 0.9)
    
    
#----------------------------------------------------------------------------#

class DataFrame:
    
    def mean_delay():
        """
        Formerly Known As: atraso_medio_companhia and atraso_medio_aeroporto
        
        This method is implemented in child classes.

        Returns
        -------
        RETURNS THE MEAN OF DATA TO PLOT GRAPHS.

        """
        pass
    
    
    def ratio_delay():
        """
        Formerly Knonw As: racios_atrasos_companhia and racios_atrasos_aeroporto

        This method is implemented in child classes.

        Returns
        -------
        RETURNS THE RATIO OF DATA TO PLOT GRAPHS.

        """
        pass
    
   
#----------------------------------------------------------------------------#

class AirlineClass(DataFrame):
      
    
    id_filter = "AIRLINE_ID" 
    
    
    def mean_delay(self, statistics_graph, airline_graph):
            
        
        #-> Get lists with names and values to Plot;
        companyNames, delays = aux.get_mean_data_to_plot(self.id_filter, statistics_graph, airline_graph)

            
        #-> Plotting Data;       
        title = 'Average Delay By Airline (Top 10)'
        ylabel = 'Minutes'

        
        return(companyNames, delays, title, ylabel) 

    
#----------------------------------------------------------------------------#    
       
    def ratio_delay(self, statistics_graph, airline_graph):
        
        #-> Get lists with names and values to Plot;
        names, ratio = aux.get_ratio_data_to_plot(self.id_filter, statistics_graph, airline_graph)
        
        
        #-> Plotting Data;        
        title = 'Delayed Flights By Airline (Top 10)'  
        ylabel = 'Ratio'
        
        
        return(names, ratio, title, ylabel)


#----------------------------------------------------------------------------#

class AirportClass(DataFrame):
        
    
    id_filter = "DEST_AIRPORT_ID"
    
       
    def mean_delay(self, statistics_graph, airport_graph):

        #-> Get lists with names and values to Plot;
        destNames, destDelays = aux.get_mean_data_to_plot(self.id_filter, statistics_graph, airport_graph)
            
        
        #-> Plotting Data;        
        title = 'Average Delay By Airport (Top 10)'
        ylabel = 'Minutes'             
        
        
        return(destNames, destDelays, title, ylabel)
            
    
#----------------------------------------------------------------------------#
    
    def ratio_delay(self, statistics_graph, airport_graph):

        #-> Get lists with names and values to Plot;
        names, ratio = aux.get_ratio_data_to_plot(self.id_filter, statistics_graph, airport_graph)
        
        
        #-> Plotting Data;
        title = 'Delayed Flights By Airport (Top 10)'          
        ylabel = 'Ratio' 
        
        
        return(names, ratio, title, ylabel)

#----------------------------------------------------------------------------#

airline = AirlineClass()
airport = AirportClass()     
g = Graphs()
g.plot_grafs(statistics_graph, airline_graph, airport_graph)


#----------------------------------------------------------------------------#

#-------------> Time Count - Checks how much time it takes to run the program.


# end_time = int(round(time.time()*1000))
# print(end_time - start_time)








