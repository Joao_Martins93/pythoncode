# -*- coding: utf-8 -*-

import csv as csv_reader
import pandas as pd
import numpy as np
    
#----------------------------------------------------------------------------#

def get_list(l): 

    #  Simplifies Top 10 Airline IDS
    
    for i in range(len(l)):
        l[i] = int(l[i])
       
    return (l)


#----------------------------------------------------------------------------#

def get_dict(graph):

    # Simplifies CSV files in dict    
        
    d = {}
            
    with open(graph, 'r') as file:
        csv_file = csv_reader.DictReader(file)
        for row in csv_file:
                    
            d[list(row.values())[0]] = list(row.values())[1][0:10]

    return d


#----------------------------------------------------------------------------#

def names_list(d, ids):
    names = []
    
    # Creates a list with the IDS and Names to plot

    for i in range(len(ids)):
        names.append(d.get(str(ids[i])))
   
    return names


#----------------------------------------------------------------------------#


def get_df_delays(id_filter, statistics_graph): 
      
        statistics_table = pd.read_csv(statistics_graph)
                
        stat_filtered_table = statistics_table.iloc[:,:-1].replace(0.0, np.nan).dropna()
                
    
              
    
        dfByID = stat_filtered_table.groupby([id_filter]).agg({id_filter : 'unique', 'ARR_DELAY_NEW' : 'mean'})                     
            
        dfByID = dfByID.nlargest(10, "ARR_DELAY_NEW", "last")
            
        return dfByID

#----------------------------------------------------------------------------#


def get_df_ratio(id_filter, statistics_graph):
        
        statistics_table = pd.read_csv(statistics_graph)
  
        statistics_table = statistics_table.iloc[:,:-1].dropna()
        
        total_flights = statistics_table.groupby([id_filter]).agg({id_filter : 'unique'})
        
        total_flights['flights_total'] = statistics_table.groupby([id_filter]).agg({id_filter : 'count'})
            
        statistics_table = statistics_table.replace(0.0, np.nan).dropna()
            
        total_flights['delayed_total'] = statistics_table.groupby([id_filter]).agg({id_filter : 'count'})
        
        
        total_flights['ratio'] = total_flights['delayed_total'] / total_flights['flights_total']
        total_flights = total_flights.nlargest(10, "ratio", "last")
        

        return total_flights

