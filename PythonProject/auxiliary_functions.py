"""
# -*- coding: utf-8 -*-

@authors: Filipe Têto and João Martins

                            PROJECTO FINAL PYTHON

"""

#----------------------------------------------------------------------------#

import csv as csv_reader
import pandas as pd
import numpy as np
    
#----------------------------------------------------------------------------#

def get_list(l):
    """
    This function receives the unformatted list and converts all the
    numbers to integers.

    Parameters
    ----------
    l : UNFORMATTED LIST.

    Returns
    -------
    l : A LIST WITH INTEGERS NUMBERS.

    """

    
    for i in range(len(l)):
        l[i] = int(l[i])
       
    return (l)


#----------------------------------------------------------------------------#

def get_dict(graph):
    """
    This function receives a CSV file and simplies it in a dictionary with key
    and values corresponding to column file name.

    Parameters
    ----------
    graph : CSV FILE.

    Returns
    -------
    d : A DICTIONARY.

    """

       
        
    d = {}
            
    with open(graph, 'r') as file:
        csv_file = csv_reader.DictReader(file)
        for row in csv_file:
                    
            d[list(row.values())[0]] = list(row.values())[1][0:10]

    return d


#----------------------------------------------------------------------------#

def names_list(d, ids):
    """
    This function creates a list with the ID's and Names to plot graphs.

    Parameters
    ----------
    d : DICTIONARY WITH KEYS AND VALUES CORRESPONDING TO ID'S AND NAMES.
    ids : A LIST WITH ID'S FROM FUNCTION GET_LIST.

    Returns
    -------
    names : A LIST WITH THE NAMES GATHERED FROM D AND IDS.

    """
    names = []
    

    for i in range(len(ids)):
        names.append(d.get(str(ids[i])))
   
    return names


#----------------------------------------------------------------------------#


def get_df_delays(id_filter, statistics_graph):
    """
    This function creates a dataframe grouped by ID's ordered by the Top 10 
    of the highest mean values.

    Parameters
    ----------
    id_filter : IS A STRING THAT FILTERS THE DATA TO REPRESENT AIRPORTS OR 
    AIRLINES.
    statistics_graph : CSV FILE.

    Returns
    -------
    dfByID : A DATAFRAME FILTERED BY ID SHOWING ONLY THE TOP 10 RESULTS.

    """
      
    statistics_table = pd.read_csv(statistics_graph)
                
    stat_filtered_table = statistics_table.iloc[:,:-1].replace(0.0, np.nan).dropna()
                
    
              
    
    dfByID = stat_filtered_table.groupby([id_filter]).agg({id_filter : 'unique', 'ARR_DELAY_NEW' : 'mean'})                     
            
    dfByID = dfByID.nlargest(10, "ARR_DELAY_NEW", "last")
            
    return dfByID

#----------------------------------------------------------------------------#


def get_df_ratio(id_filter, statistics_graph):
    """
    This function creates a dataframe grouped by ID's ordered by the Top 10 
    of the highest ratio values.

    Parameters
    ----------
    id_filter : IS A STRING THAT FILTERS THE DATA TO REPRESENT AIRPORTS OR 
    AIRLINES.
    
    statistics_graph : CSV FILE.

    Returns
    -------
    dfByID : A DATAFRAME FILTERED BY ID SHOWING ONLY THE TOP 10 RESULTS.

    """

        
    statistics_table = pd.read_csv(statistics_graph)
  
    statistics_table = statistics_table.iloc[:,:-1].dropna()
        
    total_flights = statistics_table.groupby([id_filter]).agg({id_filter : 'unique'})
        
    total_flights['flights_total'] = statistics_table.groupby([id_filter]).agg({id_filter : 'count'})
            
    statistics_table = statistics_table.replace(0.0, np.nan).dropna()
            
    total_flights['delayed_total'] = statistics_table.groupby([id_filter]).agg({id_filter : 'count'})
        
        
    total_flights['ratio'] = total_flights['delayed_total'] / total_flights['flights_total']
    total_flights = total_flights.nlargest(10, "ratio", "last")
        

    return total_flights


#----------------------------------------------------------------------------#


def get_mean_data_to_plot(id_filter, statistics_graph, graph):
    """
    This fuction organizes mean data from other fuctions to plotable data.

    Parameters
    ----------
    id_filter : IS A STRING THAT FILTERS THE DATA TO REPRESENT AIRPORTS OR 
    AIRLINES.
        
    statistics_graph : CSV FILE.
    
    graph : CSV FILE WHETER IT IS AIRLINE OR AIRPORT.

    Returns
    -------
    A LIST WITH NAMES AND A LIST WITH THE ID'S TO PLOT DATA

    """
    
    #-> Get dataframe groupby IDS and ordered by top 10 mean values
            
    airline_id = get_df_delays(id_filter, statistics_graph)

        
    #- > Get ID and delays list
        
    ids = airline_id[id_filter].tolist()
    delays = airline_id["ARR_DELAY_NEW"].tolist()    


    #-> Get a list with IDS
        
    ids = get_list(ids)


    #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
        
    d = get_dict(graph)

        
    #-> Get list with names and IDS
        
    companyNames = names_list(d, ids)    
        
        
    #-> Inverts lists to correct order
        
    companyNames.reverse()
    delays.reverse()
    
    return(companyNames, delays)


#----------------------------------------------------------------------------#

def get_ratio_data_to_plot(id_filter, statistics_graph, graph):
    """
    This fuction organizes ratio data from other fuctions to plotable data.

    Parameters
    ----------
    id_filter : IS A STRING THAT FILTERS THE DATA TO REPRESENT AIRPORTS OR 
    AIRLINES.
        
    statistics_graph : CSV FILE.
    
    graph : CSV FILE WHETER IT IS AIRLINE OR AIRPORT.

    Returns
    -------
    A LIST WITH NAMES AND A LIST WITH THE ID'S TO PLOT DATA


    """
    
    #-> Get dataframe groupby IDS and ordered by top 10 ratio values
        
    total_flights = get_df_ratio(id_filter, statistics_graph)


    #-> Get lists through DF
    
    ids = total_flights[id_filter].tolist()
    total_flight_list = total_flights["flights_total"].tolist()
    total_delayed_flights = total_flights["delayed_total"].tolist()
    ratio = total_flights["ratio"].tolist()


    #-> Get list with IDS
    
    ids = get_list(ids)
    
    
    #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
     
    d_all = get_dict(graph)
   
    
    #-> Get list with names      
    
    names = names_list(d_all, ids)   
    
    
    #-> Inverts lists to correct order
    
    total_flight_list.reverse()
    total_delayed_flights.reverse()
    ids.reverse()
    names.reverse()
    ratio.reverse()
    
    return (names, ratio)
























