# -*- coding: utf-8 -*-

"""
# -*- coding: utf-8 -*-

@authors: Filipe Têto and João Martins

                            PROJECTO FINAL PYTHON

"""

#----------------------------------------------------------------------------#

#-------------> Time Count - Initializes the timer to see how much time it 
#               takes to run the program.

import time

start_time = int(round(time.time()*1000))


#----------------------------------------------------------------------------#


import auxiliarFunctions as aux

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
   
statistics_graph = 'C:\\Users\\João\\Desktop\\projecto-final-python\\850566403_T_ONTIME.csv'
airline_graph = 'C:\\Users\\João\\Desktop\\projecto-final-python\\L_AIRLINE_ID.csv'
airport_graph = 'C:\\Users\\João\\Desktop\\projecto-final-python\\L_AIRPORT_ID.csv'


#----------------------------------------------------------------------------#
    
def plot_grafs(statistics_graph, airline_graph, airport_graph):
    """
    The method plot_grafs receives three csv files as parameters and
    plots four graphs in one figure.

    Parameters
    ----------
    statistics_graph : CSV FILE. 
    airline_graph : SECOND CSV FILE.
    airport_graph : THIRD CSV FILE.

    Returns
    -------
    RETURNS A FIGURE WITH FOUR BAR GRAPHS.

    """
    statistics_table = pd.read_csv(statistics_graph)
        
    year = int(statistics_table.iloc[0]['YEAR'])
        
    month = int(statistics_table.iloc[0]['MONTH'])
        
    month_dict = {1 : 'January', 2 : 'February', 3 : 'March', 4 : 'April', 5 : 'May',
         6 : 'June', 7 : 'July', 8 : 'August', 9 : 'September', 10 : 'October', 
         11 : 'November', 12 : 'December'}


    fig, ax = plt.subplots(2, 2, figsize=(15, 13))
    fig.suptitle(f'Delays at Arrivals in Comercial Aviation ({month_dict.get(month)} of {year})', fontsize = 15)
    
    
    #----------> Mean Delay By Company
    
    listNames, listDelays, title, ylabel = airline_mean_delay(statistics_graph, airline_graph)
    
    plt.subplot(2, 2, 1)
    plt.bar(list(listNames), list(listDelays)) 
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 60)
    

    #----------> Mean Delay By Airport
    
    listNames, listDelays, title, ylabel = airport_mean_delay(statistics_graph, airport_graph)
    
    plt.subplot(2, 2, 3)
    plt.bar(list(listNames), list(listDelays))
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 90)
    

    #----------> Mean Ratio By Company   

    names, ratios, title, ylabel = airline_ratio_delay(statistics_graph, airline_graph)
    
    plt.subplot(2, 2, 2)
    plt.bar(list(names), list(ratios)) 
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 0.6)
    
    
    #----------> Mean Ratio By Airport
    
    names, ratios, title, ylabel = airport_ratio_delay(statistics_graph, airport_graph)
   
    plt.subplot(2, 2, 4)
    plt.bar(list(names), list(ratios))
    
    
    plt.title(title, fontsize = 13)
    plt.ylabel(ylabel, fontsize = 13)
    plt.xticks(rotation=35)
    plt.ylim(0, 0.9)
    
    
#----------------------------------------------------------------------------#

# class DataFrame:
    
#     def getRatioDataFrame:
#         pass
    
#     def getMeanDataFrame:
#         pass
    
#----------------------------------------------------------------------------#

class MeanDataFrame:
    
    
    def get_df_delays(self, id_filter, statistics_graph): 
      
        statistics_table = pd.read_csv(statistics_graph)
                
        stat_filtered_table = statistics_table.iloc[:,:-1].replace(0.0, np.nan).dropna()
                
    
              
    
        dfByID = stat_filtered_table.groupby([id_filter]).agg({id_filter : 'unique', 'ARR_DELAY_NEW' : 'mean'})                     
            
        dfByID = dfByID.nlargest(10, "ARR_DELAY_NEW", "last")
            
        return dfByID
    
mean_DataFrame = MeanDataFrame()   

#----------------------------------------------------------------------------#

class RatioDataFrame:
    
    def get_df_ratio(self, id_filter, statistics_graph):
        
        statistics_table = pd.read_csv(statistics_graph)
  
        statistics_table = statistics_table.iloc[:,:-1].dropna()
        
        total_flights = statistics_table.groupby([id_filter]).agg({id_filter : 'unique'})
        
        total_flights['flights_total'] = statistics_table.groupby([id_filter]).agg({id_filter : 'count'})
            
        statistics_table = statistics_table.replace(0.0, np.nan).dropna()
            
        total_flights['delayed_total'] = statistics_table.groupby([id_filter]).agg({id_filter : 'count'})
        
        
        total_flights['ratio'] = total_flights['delayed_total'] / total_flights['flights_total']
        total_flights = total_flights.nlargest(10, "ratio", "last")
        

        return total_flights

ratio_DataFrame = RatioDataFrame()  

#----------------------------------------------------------------------------#




#----------------------------------------------------------------------------#

def airline_mean_delay(statistics_graph, airline_graph):
                
        #-> Get dataframe by IDS;
            
        airline_id = mean_DataFrame.get_df_delays("AIRLINE_ID", statistics_graph)

        
        #-> Get ID and delays list;
        
        ids = airline_id["AIRLINE_ID"].tolist()
        delays = airline_id["ARR_DELAY_NEW"].tolist()    


        #-> Get a list with IDS;
        
        ids = aux.get_list(ids)


        #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
        
        d = aux.get_dict(airline_graph)

        
        #-> Get list with names and IDS;
        
        companyNames = aux.names_list(d, ids)    
        
            
        #-> Plotting Data;
        
        
        title = 'Average Delay By Airline (Top 10)'
        ylabel = 'Minutes'
        companyNames.reverse()
        delays.reverse()
            
        
        return(companyNames, delays, title, ylabel)          

    
#----------------------------------------------------------------------------#    
    
def airline_ratio_delay(statistics_graph, airline_graph):
        
        #-> Get Ratios DF;
        
        total_flights = ratio_DataFrame.get_df_ratio("AIRLINE_ID", statistics_graph)


        #-> Get lists through DF;
        
        ids = total_flights["AIRLINE_ID"].tolist()
        total_flight_list = total_flights["flights_total"].tolist()
        total_delayed_flights = total_flights["delayed_total"].tolist()
        ratio = total_flights["ratio"].tolist()


        #-> Get list with IDS;
        
        ids = aux.get_list(ids)
        
        
        #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
         
        d_all = aux.get_dict(airline_graph)
       
        
        #-> Get list with names;       
        
        names = aux.names_list(d_all, ids)   
        
        
        #-> Inverts lists to correct order;
        
        total_flight_list.reverse()
        total_delayed_flights.reverse()
        ids.reverse()
        names.reverse()
        ratio.reverse()
        
        
        #-> Plotting Data;        

        title = 'Delayed Flights By Airline (Top 10)'  
        ylabel = 'Ratio'
        
        
        return(names, ratio, title, ylabel)
    
    
#----------------------------------------------------------------------------#    
    
def airport_mean_delay (statistics_graph, airport_graph):

        #-> Get dataframe by IDS;
        
        airport_id = mean_DataFrame.get_df_delays("DEST_AIRPORT_ID", statistics_graph)
       
       #-> Get ID and delays list;
       
        destIds = airport_id["DEST_AIRPORT_ID"].tolist()     
        destDelays = airport_id["ARR_DELAY_NEW"].tolist()
        
        
        #-> Get a list with IDS;
        
        destIds = aux.get_list(destIds)
        

        #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
        
        d = aux.get_dict(airport_graph)
        

        #-> Get list with names and IDS;
        
        destNames = aux.names_list(d, destIds)   
                   

        #-> Inverts lists to correct order;
         
        destNames.reverse()
        destDelays.reverse()
            
        
        #-> Plotting Data;
        
        title = 'Average Delay By Airport (Top 10)'
        ylabel = 'Minutes'             
        
        
        return(destNames, destDelays, title, ylabel)
            
    
#----------------------------------------------------------------------------#
    
def airport_ratio_delay(statistics_graph, airport_graph):


        #-> Get Ratios DF;
        
        total_flights = ratio_DataFrame.get_df_ratio("DEST_AIRPORT_ID", statistics_graph)


        #-> Get lists through DF;
        
        ids = total_flights["DEST_AIRPORT_ID"].tolist()
        total_flight_list = total_flights["flights_total"].tolist()
        total_delayed_flights = total_flights["delayed_total"].tolist()
        ratio = total_flights["ratio"].tolist()
       
        
        #-> Get list with IDS;
        
        ids = aux.get_list(ids)
        
        
        #-> Creates adict with keys(ex: 1234) and values (ex: Columbia)
        
        d_all = aux.get_dict(airport_graph)
       

        #-> Get list with names and IDS;
        
        names = aux.names_list(d_all, ids)   
        
        
        #-> Inverts lists to correct order;
        
        total_flight_list.reverse()
        total_delayed_flights.reverse()
        ids.reverse()
        names.reverse()
        ratio.reverse()

        
        #-> Plotting Data;
        
        title = 'Delayed Flights By Airport (Top 10)'          
        ylabel = 'Ratio' 
        
        
        return(names, ratio, title, ylabel)
    
    
#----------------------------------------------------------------------------#        


plot_grafs(statistics_graph, airline_graph, airport_graph)



#----------------------------------------------------------------------------#

#-------------> Time Count - Checks how much time it takes to run the program.


end_time = int(round(time.time()*1000))
print(end_time - start_time)








