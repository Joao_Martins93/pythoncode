## -*- coding: utf-8 -*-

## João Martins

#################################################
#################################################

print('\n')

#################################################

print('\nFicha 1: ')

## Escreva em linguagem phython um programa que leia uma
## temperatura em graus Fahrenheit e que converta em celcios


print('Ex 1: ')
temp = float(input('Enter temperature to convert: '))

c = ((temp - 32) / 1.8)
print(c)

#################################################

print('\n')

#################################################

## Escreva um programa que peca o ano de nascimento ao utilizador
## e retorne a idade que ira ter ao fim do ano

print('\nEx 2: ')

from datetime import date as d

date = int(input('Enter the year when you were born: '))
print(f'No final de {d.today().year} terá' ,d.today().year - date, 'ano(s)')

#################################################

print('\n')

#################################################

## Escreva um programa que leia um hora em horas, minutos e segundos e
## que traduza para segundos

print('\nEx 3: ')

time = input('Enter the time: ')

t=time.split(':')

total_minutes= int(t[0])*3600+int(t[1])*60 +int(t[2])

print(total_minutes)

#################################################

print('\n')

#################################################

## Se x = 1, qual o valor de x no final da execução

print('\nEx 5: ')

x = 1

if x == 1:
    x += 1
    if x == 1:      # parte do programa que nunca é executada
        x += 1      #
    else:
        x -= 1
else:
    x -= 1

print(x)

print('a- se x no inicio for 1 o resultado é 1')
print('b- se x for 0 entao o resultado é -1')
print('c- a parte do programa que nunca corre é o codigo que esta dentro do if == 1')


#################################################

print('\n')

#################################################


## Escreva um programa em python que recebe um numero
## inteiro e que o representa em notação romana

print('\nEx 6: ')

roman_dic = {1 : 'I', 4 : 'IV', 5 : 'V', 9 : 'IX', 10 : 'X', 40 : 'XL', 50 : 'L', 
        90 : 'XC', 100 : 'C', 400 : 'CD', 500 : 'D', 900 : 'CM', 1000 : 'M'}

l = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]

a = int(input('Enter a number to convert to roman: '))
roman = ''

for x in l:
    quociente = a // x
    
    if quociente != 0:
        for _ in range(quociente):
            roman += roman_dic[x]
    a = a % x
    
print(roman)


# Or:
    
print('\nOr: ')

roman_dic = tuple(zip((1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
                  ('M','CM','D','CD','C','XC','L','XL','X','IX','V','IV','I')))

n = int(input('Enter a number: '))
roman = []

for decimal, letra_r in roman_dic:
    count = n //decimal
    roman.append(letra_r * count)
    n -= decimal * count
print(''.join(roman), end = ' ')

#################################################

print('\n')

#################################################

## Escreva em linguagem python um programa que leia um ano (>0)
## e escreva o século a que pertence

print('\nEx 7: ')

year = int(input('Enter a year: '))

if(year % 100 == 0):
    print((year) // 100)
else:
    print((year) // 100 + 1)


print('\nOr: ')


year = int(input('Enter a year: '))
temp = year // 100
rest = year % 100
print(temp if rest == 0 else temp + 1)

#################################################

print('\n')

#################################################

## Escreva um programa em python que receba ano, mes e dia
## em separado, e um número de dias X e devolva uma nova data Y dias mais tarde

print('\nEx 8: ')


# dia = int(input('Introduza o dia: '))
# mes = int(input('Introduza o mes: '))
# ano = int(input('Introduza o ano: '))

# dias = int(input('Introduza os dias a avançar: '))

# # bis = ano % 4

# dic_mes = {1 : 31, 2 : 29, 3 : 31, 4 : 30, 5 : 31, 6 : 30, 7 : 31, 8 : 31, 9 : 30, 10 : 31, 11 : 30, 12 : 31}

# dia_s = dia + dias

# if dia_s in range(1, dic_mes[mes] +1):
#     print(f'{dia_s}/{mes}/{ano}')
    
# else:
#     if mes == 12 and dia_s not in range(1, dic_mes[mes] +1):
#         ano += 1
#         mes = 1
#         while dia_s > dic_mes[mes]: #??
#             dia_s -= dic_mes[mes]
#         print(f'{dia_s}/{mes}/{ano}') 
#     else:
#         while dia_s > dic_mes[mes]:   #??
#             dia_s -= dic_mes[mes]
#         mes += 1
#         print(f'{dia_s}/{mes}/{ano}')


## OR ##

# print('\nOr: ')

# if mes == 12:
#       if dia + dias > dic_mes[mes]:
#           mes = 1
#           dia = dias - (dic_mes[mes] - dia)
#           ano += 1
#       else:
#           dia = dia + dias

# else:
#      if ano != 4 and not (ano != 100 and ano != 400):
#          dias_mes = 0
#          if mes == 2:
#              dias_mes = dic_mes[mes] + 1
#          else:
#              dias_mes = dic_mes[mes]
            
#          if dia + dias > dic_mes[mes]:
#               mes = 1
#               dia = dias - (dic_mes[mes] - dia)
#               ano += 1
#          else:
#              dia = dia + dias
#      else:
#          dias_mes = dic_mes[mes]
#          if(dia + dias > dic_mes[mes]):
#              while dia + dias > dic_mes[mes]:
#                  mes = 1
#                  dia = dias - (dias_mes - dia)
#                  ano += 1
#          else:
#              dia = dia + dias

# print(f'{ano}/{mes}/{dia}')


print('\nOr: ')

ano = int(input("ano: "))
mes = int(input("mes: "))
dia = int(input("dia: "))
dias_add = int(input("que dia será daqui a este número de dias? "))

meses_dias = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12:31}

e_ano_bisexto = (mes == 2 and ano % 4 == 0 and not (ano % 400 == 0 and ano % 100 == 0))
dias_do_mes = meses_dias[mes] + e_ano_bisexto # you can add boolean expressions to numbers, as if True it will be interpreted as one and if False it will be interpreted as 0

while dia + dias_add > dias_do_mes:
    ano += (mes + 1) > 12 # Adicionado um se o proximo mes for menor que 12 (Dezembro)
    mes = (mes + 1) % 12 if mes + 1 > 12 else mes + 1 # se o mes for maior que doze incrementamos o mes e obtemos o resto da divisão (ex: mes = 13 entÃ£o mes % 12 = 1)
    dias_add -= dias_do_mes - dia # removemos os restantes dias que faltam no mes dos dias adicionados
    dia = 0 # pomos o dia como 0 para adicionar-mos os restantes dias adicionais caso saia do loop ou no próximo loop sem ter um dia a mais
    dias_do_mes = meses_dias[mes] + e_ano_bisexto # atualizamos os dias do mes

dia += dias_add # adicionamos os restantes dias do mes

print(f"{ano}-{mes}-{dia}")

#################################################

print('\n')

#################################################

print('\nEx 9: ')

x = 1
y = 1
while x == 1 and y < 5:
    y = y + 2

print('O erro neste programa era na condição do while estar (x = 1)  e deveria estar (x == 1)')


#################################################

print('\n')

#################################################

print('\nEx 10: ')

n = int(input("Escreve um número inteiro: "))
print("Tabuada do", n, ":")

i = 1
while i <= 10:
    print(n, "x", i, "=", n * i)
    i += 1
    
print('\nN = ', n)
print('I =', i)

print('\nO programa não terminava pois o (i) não era incrementado corretamente')
    
#################################################

print('\n')

#################################################
import math as m


print('\nEx 12: ')

n = int(input("Escreve um número inteiro: "))
current_n = 1
while current_n <= n and m.factorial(current_n) <= 1000:
    print("Factorial de " + str(current_n) + ": " + str(m.factorial(current_n)))
    current_n += 1

#################################################
