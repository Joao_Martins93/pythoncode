## -*- coding: utf-8 -*-

## João Martins


#################################################


print('\n')


#################################################


a = [['a', 2], ['a', 0], ['b', 25], ['c', 0]]
a.sort()
print('Sorted: ', a)


def compare_items(item):
    return len(item)


# a = [['a', 2], ['abdjssj', 0], [][]]


#################################################


print('\n')


#################################################

def multiplicar_numero(numero, valor = 2):
    return numero * valor

print(multiplicar_numero(2))

print(multiplicar_numero(2, 3))



#################################################


print('\n')


#################################################


a = 'C'

def triplica(string):
    return string * 3

print(triplica(a))


#################################################


print('\n')


#################################################

def range(start, end, step = 1):
    i = start
    while i < end:
        yield i           ## yield-> para a execução 
        i += step         ## e guarda o estado da função


a = range(0, 10, 2)

for i in a:
    print(i)

#################################################


print('\n')


#################################################

def aplica_funcao_a_elementos(lista, funcao):
    for i in range(0, len(lista)):
        lista[i] = funcao(lista[i])
    return lista

def add_two(x):
    return x + 2

print(aplica_funcao_a_elementos([2, 5, 1, 8], add_two))



lista =[2, 5, 1, 8]
a = map(add_two, lista)  ## MAP-> aplica uma funcao a cada elemento da lista

print(a)

a = list(a)

print(a)


#################################################


print('\n')


#################################################

def multiply(a, b):
    return a * b

a = map(multiply, (1, 2, 3), (2, 3, 4))

a = list(a)
print(a)



#################################################


print('\n')


#################################################


a = [1, 22, 33, 3]

def lt_dezasseis(x):
    return x < 16

filtered_a = list(filter(lt_dezasseis, a))

## filter-> filtrar os elementos da lista para os quais uma dada
##          função retorna False

print(filtered_a)


#################################################


print('\n')


#################################################

a = ('abc', 'ahsdkjfhagrhty', 'aisudgfuiahs')

sorted_a = sorted(a, key = len)

print(sorted_a)

sorted_a = sorted(a, key = len, reverse = True)

print(sorted_a)

#################################################


print('\n')


#################################################

a = ['abc', 'def', 'ghi']


def my_sum(lista):
    accumulator = type(lista[0])()
    for item in lista:
        accumulator += item
    return accumulator

print(my_sum(a))
print('\n')

lista_listas = [[1, 2], [3, 4], [5, 6]]

print(my_sum(lista_listas))
print('\n')

def add(a, b):
    return a + b

from functools import reduce

sumed_lista_listas = reduce(add, lista_listas)
print(sumed_lista_listas)
print('\n')


fruits = ['banana', 'apple', 'peach']
sumed_fruits = reduce(add, fruits, 'I love ')
print(sumed_fruits)
print('\n')

sumed_fruits = reduce(lambda x, y: x + y, fruits)
print(sumed_fruits)

#################################################


print('\n')


#################################################

a = [[1, 2], [3, 4]]

flattened_a = [sub_item for item in a for sub_item in item]

print(flattened_a)

print('Or: ')

print('\nItem: ')
for item in a:
    for sub_item in item:
        print (item)
        
        
print('\nSub_Item: ')
for item in a:
    for sub_item in item:
        print (sub_item)

print('\n')

result = []

for item in a:
    for sub_item in item:
        result.append(sub_item)
print(result)

print('\n')

from functools import reduce
flattened_a = reduce(lambda x, y : x + y, a)
print(flattened_a)



#################################################


print('\n')


#################################################

def imprimeDivisaoInteira(x, y):
    """
    

    Parameters
    ----------
    x : int
        dividendo.
    y : int
        divisor.

    Returns
    -------
    None.

    """

    if y == 0:
        print('Divisao por zero')
    else:
        print(x // y)
        
        
        
imprimeDivisaoInteira(4, 2)
imprimeDivisaoInteira(2, 4)
imprimeDivisaoInteira(3, 0)
help(imprimeDivisaoInteira)
## imprimeDivisaoInteira()  da erro pois falta parametros

#################################################

print('\n')


#################################################


def potencia(a, b):
    return a ** b

a = 2
b = 3

potencia(b, a)
potencia(a, b)
print(potencia(b, a))
print(potencia(a, b))
print(potencia(2, 0))
# ## print(potencia(2))  falta 1 parametro

def potenciaP(a):
    return a ** a
print(potenciaP(a))

print('\n')




def potencia1(a, b = a):
    if b == a:
        return a ** a
    else:
        return a ** b
print(potencia1(a, b))

print('\n')


def potencia2(a, b = None):
    return a ** a if b == None else a ** b
print(potencia2(a))



#################################################


print('\n')


#################################################

a = 4

def printFuncao():
    a = 17
    print('Dentro da funcao: ', a)

printFuncao()

print('Fora da funcao: ', a)


######

a = 0
def function():
    global a     ## refere a variavel global para nova atribuicao
    a = 2
    print('local: ', a)
function()
print(a)

######

def function1():
    a = 3
    print('non-local: ', a)   
    def function2():
        nonlocal a 
        a = 4
        print(a)
    print('non-local: ', a)
    
function1()


#################################################


print('\n')


#################################################
from datetime import datetime as dt

ANO_ATUAL = dt.now().year
MES_ATUAL = dt.now().month
DIA_ATUAL = dt.now().day


# print('Dados da Pai: ')

# anoPai = int(input('\nIntroduza o ano de nascimento: '))

# mesPai = int(input('\nIntroduza o mes de nascimento: '))

# diaPai = int(input('\nIntroduza o dia de nascimento: '))

# if mesPai > MES_ATUAL or \
#     (mesPai == MES_ATUAL and diaPai > DIA_ATUAL):
        
#         print('\nPai tem', ANO_ATUAL - anoPai - 1, 'ano(s)')
# else:
#     print('\nPai tem', ANO_ATUAL - anoPai, 'ano(s)')

# if mesMae > MES_ATUAL or \
#     (mesMae == MES_ATUAL and diaMae > DIA_ATUAL):
        
#         print('\nMae tem', ANO_ATUAL - anoMae - 1, 'ano(s)')
# else:
#     print('\nMae tem', ANO_ATUAL - anoMae, 'ano(s)')



def pedir_dados():
    
    print('\nDados: ')
    ano = int(input('Introduza o ano de nascimento: '))
    mes = int(input('\nIntroduza o mes de nascimento: '))
    dia = int(input('\nIntroduza o dia de nascimento: '))
    return ano, mes, dia


def dados(a, m, d, n):
    
    if m > MES_ATUAL or \
    (m == MES_ATUAL and d > DIA_ATUAL):
        print(f'\n{n} tem', ANO_ATUAL - a - 1, 'ano(s)')
    else:
        print(f'\n{n} tem', ANO_ATUAL - a, 'ano(s)')
    


anoPai, mesPai, diaPai = pedir_dados()

anoMae, mesMae, diaMae = pedir_dados()


dados_pai = dados(anoPai, mesPai, diaPai, 'Pai')
dados_mae = dados(anoMae, mesMae, diaMae, 'Mae')


#################################################


print('\n')


#################################################

print('\nExercicio 4: ')

def somaDivisores(num):
 """
 Soma de divisores de um numero dado
 
 Requires:
     num seja int e num > 0
 
 Ensures: um int correspondente à soma dos divisores
 de num que sejam maiores que 1 e menores que num
 """

print('\na- como cliente da função somaDivisores tenho de introduzir um número que seja inteiro e maior que zero para cumprir o contrato.')
print('\nb- se chamar a função satisfazendo a sua pré-condição irei obter a soma dos divisores do numero inteiro passado por parametro.')

#################################################


print('\n')


#################################################

print('\nSupondo que a função somaDivisores do exercicio 4 está implementada: ')

print('\nExercicio 5: ')

num = int(input('Enter a number: '))

while num >=0:
    somaDivisores(num)
    num = int(input('Enter another number(to quit enter a negative number): '))

#################################################


print('\n')


#################################################

print('\nExercicio 11: ')

def numPrimo(num):
   
    for i in range(2, num+1):
        if i != num:
            if num % i == 0:
                print(f'{num} não é número primo!')
                break
        else:
            print(f'{num} é número primo!')


num = int(input('Introduza um número maior que um para verificar se é primo: '))

while num >= 1:
    
    mult = numPrimo(num)
        
    num = int(input('Introduza outro número(ou 0 para sair): '))
print('BYE!')



