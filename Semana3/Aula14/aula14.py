## -*- coding: utf-8 -*-

## João Martins

#################################################

print('\n')

#################################################

print('\nFicha 4: ')
print('Ex 4: ')

from statistics import mean

def medias(ficheiro):
    try:   
        with open(ficheiro) as f_reader:
            for line in f_reader:
                line = line.strip()
                split_line = line.split()
                # for i in range(len(split_line)):
                #     split_line[i] = float(split_line[i])
                # for item in split_line:
                #     item = float(item)
                # print(split_line)
                float_list = list(map(float, split_line))
                print(round(mean(float_list), 2))
    except:
        print('Something went wrong while reading the file')

            
medias('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula14\\temperaturas.txt')

print('\nOr: ')

def medias(ficheiro):
    try:   
        f_reader = open(ficheiro)
        
        try:    
            for line in f_reader:
                line = line.strip()
                split_line = line.split()
                # for i in range(len(split_line)):
                #     split_line[i] = float(split_line[i])
                
                float_list = list(map(float, split_line))
                print(round(mean(float_list), 2))
        except:
              print('Something went wrong while reading the file')
        finally:
            f_reader.close()
    except:
        print('falha a abrir ficheiro')

medias('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula14\\temperaturas.txt')



print('\nEx 5: ')

def ficheiro_falha():
    user_input = input('Indique o nome do ficheiro: ')
    try:
        medias(user_input)
    except:
        print('Error.')
ficheiro_falha()


#################################################

print('\n')

#################################################

print('\nEx 1 (Manipulação de CSV): ')

import csv

with open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula14\\eggs.csv', newline = '') as csvfile:
    spamreader = csv.reader(csvfile, delimiter = ' ', quotechar = '|')
    # for row in spamreader:
    #     print(', '.join(row))
    print(spamreader.__next__())
    
#################################################

print('\n')

#################################################

#  [(1.2,1.5), (2.3, 18.5), (3.4, 19.4)]

<<<<<<< HEAD
=======
print('\nEx 2: ')

>>>>>>> e39f2669af68a3d8552bc9c9dfa21378ca87261d
import csv


def le_graficos(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter = ';')
        linha1 = csv_reader.__next__()
        linha2 = csv_reader.__next__()
        for i in range(len(linha1)):
            item_linha1_com_ponto = linha1[i].replace(',', '.') # 1,2 -> 1.2
            item_linha2_com_ponto = linha2[i].replace(',', '.') # 1,2 -> 1.2
            grafico.append((float(item_linha1_com_ponto), float(item_linha2_com_ponto)))
    return grafico
<<<<<<< HEAD

print(le_graficos('C:\\Users\\Admin\\Desktop\\pythoncode\\Semana3\\Aula14\\graficos.csv'))
=======
>>>>>>> e39f2669af68a3d8552bc9c9dfa21378ca87261d

print(le_graficos('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula14\\graficos.csv'))

print('\n')


def converte_para_notacao_ponto(item1, item2):
    return float(item1.replace(',', '.')), float(item2.replace(',', '.'))


def le_graficos(ficheiro_csv):
    grafico = []
    with open(ficheiro_csv) as csvfile:
        csv_reader = csv.reader(csvfile, delimiter = ';')
        linha1 = csv_reader.__next__()
        linha2 = csv_reader.__next__()
        grafico = list(map(converte_para_notacao_ponto,linha1, linha2))
        return grafico
    
print(le_graficos('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula14\\graficos.csv'))


#################################################

print('\n')

#################################################

##############  CLASS  ###############

class House:
    
    area = 120              # variaveis da class
    window_size = (2, 3)
    
    def func(self):         # funcoes da class
        pass                # indica que falta código
        
    # variaveis e funcoes dentro de uma classe sao chamados atributos
    # tudo o que esta dentro de uma funcao sao atributos

    def __init__(self, cor_paredes= ' ', cor_telhado = ' '):
        self.cor_paredes = cor_paredes
        self.cor_telhado = cor_telhado


print(House.area)           # funções só podem ser acedidas por objetos 
                            # da classe a menos que se consiga passar um objeto
                            

##############
#
# int()
# float() estas funcoes chaman-se inicializadores
# 
##############

concrete_House = House(0xffffff, 0x0000ff)  # ->(cor branca) 0x indica que o numero é hexadecimal
concrete_House.area = 450

print(concrete_House.cor_paredes)

print(concrete_House.cor_telhado)


house = {'area' : 120, 'window_soze' : (2, 3)}
house['portas'] = 2
print(house.keys())



another_house = House()
another_house.cor_telhado
another_house.cor_paredes
another_house.portas = 2

print('quantas portas? ' + str(another_house.portas))


del(another_house.portas)

# del(another_house.area)

print('qual a area da casa? ' + str(another_house.area))


and_another_house = House()
print(and_another_house.area)

#################################################

print('\n')

#################################################

class Pais:
    
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
    
    def trabalhar(self):
        print('trabalhar')
        
        
        
class Human:
    compassion = True        
        

    
class Eu(Pais, Human):
    
    def __init__(self, nome, idade, hobby):
        super().__init__(nome, idade)
        self.hobby = hobby
        
        
    def trabalhar(self):
        super().trabalhar()
        print('e trabalhar mais')





eu = Eu('João Martins', 27, 'Jogar à bola')
print('O meu nome é: ' + eu.nome)
print('A minha idade é: ' + str(eu.idade))
print('O meu hobby é: ' + eu.hobby)
eu.trabalhar()
print(f'I have compassion. {eu.compassion}')


class EstruturaDeDados():
    pass

estrutura = EstruturaDeDados()
estrutura.x = 2



#################################################

print('\n')

#################################################

print('\nFicha 8: ')

print('\nEx 1: ')
class Pessoa:
    
    def __init__(self, nome, idade, peso, altura):
        self.nome = nome
        self.idade = idade
        self.peso = peso
        self.altura = altura

    def Envelhecer(self):
        self.idade += 1

    def Engordar(self, peso):
        self.peso += peso
        
    def Emagrecer(self, peso):
        self.peso -= peso
        
    def Crescer(self):
        if self.idade < 21:
            self.altura += 0.5

print('\nEx 2: ')

class Macaco:
    
    def __init__(self, nome= '', bucho = []):
        self.nome = nome
        self.bucho = bucho

    
    def comer(self, a1=''):
        self.bucho.append(a1)
    
    def verBucho(self):
        print(self.bucho)
    
    def digerir(self):
        self.bucho.clear()
        
        
m1 = Macaco()
m1.comer('banana')
m1.verBucho()
m1.comer('ananás')
m1.verBucho()
m1.digerir()
m1.verBucho()


m2 = Macaco()
m2.comer(m1)
m2.verBucho()
m2.digerir()
m2.verBucho()


