## -*- coding: utf-8 -*-

## João Martins


#################################################

print('\n')

#################################################
print('\nFICHA 2: ')
print('\nEx 7: ')

def maximum(a, b):
    return a if a > b else b
print(maximum(2, 16))


print('\nOr: ')


def maximum(a, b):
    '''
    Devolve o maior de dois numeros

    Parameters
    ----------
    a : int
        Um número inteiro.
    b : int
        Um número inteiro.

    Returns
    -------
    int
        maior de dois números.

    '''
    return max(a, b)
print(maximum(2, 16))



print('\n')


def minimum(a, b):
    return b if a == maximum(a, b) else a
print(minimum(3, 15))


#################################################

print('\n')

#################################################

print('\nEx 8: ')

##  123 -> 12

def retira(a):
    '''
    retira casa das unidades do inteiro

    Parameters
    ----------
    a : string
        numero inteiro mas tratado como string.

    Returns
    -------
    string
        retorna a string/numero inteiro sem a casa das unidades.

    '''
    
    # print('\nOr: ')
    # if len(a) <= 1:
    #     return 0
    # else:
    #     return  a[:-1]
    
    
    str_n = str(a)
    return 0 if len(str_n) <= 1 else str_n[:-1]
    
print(retira(457575467))


print('\nOr: ')



def retira(b):
    return b // 10

print(retira(1234))




#################################################

print('\n')

#################################################

print('\nEx 9: ')

def add_zero(a):
    '''
    Adiciona um zero no fim de um inteiro. Ex (123 -> 1230)

    Parameters
    ----------
    a : int
        numero inteiro.

    Returns
    -------
    
        Devolve um inteiro com 0 no fim.

    '''
    return int(str(a) + '0')

print(add_zero(1223))


#################################################

print('\n')

#################################################


a = list(map(lambda x: x + 1, range(1, 4)))
print(a)

print('\n')

a = list(filter(lambda x: x > 5, range(1, 7)))
print(a)

#################################################

print('\n')

#################################################


######      FILES      ##########

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt')
print(f)

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt')
print(f)

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'w') # apaga o que estava antes e escreve por cima

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'a') # append # escreve informação para um ficheiro sem apagar o que la está

# f = open('demofile.txt', 'x') # cria um ficheiro e dá erro se existir

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'r') # abre ficheiro para leitura
print(f.read())
read = f.read(5)  # le 5 primeiros caracteres to ficheiro
print(read)


print(f.readline())
print(f.readline())


f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'r')
for line in f:
    print(line)
f.close()


with open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt') as file_reader:
    for line in file_reader:
        print(line)



f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'a')
f.write('\nNow the file has more content!')
# f.write('\\nNow the file has more content!')
f.close()



f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'w')
f.write('Woops! I have deleted the content!')
f.close()

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'r')
print(f.read())

import os 

f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'r', encoding = 'utf-8')

print(os.path.abspath('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt')) # caminho absoluto para o ficheiro

print('\n')

print(os.path.relpath('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt'))  # imprime caminho relativo

print('\n')

print(os.path.basename('./blobfile.txt')) # imprime nome do ficheiro





import os 
os.remove('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt')
import copy

lines = []
lines_for_writing = []
lines_after_writing = []


with open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt') as f_reader:
    lines = f_reader.readlines() # le linhas para uma lista de strings
    lines_for_writing = copy.deepcopy(lines)
    
    for i in range(len(lines_for_writing)):
        lines_for_writing[i] = lines[i].replace('s', 'p')    
    print(lines)



with open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt') as f_reader:
    lines = f_reader.readlines() # le linhas para uma lista de strings
    print(lines)






with open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt', 'w') as f_writer:
    f_writer.writelines(lines)



with open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt') as f_reader:
    lines_after_writing = f_reader.readlines() # le linhas para uma lista de strings
    print(lines_after_writing == lines)
    

#################################################

print('\n')

#################################################

try:
    a = int('12')
    a / 0
except ValueError:
    print('The string must contain numbers not letters!')
except:
    print('Alguma coisa correu mal')
finally:
    print('thnx')





f = open('C:\\Users\\João\\Desktop\\pythoncode\\Semana3\\Aula13\\blobfile.txt')
try:
    # do something with file content
    pass
# except expression as identifier:
    # handle exception
    pass
finally:
    f.close()



# with open('demofile.txt') as f_reader:
#     try:
#         f_reader.readlines
#         # do something
#     except expression as identifier: # as é para pegarmos na excepção e modificala no bloco 
#         identifier


import csv
with open('example.csv') as csvfile:
    spamreader  = csv.reader(csvfile, fmtparams = any)

#################################################

print('\n')

#################################################

print('\nFicha 3: ')
print('Ex 1: ')

print('\nA: ', map(lambda x : x + 1, range(1,4)))
print('A_1 (list): ', list(map(lambda x : x + 1, range(1,4))))

print('\nB: ', map(lambda x : x > 0, [3, -5, -2, 0]))
print('B_1 (list): ', list(map(lambda x : x > 0, [3, -5, -2, 0])))

print('\nC: ', filter(lambda x : x > 5, range(1,7)))
print('C_1 (list): ', list(filter(lambda x : x > 5, range(1,7))))

print('\nD: ', filter(lambda x : x % 2 == 0, range(1,11)))
print('D_1 (list): ', list(filter(lambda x : x % 2 == 0, range(1,11))))

#################################################

print('\n')

#################################################

print('\nEx 2: ')

import functools as f
print('\nA: ', f.reduce(lambda y, z : y * 3 + z, range(1,5)))
print('\nB: ', f.reduce (lambda x, y : x ** 2 + y, range(2,6)))

