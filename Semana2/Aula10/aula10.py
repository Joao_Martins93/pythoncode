# -*- coding: utf-8 -*-

# João Martins

######################################

print('\nExercise 1: ')

s = 'abfabdcaa'
d = {}

for i in s:
    d[i] = d.get(i, 0) + 1
    
print(d)

#################################################

print('\n')

#################################################

print('\nExemplos aula: ')
print('->')
s = 'string Occurences'
d = {}

for i in s:
    d[i] = d.get(i, 0) + 1
print(d)

#################################################

print('\n')

#################################################

print('\nExercicio 6: ')

s = 'Python Course'
x = ['o', 'r']

d = {}

for i in s:
    if i in x:
        d[i] = d.get(i, 0) + 1
        
print(d)

print('\nOR: ')

print('\n->')
s = 'Python Course'
x = ['o', 'r']

d = {}

for i in s:
    if i in x:
        d[i] = d.get(i, 0) + 1
        
print(d)


#################################################

print('\n')

#################################################

print('\nExercise 7: ')

d = {'x' : 3, 'y' : 2, 'z' : 1, 'y' : 4, 'z' : 2}

r = {}

for k, v in d.items():
    if k not in r.keys():
        r[k] = v
print(r)

#################################################

print('\n')

#################################################

print('\nExercise 8: ')
students = [
            {'id' : 123, 'name' : 'Sophia', 's' : True},
            {'id' : 378, 'name' : 'William', 's' : False},
            {'id' : 934, 'name' : 'Sara', 's' : True}
            ]
c = 0
for i in range(len(students)):
    if students[i]['s'] == True:
        c += 1
print(c)

print('\nOR: ')

s = 0
for i in students: # i in range(len(students))
    s += int(i['s'])    # i['s']   # s += students[i]['s']
print(s)


print('\nOr: ')

print(sum(d['s'] for d in students))

#################################################

print('\n')

#################################################

family = {
          'child1' : {'name' : 'James', 'age' : 8},
          'child2' : {'name' : 'Emma', 'age' : 20}
          }
print(family)

d1 = {'name' : 'James', 'age' : 8}
d2 = {'name' : 'Emma', 'age' : 20}

family = {
        'child1' : d1,
        'child2' : d2
        }

#################################################

print('\n')

#################################################

d = {
      'F' : 0,
      'B' : 0
      }

import random

for _ in range(17):
    d[random.choice(list(d.keys()))] += 1
print(d)

#################################################

print('\n')

#################################################

# defining list/tuples/dic/set with no elements
# a = []
# b = ()
# c = {}
# d = set()

#################################################

#################   SET   #######################

f = {'apple', 'orange', 'banana'}

print(type(f))
print(len(f))
print('\nF: ', f)

print('\n')

for i  in f:
    print(i)

m = set(('orange', 'banana', 'apple'))
print('\nM: ', m)

print(f == m)

print('cherry' in f)

#################################################

print('\n')

#################################################

f = {'apple', 'orange', 'banana'}

f.add('cherry')
print('\nF: ', f)

f.update(['mango', 'grapes'])
print('\nF: ', f)

f.remove('apple')
print(f)

#################################################

print('\n')

#################################################

print('\nExamples: ')
f = {'a', 'b', 'c'}
f.add('d')
## f.add('e', 'f')
## f.add('e2', 'f2')
## f.add(['e2', 'f2'])

## 4A = {'a', ['b', 'c']}

f.update('g')
f.update('h', 'i')
f.update(['h', 'i'])

print(f)

#################################################

print('\n')

#################################################

vowels = {'a', 'e', 'i', 'o', 'u'}

#vowels.remove('k')

vowels.discard('k')

v2 = vowels

c = vowels.copy()

print('\nVowels1: ', vowels)

x = vowels.pop()

print('\nX: ', x)
print('\nVowels2: ', vowels)
print('\nV2: ', v2)
print('\nC: ', c)

vowels.clear()
print(vowels)
print(len(vowels))

del c

#################################################

print('\n')

#################################################

x = {1, 2, 3}
y = {2, 3, 4}
print('\nUnion: ', x.union(y))
print('\nUnion2: ', x | y)

print('\nIntersection: ', x.intersection(y))
print('\nIntersection2: ', x & y)

x.update(y)
print('\nUpdate: ', x)

#################################################

print('\n')

#################################################

a = {1, 2, 3, 4, 5}
b = {2, 4, 7}

print('\nA - B: ', a - b)
print('\nB - A: ', b - a)


r = a.difference(b)
print('\nR: ', r)
print('\nA: ', a)
print('\nB: ', b)


x = {1, 2, 3}
y = {2, 3, 4}
print('\nSymetric dif: ', x.symmetric_difference(y))
print('\nXOR: ', x ^ y)
print('\nUnion(x-y) - Intersection(x-y): ', x.union(y) - x.intersection(y))
print('\nUnion(x-y) - Intersection(y-x): ', x.union(y) - y.intersection(x))


r = a.difference_update(b)
print('\nR: ', r)
print('\nA: ', a)
print('\nB: ', b) 


#################################################

print('\n')

#################################################

print('\nExercise 1: ')

a = {1, 2, 3, 4, 6, 9, 10}
b = {1, 3, 4, 9, 13, 14, 15}
c = {1, 2, 3, 6, 9, 11, 12, 14, 15}

print('\nAnswer 1_1: ', c - a.intersection(b).intersection(c)) # Or c-(a & b & c)
print('\nAnswer 1_2: ', c-(a & b & c))
print('\nAnswer 1_3: ', c- a.intersection(b, c))
print('\nAnswer 1_4: ', c.difference(a & b & c))

                                                    
print('\nAnswer 2_1: ', a.intersection(b) - c) 
print('\nAnswer 2_2: ', (a & b) - c)

print('\nAnswer 3_1: ', a.union(b) - c)
print('\nAnswer 3_2: ', a.union(b) - a.union(b).intersection(c))                                
print('\nAnswer 3_3: ', (a | b) - c)

#################################################

print('\n')

#################################################

s = 'Hamed'

a = [13, 25]

t = (7, 8)

d = {'one' : 1, 'two' : 2}

x = {56, 98}

x.update(s, a, t, d)

print(x)

#################################################

print('\n')

#################################################

x = {1, 2}
y = {1, 2, 3}
print(x.isdisjoint(y))

x = {1, 2}
y = {3, 7,  8}
print(x.isdisjoint(y))  # isdisjoint() -> no intersection


#################################################

print('\n')

#################################################


a = {1, 2, 4}
b = {1, 2, 3, 4, 5}

print(a.issubset(b))
print(b.issubset(a))

c = set()

print(c.issubset(a))
print(a.issubset(c))

#################################################

print('\n')

#################################################

print('\nExercise 2: ')
s = 'Python Course'
s = set(s)
l = {'a', 'y', 'c', 'o', 'z'}

print(l.intersection(s))

print('\nOr: ')

print(s.intersection(l))

#################################################

print('\n')

#################################################

print('\nHomework 1: ')

d1 = {'a' : 1, 'b' : 3, 'c' : 2}
d2 = {'a' : 2, 'b' : 3, 'c' : 1}
d = {}
# out {'b' : 3}

for i in d1:
    if d1[i] == d2[i]:
        d[i] = d1[i]
print(d)


# SET
print('\nOr using set: ')
s1 = d1.items()
s2 = d2.items()
d = s1 & s2

print(dict(d))

#################################################

print('\n')

#################################################

print('\nHomework 2: ')

print('\nEx 1: ')
a = [2, 4]
b = [6, 8]
c = []

print(a)
print(b)

if a[1] < b[0] or a[0] > b[1]:
    print('\nNo intersection.')
    
else:    
    if a[0] > b[0]:
        
        c.append(a[0])
    
    else:
        c.append(b[0])
    
    
        
    if a[1] < b[1]:
        c.append(a[1])
    else:
        c.append(b[1])
    print('\n')
    print(c) 
    
    
print('\nEx 2: ')
a = [4, 8]
b = [2, 6]
c = []


print(a)
print(b)

if a[1] < b[0] or a[0] > b[1]:
    print('\nNo intersection.')
    
else:    
    if a[0] > b[0]:
        
        c.append(a[0])
    
    else:
        c.append(b[0])
    
    
        
    if a[1] < b[1]:
        c.append(a[1])
    else:
        c.append(b[1])
    print('\n')
    print(c) 
    


print('\nEx 3: ')

a = [4, 6]
b = [8, 10]
c = []

print(a)
print(b)

if a[1] < b[0] or a[0] > b[1]:
    print('\nNo intersection.')
    
else:    
    if a[0] > b[0]:
        
        c.append(a[0])
    
    else:
        c.append(b[0])
    
    
        
    if a[1] < b[1]:
        c.append(a[1])
    else:
        c.append(b[1])
    print('\n')
    print(c) 
    
    
    
print('\nEx 4: ')

a = [3, 7]
b = [5, 9]
c = []

print(a)
print(b)

if a[1] < b[0] or a[0] > b[1]:
    print('\nNo intersection.')
    
else:    
    if a[0] > b[0]:
        
        c.append(a[0])
    
    else:
        c.append(b[0])
    
    
        
    if a[1] < b[1]:
        c.append(a[1])
    else:
        c.append(b[1])
    print('\n')
    print(c) 


#################################################

print('\n')

#################################################



def inter(a, b):
        print(a)
        print(b)
        if (min(a) > min(b) or min(b) > min(a)) and (max(a) < max(b) or max(b) < max(a)):
            print('Intersection')
        else:
            print('No intersection')


a = [4, 5]
b = [3, 6]

inter(set(range(a[0], a[1] + 1)), set(range(b[0], b[1] + 1)))

print(set(range(a[0], a[1] + 1)))

print(set(range(b[0], b[1] + 1)))

print(set(range(a[0], a[1])).issubset((set(range(b[0], b[1])))))


######################################



######################################
















