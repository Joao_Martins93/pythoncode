# -*- coding: utf-8 -*-

# João Martins

#######################################################

########  EXAMPLES  TUPLES  ########

t = ('English', 'History', 'Mathematics')
print(t)
print(type(t))
print(len(t))


print('\n')


t1 = (3)
t2 = (3,)
s1 = ('a')
s2 = ('a',)


#################################################

print('\n')

#################################################


t = ('English', 'History', 'Mathematics')
print(t[0])
print(t[1:3])
print(t.index('English'))
print('English' in t)
# t[0] = 'art' # Error: immutable

for i in t:
    print(f'I like to read {i}')


#################################################

print('\n')

#################################################


t = (1, 9, 2)

print(sum(t))
print(max(t))
print(min(t))
print(t.count(9))

print(t*2)
print(t + t + t)

print((3, 6) + (9,))
print((1, 2) + (9, 6))

print(tuple(reversed(t)))


t1 = (1, 2)
t2 = (2, 1)
print(t1 == t2)



#################################################

print('\n')

#################################################


#### APPEND

print('\nExercise 1:')


t = (4, 6)
t += (9,)
print(t)


print('\nOr: ')


t = (4, 6)
print(t)

l = list(t)
print(l)

l.append(9)
# or ->  l.extend([9])
print(l)

t = tuple(l)
print(t)


#################################################

print('\n')

#################################################


print('\nExercise 2: ')

t = "$ $ Python$#Course $"
t = t.replace('$ ', '')
t = t.replace(' $', '')
print(t.replace('$#', ' '))


print('\nOr: ')



t = t.strip('# $')
t = t.replace('$#', ' ')
print(t)

print('\nOr: ')

t = t.strip('# $')


l = list(t)
b = l.copy()
for i in l:
    if i == '#' or i == '$' or i == ' ':  # i in ['#', '$', ' ']
        b.remove(i)

b.insert(b.index('C'), ' ')

t = ''.join(b)
print(b)


#################################################

print('\n')

#################################################

t = (4, 7, 2, 9, 8)
x = list(t)
x.remove(2)
t = tuple(x)
print(t)


#################################################

print('\n')

#################################################

print('Example 1: ')

t = (4, 8)
a, b = t
print(a)
print(b)


print('\nExample 2: ')

car = ('blue', 'auto', 7)

color, _, a= car

print(color)
print(_)
print(a)

#################################################

print('\n')

#################################################

a = (1, 2)
b = (3, 4)
c = zip(a, b)
x = list(c)

print(x)
print(x[0])
print(type(x[0]))

print('\n')

z = ((1, 3), (2, 4))
u = zip(*z)
print(list(u))

#################################################

print('\n')

#################################################

print('Example 1: ')

a = (1, 2, 'A')
b = (3, 4, 8)
c = zip(a, b)
x = list(c)
print(x)

print(list(zip(*x)))

print('\n')

a = [11, 22, 33]
b = zip(a, range(2))
print(list(b))


#################################################

print('\n')

#################################################

print('\nExercise 3: ')

a = [1, 2, 'A']
b = ('Python', 161.8, 0, 5)
c = {10, 12, 14, 16, 18, 20}

# print(list(zip(a, b, c)))

c = list(c)


size = min (len(a), len(b), len(c))


t = []
for i in range(size):
    t.append((a[i], b[i], c[i]))
    
print(t)

#################################################

print('\n')

#################################################

a = (1,)

print(isinstance(a, tuple))    

#################################################

print('\n')

#################################################

num = [8, 2, (9, 3), 4, (1, 6, 7), 34]
c = 0

for i in num:
    if isinstance(i, tuple):
        continue
    c += 1
    
print(c)
print(len(num) - c)

#################################################

print('\n')

#################################################

print('\nExercise 4: ')

num = [8, 2, (9, 3), 4, (1, 6, 7), 34]
c = 0

for i in num:
    if type(i) == tuple:
        c += 1
    
print(c)


#################################################

print('\n')

#################################################

print('\nExample: ')
a = [(1, 2, 3), (4, 5, 6)]

b = [i[:-1] + (9,) for i in a]

print(b)



#################################################

print('\n')

#################################################

print('\nHomework 1: ')

a = [(1, 2, 3), (4, 5, 6)]
b = []
c = a
for i in range(len(a)):
    b = list(a[i])
    b[len(b) -1 ] = 9
    c[i] = tuple(b)
print(c)    
    

#################################################

print('\n')

#################################################

print('\nHomework 2: ')

my_obj = [(1,3), (4,5)]
#my_obj = enumerate(my_obj)

for i, j in my_obj:
    print('i is: ', i)
    print('j is: ', j)
    

#################################################

print('\n')

#################################################

print('\nHomework 3: ')
a = [(1, 3), (2, 4) , ('A', 8)]
b = []
c = ()

for i in range(len(a)-1):
    for j in range(len(a)):
        c += (a[j][i],)
    b.append(c)
    c = ()
    
print(b)
















