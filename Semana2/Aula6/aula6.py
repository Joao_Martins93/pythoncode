# -*- coding: utf-8 -*-

# João Martins


##################################################

import math
print(math.pi)

import math as m
m.pi 
math.pi 

from os import getcwd
getcwd()

from os import getcwd as gc
gc()


from math import e, pi
print(e)


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = np.array([-20, -3, -2, -1, 0, 1, 2, 3, 4])
plt.boxplot(data)
import matplotlib.pyplot as plt

##################################################
"""

question?

check the number
1_more than 100
2_equal 100
3_less than 100

check:
        a_odd
        b_even
"""
##################################################

print('\n')

##################################################

print('\nExamples: ')

import random
import math

for i in range(5):
    print(random.randint(1, 25))
print(math.pi)

####

print('\n')

def square(a):
    return a ** 2
square(2)

##################################################

print('\n')

##################################################
import math as m
import random as r
print('\nExercise 2: ')

print('In math module: ')
print(m.fmod(9,4))                 # 9%4
print(m.gcd(30,4))                 # greatest common divisor
print(m.fabs(-4))

print('\nIn random module: ')
print(r.randint(1, 5))
print(r.choice([1, 5]))
a = [1, 2, 3, 4]
print(r.shuffle(a))

###########################

print('\nMore Examples: ')

import math
print( math.sqrt(4))        #2.0
print( math.trunc(2.3))     #2
print( math.floor(2.3))     #2
print( math.ceil(2.3))      #3
print( math.factorial(4))   #24 , 4! = 4*3*2*1
print( math.log2(32))       #5.0
print( math.log10(100))     #2.0
print( math.e)              #2.7
print( math.log(32))        #3.46
print( math.sin(5))         #-0.9
print( math.fmod(9,4))      #1.0 , 9%4
print( math.gcd(30,4))      #2 , greatest common divisor
print( math.fabs(-4))       #4.0 , float abs
print( abs(-4))             #4
print( math.pow(2,3))       # 8.0
print( pow(2,3))            # 8
print( math.pi)             # 3.1415926…
print(f'{math.pi :.2f}')    # 3.14


#############

print('\n')

import random
print( random . randint(1, 5))  # random number between 1 to 5
print( random . choice([1, 5])) # random choice between only 1 or 5
a = [1, 2, 3, 4]
random.shuffle(a)               # randomize arrangement of a
print(a)                        # [4, 2, 1, 3]

##########

print('\n')

import sys
print(sys.version)              # 3.8.5
print(sys.platform)             # win32
import platform
platform.release()              # 10 ➔ it means windows 10

##########

import datetime
now = datetime.datetime.now()
print(now)                          # 2020-10-20 11:30:47.724484
print(now.year)                     # 2020
print(now.month)                    # 10
print(now.day)                      # 20
print(datetime.datetime.today())    # 2020-10-20 11:31:45.597811
type(now)                           # datetime.datetime

##################################################

print('\n')

##################################################

print('\nExercice 3: ')

print('\nSolution 1: ')

from datetime import datetime as dt

m = dt.today().minute

if m % 2 == 0:
    print('Not an Odd minute')
else:
    print('Odd minute')
    
####### OR ########    
print('\nOr:')
print('Solution 2: ')

from datetime import datetime as dt

m = dt.today().minute
check = False

for i in range(1, 60, 2):
    if m == i:
        check = True
        break
    else:
        check = False
if check:
    print('Odd minute')
else:
    print('not an Odd minute')

####### OR ########  

print('\nOr:')
print('Solution 3: ')

from datetime import datetime as dt

m = dt.today().minute

odds_lst = [i for i in range(1, 60, 2)]

if m in odds_lst:
    print('Odd minute')
else:
    print('not an Odd minute')
    

####### OR ########  

print('\nOr:')
print('Solution 4: ')

n = int(input("Enter a number: "))

while(n != -1):

    if(n > 100):
        if(n % 2 == 0):
            print("\nEven ", n)
        else:
            print("\nOdd ", n)
           
    elif(n < 100):
        if(n % 2 == 0):
            print("\nEven ", n)
        else:
            print("\nOdd ", n)
           
    else:
        print("\nEven ", n)
    
    n = int(input("\nEnter a number (-1 to exit): "))
print("\nBYE!!")

##################################################

print('\n')

##################################################

#sum??
#import numpy 
#numpy.sum??


import math
print(dir(math))

def my_func():
    """
    this is a custom function    
    """
    print("python")
my_func()

def square(a):
    return a ** 2

########################################################
print(square(int(input("\nEnter a number: "))))

import math as m
print (m.fmod(9, 4))
print (m.gcd(30, 4))
print (m.fabs(-4))

import random as r
print (r.randint(1, 5))
print (r.choice([1, 5]))

a = [1, 2, 3, 4]
print (r.shuffle([1,2,3,4]))

#########################################
import sys
print(sys.version)

##########################################
import datetime


now = datetime.datetime.now()
print(now)

print(now.day)
print(now.month)
print(now.year)


print(datetime.datetime.today())

#############################################

minute = datetime.datetime.now().minute

if(minute % 2 == 0):
    print(f"\nEven {minute}")
else:
    print("\nOdd ", minute)


if minute in range(1, 61, 2):
    print("\nOdd ", minute)
else:
    print("\nEvem ", minute)

for i in range(1, 60, 2):
    
    if minute == i:
        print("Odd minute")
        break
    else:
        print("Even minute")
        break
    


odd_list = [i for i in range(1, 60, 2)]

if minute in odd_list:
    print("Odd  minute")
else:
    print("Even minute")

