# -*- coding: utf-8 -*-

# João Martins

##################################

###   DICTIONARY  ####

# EXAMPLES:


# use {key:value}
#


#################################################

print('\n')

#################################################

d = {'brand' : 'cherry',
      'model' : 'arizo5',
      'color' : 'white'
      }

print(type(d))
print(len(d))

print('\n')

d['year'] = '2010'
print(d)

print(d['model'])

d['color'] = 'Black'

print(d)

print('\n')

x = d.get('model')
print(x)

x = d.get('cylinder')
print(x)

x = d.get('cylinder', -1)
print(x)

#################################################

print('\n')

#################################################

d = {'brand' : 'cherry',
      'model' : 'arizo5',
      'color' : 'white'
      }
print(d)

print('\nKeys: ', list(d.keys()))
print('\nValues: ', list(d.values()))
print('\nItems: ', list(d.items()))

print('\n')

for k, v in d.items():
    print(k, ':', v)
    
print('\n')

d.pop('model')
print(d)

f = d.popitem()
print(d)
print(f)
print(type(f))

d.clear()

print(d)

del d

#################################################

print('\n')

#################################################

d1 = {1 : 'One', 2 : 'Two', 3 : 'Three'}

d2 = {'1' : 'One', '2' : 'Two', '3' : 'Three'}


print(d1[1])
print(d2['1'])


d2['4'] = 'Four'
print(d2)


d2['4'] = 'F'
print(d2)


#################################################

print('\n')

#################################################


d1 = {'1' : 'One', '2' : 'Two', '3' : 'Three'}

## check if '1' is in d1
x = '1' in d1

y = d1.get('3', -1)
print(x)
print(y)


#################################################

print('\n')

#################################################

d1 = {'1' : 'One', '2' : 'Two', '3' : 'Three'}

for k, v in d1.items():
    print(f'key {k} is: ', k)
    print(f'value {k} is:', v)

#################################################

print('\n')

#################################################


a = ['x', 'y', 'x', 'z', 'y', 'x']
d = {}

for i in a:
    if i not in d:
        d[i] = 1
    else:
        d[i] += 1
print(d)

#################################################

print('\n')

#################################################

a = ['x', 'y', 'x', 'z', 'y', 'x']
d = {}

for i in a:
    d[i] = d.get(i, 0) + 1
    
print(d)

#################################################

print('\n')

#################################################

a = ['x', 'y', 'x', 'z', 'y', 'x']
d = {}

for i in a:
    d[i] = d.setdefault(1, 0) + 1
print(d)

#################################################

print('\n')

#################################################

d3 = {}

for i in range(1, 101):
    d3.setdefault(i, str(i))
print(d3)


#################################################

print('\n')

#################################################


a = {}
b = a
c = a.copy()

#################################################

print('\n')

#################################################


print("\nExercise 1: ")

a = "abfabdcaa"
a = list(a)
d = {}

for i in a:
    if i not in d:
        d[i] = 1
    else:
        d[i] += 1
print(d)        


####### OR ########

print('\nOr: ')

a = "abfabdcaa"
d = {}

for i in a:
    d[i] = d.get(i, 0) + 1
print(d) 



#################################################

print('\n')

#################################################


print('\nExercise 2: ')

print("\nExample 1: ")
s = 'a dictionary is a datastructure.'
d = {}
l = s.split()

for i in l:
        d[i] = d.get(i, 0) + 1
print(d)


#################################################

print('\n')

#################################################

print("\nExercise 3: ")
line = 'a dictionary is a datastructure\na set is also a datastructure.'
line = line.strip('.')
d = {}
s = line.split()

for i in s:
    d[i] = d.get(i, 0) + 1

print(d)

#####


print("\nExample 1: ")

line = 'a dictionary is a datastructure\na set is also a datastructure.'

line1 = line.split('\n')[0]
line1 = line1.split('.')[0]

# OR

line1 = line.split('\n')[0].split('.')[0]

# line2 = line.split('\n')[1]
# line2 = line2.split('.')[0]

## OR

line2 = line.split('\n')[1].split('.')[0]

####### OR #########

print('\nOR: ')

lines = 'a dictionary is a datastructure\na set is also a datastructure.'

d = {}

for i in range(len(lines.split('\n'))):
    
    line = lines.split('\n')[i].split('.')[0]
    
    s = line.split()
    
    for i in s:    
        d[i] = d.get(i, 0) + 1

print(d)


#################################################

print('\n')

#################################################


print('\nExercise 4: ')

d = {'a' : 4, 'b' : 2, 'f' : 1, 'd' : 1, 'c' : 1}
s = 0

for i in d:
    s += d[i]
print(s)

print('\nOR: ')
print(sum(d.values()))


#################################################

print('\n')

#################################################


d = {'a' : 4, 'b' : 2, 'f' : 1, 'd' : 1, 'c' : 1}

import operator

k = operator.itemgetter(1)
print(sorted(d.items(), key = k))

k = operator.itemgetter(0)
print(sorted(d.items(), key = k))

#################################################

print('\n')

#################################################

num = {
        'ali' : [12, 13, 8],
        'sara' : [15, 7, 14],
        'taha' : [5, 18, 13]
        }

d = {k : sorted(v) for k, v in num.items()}

print(d)

#################################################

print('\n')

#################################################

d1 = {'x' : 3, 'y' : 2, 'z' : 1}
d2 = {'w' : 8, 't' : 7, 'z' : 5}

d1.update(d2)
print(d1)

########### OR ###############

print('\nOR: ')

d1 = {'x' : 3, 'y' : 2, 'z' : 1}

d2 = {'w' : 8, 't' : 7, 'z' : 5}

d = {}

for i in (d1, d2):
    print(i)
    d.update(i)
print(d)


#  OR

print('\nOR: ')
d = {**d1, **d2}
print(d)

#################################################

print('\n')

#################################################

d1 = {'1' : 'One', '2' : 'Two'}

d2 = {'3' : 'Three', '4': 'Four'}

for i in (d1, d2):
    print(i)

#################################################

print('\n')

#################################################

# for same key sum value

print('\nExercise 5: ')

d1 = {'x' : 3, 'y' : 2, 'z' : 1}

d2 = {'w' : 8, 't' : 7, 'z' : 5}

for i, j in d2.items():
    if i in d1:
        d1[i] += d2[i]
    else:
        d1.update({i : j})
print(d1)

#############################

######  HOMEWORK  ######

print('\nDictionary HomeWork: ')
person = {'phone' : {'home' : '01-4455', 'mobile' : '918-123456'}, 'name' : 'João', 'age' : 48, 'children' : ['Olivia', 'Sophia']}

print(len(person))
print(person['phone']['home'])
print(person['phone']['mobile'])

print(person['children'])
print(person['children'][0])

print(person.pop('age'))

#############################