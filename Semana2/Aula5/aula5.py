## -*- coding: utf-8 -*-

## João Martins

######################################

print('\nExemplos Aula 6: ')
print('\n')

print ('a2'.isidentifier()) # True
print ('2a'.isidentifier()) # False
print ('_myvar'.isidentifier())
print ('my_var'.isidentifier())
print ('my-var'.isidentifier())
print ('my var'.isidentifier())
print ('my$'.isidentifier())
print ('2a#'.isidentifier())

######################################

print('\n')

######################################

a = 5
b = 1
print('Five plus one is {a + b}')
print(f'Five plus one is {a + b}')

a = b = c = 5
print(a, b, c)

x = 1
y = 2
y , x = x, y
print(x)
print(y)

######################################

print('\n')

######################################

s = "Python Course"
print(type(s))

i = 2
print(type(i))

f = 2.5
print(type(f))

c = 2 + 3j
print(type(c))


print(bool(5))
print(bool(-2))
print(bool('Hamed'))

print(bool(0))
print(bool(''))

print(bool([]))
print(bool({}))
print(bool(()))

b = True
c = 5 < 2

print(type(b))
print(type(c))
######################################

print('\n')

######################################

l = ["apples", "grapes", "oranges"]
print(type(l))

t = ("apple", "banana", "cherry")
print(type(t))

d = {'id' : '123', 'name' : 'farshid'}
print(type(d))

s = {'apple', 'banana', 'cherry'}
print(type(s))

######################################

print('\n')

######################################

a = int(input('Enter a:'))
b = int(input('Enter b:'))
c = a + b
print(c)

n = 12.5
print('%i' % n)
print('%f' % n)
print('%e' % n)

######################################

print('\n')

######################################
print(1 + 3)

print(5 - 3)

print(2 * 2)

print(3 / 2)

print(3 // 2)

print(17 % 5)

print(2 ** 3)
print(0 ** 0)
print(6 ** 0)

print(8 - 2 * 3)
print(1 + 3 * 4 / 2)
print(16 / 2 ** 2)

print(2**2**3)

x = 4
x += 2
print(x)

y = 8
y //= 2
print(y)

print(2 == 3)
print(2 != 3)
print(2 < 3)

print(1 < 3 or 4 > 5)
print(1 < 3 and 4 > 5)
print(not 1 < 3)

print(1 >= 2 and (5/0) > 2)

x = [1,2,3,4,5]
print(3 in x)
print(24 not in x)

a = 12
print(bin(a))

b = 14
print(bin(b))

c = a | b
print(bin(c))

c = a & b
print(bin(c))

c =  a ^ b
print(c)

a = 13
print(a << 1)

a = 20 
print(a >> 1)

a = 18
print(a >> 2)

s1 = 'Python'
s2 = ' Course'
s3 = s1 + s2
print(s3)

s = 'sara'
print(2*s)

######################################

print('\n')

######################################
a = 2
b = 5

m = a if a < b else b
print (m)

######################################

print('\n')

######################################
list1 = ['a', 'e', 'o', 'i', 'u']

s = 'yes' if ('o' in list1) else 'no'
print(s)
######################################

print('\n')

######################################
x = 2
y = 6

z = 1 + (x if x > y else y + 2)
######################################

print('\n')

######################################
name = 'farshid'
v = 'aeiou'
c = 0
for ch in name:
    if ch in v:
        print(ch)
        c += 1
print(c)

############# OU ################
name = 'farshid joe sara'
v = 'aeiou'

m = []
for ch in name:
    if ch in v:
        m.append(ch)
print(set(m))

############# OU ################

name = 'farshid'
v = 'aeiou'

a = [ch for ch in name if ch in v]
print(a)

######################################

print('\n')

######################################

##### LOOPS #######

for i in range(1, 4):
    for j in range(2, 4):
        print(j, end =' ')
    print()
######################################

print('\n')

######################################
for i in range(5):
    if i == 3:
        break
    else:
        print(i, end = ' ')
        
######################################       

print('\n')

######################################
for i in range(5):
    if i == 3:
        continue
    else:
        print(i, end = ' ')       
 
######################################        

print('\n')

######################################        
i = 1

while i <= 3:
    print(i, end = ' ')
    i += 1

######################################

print('\n')

######################################

n = 7
while n >= 3:
    print(n, end = ' ')
    n -= 1

######################################

print('\n')

######################################
s = 'abcdef'
i = 0

while True:
    if s[i] == 'd':
        break
    print(s[i], end = ' ')
    i += 1
    
######################################

print('\n')

######################################

n = 8
while n > 2:
    n -= 1
    if n == 5:
        break
    print(n, end = ' ')
    
###

n = 8
while n > 2:
    n -= 1
    if n == 5:
        continue
    print(n, end = ' ')

######################################

print('\n')

######################################
n = 1
while n <= 3 : print(n) ; n += 1


n = 1
while n <= 3:
    print(n)
    n += 1

######################################

print('\n')

######################################
import random

n = random.randrange(0, 10)
f = 'no'

print(n)

while f == 'no':
    a = int(input('Game: guess number between 0 to 9: '))
    
    if(a < n):
        print('increase')
    elif (a > n):
        print('decrease')
    else:
        print('Correct, You won')
        f = 'yes'
print('Thank you.')
    
########################################

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
       