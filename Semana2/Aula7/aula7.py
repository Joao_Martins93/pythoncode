#-*- coding: utf-8 -*-

# João Martins

#################################################

# a = 'joao'

# print(a[0])

# a[0] = 'b'


#################################################

###########        LIST        #############

print('\nList Examples: ')

a = [5, 7, 12]
print(a[0])
print(a[1])
print(a[2])

print(type(a))
print(len(a))

b = [1.618, 'Python Course', 0, {'Joe' : 21}, [], (3, 6, 9)]
print(b)

print('\n')

print(dir(a))

print('\n')

print(dir(list))

print('\n')


my_list = ['p', 'r', 'o', 'b', 'e']

print(my_list[-1])
print(my_list[-5])
print(my_list[0])

print('\n')
a = [5, 7, 12]

print(a.index(7))

print(a[1])

a[1] = 8

s = 'sara'

print(s[1])

print('\n')

# s[1] = 'd'  not mutable

a = [1, 2]
b = [2, 1]

print(a == b)

print('\n')

## show items in a list

friends = ['Hamed', 'Josi', 'Stefan']

for f in friends:
    print(f)
    
print('\n')

for i in range(3):
    print(friends[i])    


#################################################


print('\n')


#################################################

print('\nExercise 1: ')



# my_list = [1, 2, 23, 4, 'word']           ###############
# for i in [0, 1, 2, 3, 4]:                 #
#     print(my_list[i], my_list[i+1])       #

# print("\nOR: ")                           # error in those examples:
# print("\n")                               #   index out of range

# my_list = [1, 2, 23, 4, 'word']           #
# for i in range(len(my_list)):             #
#     print(my_list[i], my_list[i+1])       ################

my_list = [1, 2, 23, 4, 'word']
for i in [0, 1, 2, 3, 4]:
    if i == len(my_list) - 1:
        break
    print(my_list[i], my_list[i+1])


print("\nOR: ")
print("\n")


my_list = [1, 2, 23, 4, 'word']
for i in range(len(my_list) - 1):
    print(my_list[i], my_list[i+1])



print("\nOR: ")
print("\n")

my_list = [1, 2, 23, 4, 'word']
for i in [0, 1, 2, 3]:
    print(my_list[i], my_list[i+1])
    

print("\nOR: ")
print("\n")

my_list = [1, 2, 23, 4, 'word']
for i in [0, 1, 2, 3]:
    print(my_list[i], my_list[i+1])
    if i == 3:
        break


print("\nOR  if: ")
print("\n")



my_list = [1, 2, 23, 4, 'word']
for i in range(len(my_list)-1):
    
    if(i <= len(my_list)):
      print(my_list[i], my_list[i+1])

#################################################


print('\n')


#################################################

# Examples

a = [7, 5, 30, 2, 6, 25]

print(a[1 : 4])         # [5, 30, 2]
print(a[ : 3])          # [7, 5, 30]
print(a[3 : ])           # [2, 6, 25]

print(a[3 : 0])         # []
print(a[3 : 0 : -1])    # [2, 30, 5]
print(a[: : -1])        # [25, 6, 2, 30, 5, 7]

print(a[0 : 7 : 2])     # [7, 30, 6]
print(a[6 : 0 : -2])    # [25, 2, 5]
print(a[50 : 0 : -2])   # [25, 2, 5]
print(a[ : 0 : -2])     # [25, 2, 5]

print(a[3 : 5])         # [14, 15]

print(a)                #[7, 5, 30, 14, 15, 25]

#################################################


print('\n')


#################################################

a = [4, 7]
b = a*2
print(b)    # [4, 7, 4, 7]

a = [1, 2]
b = ['a', 'b', 'c']
c = a + b

print(c)    # [1, 2, 'a', 'b', 'c']

#################################################


print('\n')


#################################################

a = [7, 5, 30, 2, 6, 25]

print("\n14 in [7, 5, 30, 2, 6, 25]", 14 in a)
print("\n14 not in [7, 5, 30, 2, 6, 25]", 14 not in a)

#################################################


print('\n')


#################################################

f = [3, [109, 27], 4, 15]

print(f[1])     # [109, 27]
print(f[1][1])  # 27
print(len(f))   # 4

#################################################


print('\n')


#################################################

print('Exercise 2: ')

a =[7, 5, 30, 2, 6, 25]

maxx = 0

for i in a:
    if(i > maxx):
        maxx = i
print("\nMAXX: ", maxx)

print('\nOr: ')

print("\nMAX: ", max(a))   # 30
print("\nMIN: ", min(a))   # 2
print("\nSUM: ", sum(a))   # 75

#################################################


print('\n')


#################################################

s = 0
for i in a:
    s += i
print("\nSum: ", s)

#################################################


print('\n')


#################################################


a = [1, 3, 6, 5, 3]
print(a.count(3))   # 2


#################################################


print('\n')


#################################################

a = [1, 2, 6, 5, 2]
a.insert(2, 13)      # inser(index, obj)
print(a)             # [1, 2, 13, 6, 5, 2]

#################################################


print('\n')


#################################################

a = [1, 2, 6, 5, 2]
a.remove(2)
print(a)
a.remove(2)
print(a)

#################################################


print('\n')


#################################################

x = [10, 15, 12, 8]
a = x.pop()
print(a)
print(x)

#################################################


print('\n')


#################################################

y = ['a', 'b', 'c']
p = y.pop(1)
print(p)
print(y)

#################################################


print('\n')


#################################################

a = [5, 9, 3]
del a[1]
print(a)

#################################################


print('\n')


#################################################

a = [0, 1, 2, 3, 4, 5, 6]
del a[2:4]
print(a)

#################################################


print('\n')


#################################################

a = [1, 2, 3]
print(a)
print("\na[::-1]: ", a[::-1])
a.reverse()
print("\nREVERSE: ", a)


b = a.reverse()
print(b)

#################################################


print('\n')


#################################################

a = [2, 4, 3, 5, 1]
a.sort()
print("\nSORT: ", a)

#################################################


print('\n')


#################################################

x = [1, 2, 3]
x.extend([5])
print("\nEXTEND: ", x)

#################################################


print('\n')


#################################################

x = [1, 2, 3]
y = [4, 5]
x.extend(y)
print(x)
print("\nLEN X: ",  len(x))
print("\nLEN Y: ", len(y))

#################################################


print('\n')


#################################################

a = [1, 2, 3]
a.append(4)
print(a)

#################################################


print('\n')


#################################################

x = [1, 2, 3]
y = [5, 6]
x.append(y)
print('Append: ', x)
print(len(x))
print(len(y))

#################################################


print('\n')


#################################################


a = []

for i in range(4):
    a.append(i)
print(a)


#################################################


print('\n')


#################################################

a = [1, 2, 3]

a.clear()
print(a)
print(len(a))


#################################################


print('\n')


#################################################

a = [1, 2, 3]
b = [6]
a.append(b)
print(a)
c = a[-1][0]
d = a[-1]
print(c)
print(d)

#################################################


print('\n')


#################################################

a = [1, 2, 3]
a.clear()
print(a)

#################################################


print('\n')


#################################################

a = [1, 2, 3]
b = a.copy()
print(b)

#################################################


print('\n')


#################################################

a = [1, 2, 3]
b = a.copy()
c = a
d = a[:]

print("\nA: ", a)
print("\nB: ", b)
print("\nC: ", c)
print("\nD: ", d)

#################################################


print('\n')


#################################################

a[1] = 22
c[0] = 11
d[2] = 33

print("\nNEW A: ", a)
print("\nNEW C: ", c)
print("\nNEW D: ", d)

#################################################


print('\n')


#################################################

x = 2
y = x
y += 1
print(x)
print(y)

#################################################


print('\n')


#################################################

x = []
y = x
y.append(5)
print(x)
print(y)

#################################################


print('\n')


#################################################

a = [i for i in range(4)]
print(a)

###########

a = [i*2 for i in range(4)]
print(a)

###########

a = [i*i for i in range(3, 6)]
print(a)

#################################################


print('\n')


#################################################

a = [1, -2, 5, -56, 8]
b = [abs(i) for i in a]
print(b)

#################################################


print('\n')


#################################################

import math
a = [round(math.pi, i) for i in range(1, 5)]    # aumenta casas decimais
print(a)

#################################################


print('\n')


#################################################

a = ('$ali sara$')
b = a.strip('$')

print(a)
print("\nSTRIP($): ", b)

print('\nOr: ')

a = ['$ali', 'sara$']
b = [i.strip('$') for i in a]
print(b)

#################################################


print('\n')


#################################################

a = '$$$$$$$$$$$$$$##JOAO11$$'
b = list(a)     #convert string to list
print(a)
print(b)

c = ''.join(b)  #convert list to string
print(c)

#################################################


print('\n')


#################################################

a = [11, 8, 14, 20, 2]

b =  [i for i in a if i < 10]
print(b)

#################################################


print('\n')


#################################################

print('Exercise 3: ')

a = [1, 2]
b = [1, 4, 5]
c = []

for i in a:
    for j in b:
        if i != j:
            c.append((i, j))
print(c)

#################################################


print('\n')


#################################################

a = [2.6, float('NaN'), 4.8, 6.9, float('NaN')]
b = []

import math

for i in a:
    if not math.isnan(i):
        b.append(i)
print(b)

#################################################


print('\n')


#################################################

a = [1, 2, 3, 4]
for i in a:
    if i > 2:
        a.pop()
print(a)

#################################################


print('\n')


#################################################

a = [1, 2, 3, 4]
for i in a:
    if i < 2:
        a.pop()
print(a)

#################################################


print('\n')


#################################################

a = [1, 2, 3, 4]
for i in a:
    if i == 2:
        a.pop()
print(a)

#################################################


print('\n')


#################################################

# a = [1, 2, 3, 4]          ##############
# for i in range(len(a)):   # program get error because one element
#     if a[i] > 1:          # is pop and the len(a) is -1 each pop
#         a.pop(i)          #  error : list index out of range
# print(a)                  ##############

#################################################


print('\n')


#################################################

a = [1, 2, 3, 4]
for i in range(len(a)):
    if a[i] == 2:
        a[i] = 0
print(a)

#################################################


print('\n')


#################################################

a = [1, 2, 3, 4]
for i in range(len(a) - 1):
    if a[i] == 2:
        a.pop(i)
print(a)

#################################################


print('\n')


#################################################

##########  HOMEWORK ###################

a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
  #    0           1          2

size = len(a)

  # [1, 2, 3]
  # [4, 5, 6]
  # [7, 8, 9]


print("\nFirst row: ", a[0][:])
print("\nFirst column: ", a[0][0], a[1][0], a[2][0])

print("\nFirst row: ", a[0])

##########################################
print("\n")
print("1- ")
print("First row: ")
a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
size = len(a)
for i in range(size):
    for j in range(size):
        if i == 0:
            print(a[i][j], end= ' ')

# or 

print('\nOr: ')
for j in range(size):
    print(a[0][j], end = ' ')

##########################################        
print("\n")
print("2- ")
print("First column: ")
for i in range(size):
    for j in range(size):
        if j == 0:
            print(a[i][j], end= ' ')

##########################################
print("\n")
print("3- ")
print("Main diameter: ")
for i in range(size):        
    print(a[i][i], end= ' ')

##########################################
print("\n")
print("4- ")
print("Another diameter: ")
for i in range(size):
    print(a[size-1-i][i], end= ' ')

##########################################
print("\n")
print("5- ")
print("Sum of rows: ")

soma1 = soma2 = soma3 = 0

for i in range(size):
    soma1 += a[0][i]
print('\nRow 1: ', soma1)


for i in range(size):
    soma2 += a[1][i]
print('\nRow 2: ', soma2)

for i in range(size):
    soma3 += a[2][i]
print('\nRow 3: ', soma3)


print('\nOr: ')

print('\nRow 1: ', sum(a[0]))
print('\nRow 1: ', sum(a[1]))
print('\nRow 1: ', sum(a[2]))

print('\nOr: ')

soma = 0
for i in range(size):
    for j in range(size):
        soma += a[i][j]
    print(f'\nRow {i+1}: ', soma)
    soma = 0

##########################################

print("\n6- ")
print("Sum of columns: ")

soma1 = soma2 = soma3 = 0

for i in range(size):
    soma1 += a[i][0]
print('\nColumn 1: ', soma1)


for i in range(size):
    soma2 += a[i][1]
print('\nColumn 2: ', soma2)

for i in range(size):
    soma3 += a[i][2]
print('\nColumn 3: ', soma3)

print('\nOr: ')

soma = 0
for i in range(size):
    for j in range(size):
        soma += a[j][i]
    print(f'\nColumn {i+1}: ', soma)
    soma = 0
##########################################








