# -*- coding: utf-8 -*-

## João Martins

#########################################

print('\nExemplo slide 5: ')

for i in [5, 4, 3, 2, 1] :
    print(i)
print('Boom!!')

#########################################

#########################################

print('\nExemplo slide 6: ')

friends = ['Ana', 'Filipe', 'João']
for friend in friends : 
   print('Congrats on your new job:', friend)
print('Done!')

#########################################

#########################################

print('\nExemplo slide 8: ')
print('Before')
for i in range(5):
    print(i)
print('After')

#########################################

#########################################

print('\nExemplo slide 9: ')
print('Before')
for i in range(2, 6):
     print(i)
print('After')

# e

print('Before')
for i in range(15, 0, -5):
    print(i)
print('After')

#########################################

#########################################

print('\nExemplo slide 11: ')
print('Before')
for thing in [9, 41, 12, 3, 74, 15] :
     print(thing)
print('After')

#########################################

#########################################

print("\nExemplos: ")
friends = ['eu', 'tu', 'ele']

for friend in friends:
    print("F: ", friend)
print("BOOM!")


print("Before")
for i in range(1000, 1400):
    print(i)
print("After")

print("Before")
for i in range(15, 0, -5):
    print(i)
print("After")

print("Before")
for i in range(1000, 1400):
    print(i)
print("After")

##############################################################
## Ex 1
##############################################################

print("\nEx 1: ")
string = "The Best of made in Portugal - Hats, Soaps, Shoes, Tiles & Ceramics, Cork"

print("\nExemplo 1: ")
up = ""
for char in string:
    if char.isupper():
        up += char
print(up)


print("\nExemplo 2: ")
up = ""
for char in string:
    if ord(char) in range(65, 90):
        up += char
print(up)


###############################################################


###############################################################
## Ex 2
###############################################################

print("\nEx 2: ")
for i in range(0, 50):
    if(i % 4 == 0):
        print(i)
###############################################################
        
## exemplos

print("\nMais exemplos: ")


print("\n1 ->")
str = "Joseanne is a good teacher"

print(str.count("a"))

#########

print("\n2 ->")
str1 = "Title is something"
str2 = str1.lstrip('is')


print(str1.lstrip("is"))


print(str1.partition(" "))

#########

print("\n3 ->")

largest_so_far = -1
print("Before", largest_so_far)

for num in [9, 41, 12, 3, 74, 15]:
    if num > largest_so_far:
        largest_so_far = num
    print (largest_so_far, num)
    
print("After", largest_so_far)
#################################

print('\nExemplo slide 16: ')

loop_interaction = 0
print('Before', loop_interaction )
for thing in [9, 41, 12, 3, 74, 15]:
    loop_interaction = loop_interaction + 1
    print(loop_interaction , thing)
print('After', loop_interaction )
#################################

print('\nExemplo slide 17: ')

sum = 0
print('Before', sum)
for thing in [9, 41, 12, 3, 74, 15] :
    sum = sum + thing
    print(sum, thing)
print('After', sum)
#################################

#################################

print('\nExemplo slide 18: ')
count = 0
sum = 0
print('Before', count, sum)
for value in [9, 41, 12, 3, 74, 15] :
    count = count + 1
    sum = sum + value
    print(count, sum, value)
average_value = sum / count
print('After', count, sum, average_value)

#################################

print('\nExemplo slide 19: ')

print('Before')
for value in [9, 41, 12, 3, 74, 15] :
    if value > 20:
 	    print('Large number',value)
print('After')

#################################


#################################

print('\nExemplo slide 20: ')

found = False
print('Before', found)
for value in [9, 41, 12, 3, 74, 15] : 
   if value == 3 :
       found = True
   print(found, value)
print('After', found)

#################################

#################################

print('\nExemplo slide 21: ')

smallest = None
print('Before')
for value in [9, 41, 12, 3, 74, 15] :
    if smallest is None : 
        smallest = value
    elif value < smallest : 
        smallest = value
    print(smallest, value)
print('After', smallest)


#################################


#################################

print("\n4 ->")
list = [1, 2, 4, 6, 7, 6]
print(len(list))

#################################

print("\n5 ->")
found = False
print('Before', found)
for value in [9, 41, 12, 3, 74, 15]:
    if value == 3:
        found = True
    else:
        found = False
    print(found, value)

##########    

print("\n6 ->")    
palavra = None
print('Before') 
for value in [9, 41, 12, 3, 74, 15]:
    if palavra is None :
        palavra = value
    elif value < palavra :
        palavra = value
    print(palavra, value)
print('After', palavra)
########