#-*- coding: utf-8 -*-

#João Martins

######################################################################
# ex 1
######################################################################

#a) int
#b) int
#c) float
#d) string
######################################################################


######################################################################
# ex 2
######################################################################
print("\n")
print("\nEx 2: ")
word = input("Insert a word: ")

reverse = word[::-1]

print(reverse)

######################################################################


######################################################################
# ex 3
######################################################################
print("\n")
print("\nEx 3: ")
# a)
S = "abc"

sizeS = len(S)

# b)
S1 = (S[0] * sizeS) + (S[1] * sizeS) + (S[2] * sizeS)
print(S1)

######################################################################


######################################################################
# ex 4
######################################################################

#************************************************
# a)
print("\n")
print("\nEx 4: ")
print("a - ")
S = "aaabbbccc"

i = 0
posB = 0
posCCC = 0
while (i < len(S)):
    
    
    if(S[i] == 'b' and S[i-1] != 'b'):
        posB = i
      
    elif((S[i] == 'c') and (S[i+1] == 'c') and (S[i+2] == 'c')):
        posCCC = i
        break
    
    i = i + 1

print("\nB :", posB)
print("\nCCC :", posCCC)
#++++++++++++++++++++++++++++++++++++++++++

#Ou esta versão, pois não entendi bem este exercicio
print("\na - segunda versão: ")
# a)
S1 = S[S.index('b') : S.index('b') + 1]
S2 = S[S.index('c') : S.index('c') + 3]

print("\nB :", S1)
print("\nC :", S2)
#**********************************************************

print("\nb -")
# b)
S3 = S.replace("a", "X")
        
S4 = S.replace("a", "X", 1)
print (S3)
print(S4)
######################################################################


######################################################################
# ex 5
######################################################################
print("\n")
print("\nEx 5: ")
string = "aaa bbb ccc"

print("\n1 - ")
# 1-
str1 = string.replace("a", "A")
str1 = str1.replace("b", "B")
str1 = str1.replace("c", "C")

print("\nSTR1 : ", str1)


print("\n2 - ")
# 2-

str2 = string.replace("a", "A")
str2 = str2.replace("c", "C")
print("\nSTR2 : ", str2)

######################################################################


######################################################################
# ex 6
######################################################################

a = 10
b = 10
c = 10
d = 9

######################################################################


######################################################################
# ex 7
######################################################################
print("\n")
print("\nEx 7: ")
x = input("X :")
y = input("Y :")

x,y = y,x

print("\nX :", x)
print("\nY :", y)

######################################################################


######################################################################
# ex 8
######################################################################

# uma solução correta seria a seguinte pois a do exercicio está errada
print("\n")
print("\nEx 8: ")
print ("\nEnter a number  :")
a = int ( input ())
if a % 2 == 0 and a < 100:
      print ("the number is even and smaller than 100")
elif a % 2 == 0 and a >= 100:
                print ("The number is even and equal or higher than 100")
elif a % 2 != 0 and a < 100:
        print ("The number is odd and smaller than 100")
else:
          print ("The number is odd and equal or higher than 100")


######################################################################


