## -*- coding: utf-8 -*-

## João Martins

##########################################################################
## Ex 1
##########################################################################
print("\nEx 1: ")

print("\n(5 > 4) and (3 == 5):                ", (5 > 4) and (3 == 5))
print("not (5 > 4):                         ", not (5 > 4))
print("(5 > 4) or (3 == 5):                 ", (5 > 4) or (3 == 5))
print("not ((5 > 4) or (3 == 5)):           ", not ((5 > 4) or (3 == 5)))
print("(True and True) and (True == False): ", (True and True) and (True == False))
print("(not False) or (not True):           ", (not False) or (not True))
##########################################################################



##########################################################################
## Ex 2
##########################################################################
print("\nEx 2: ")

spam = 0
if spam == 10:                                                 ######## block
      print('eggs')                                            ########   3  
      if spam > 5:                              ###### block
            print('bacon')                       ######   2
      else:                 ############ block 
          print('ham')      ############   1
      print('spam')                                            ########   3
print('spam')


print("\nBloco 1 -> (if spam == 10)")
print("\nBloco 2 -> (if spam >5 )")
print("\nBloco 3 -> (else)")
#########################################################################



#########################################################################
## Ex 3
#########################################################################

print("\nEx 3: ")

print("\nExemplo 1: ")

#list1 = [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]
list1 = []
size = int(input("Enter how many numbers your want: "))
i = 0
r1 = 0
r2 = 0
r3 = 0
r4 = 0

while i < size:
    number = int(input("Enter number: "))
    list1.append(number)
    if number in range(0, 26):
        r1 = r1 + 1
    if number in range(26, 51):
        r2 = r2 + 1
    if number in range(51, 76):
        r3 = r3 + 1
    if number in range(76, 101):
        r4 = r4 + 1 
    i = i + 1
        
print("\nList : ", list1)

print("\n1 [0 ,25]: ", r1)
print("2 [26 ,50]: ", r2)
print("3 [51 ,75]: ", r3)
print("4 [76 ,100]: ", r4)

###################### OU #########################

print("\n OU ")
print("\nExemplo 2: ")

size = int(input("Enter how many number you want: "))
i = 0

r1 = 0
r2 = 0
r3 = 0
r4 = 0

while i < size:
    number = int(input("Enter number: "))
    if number in range(0, 26):
        r1 = r1 + 1
    if number in range(26, 51):
        r2 = r2 + 1
    if number in range(51, 76):
        r3 = r3 + 1
    if number in range(76, 101):
        r4 = r4 + 1     
    i = i + 1
     
print("\n1 [0 ,25]: ", r1)
print("2 [26 ,50]: ", r2)
print("3 [51 ,75]: ", r3)
print("4 [76 ,100]: ", r4)

####################### OU ############################

print("\n OU ")
print("\nExemplo 3: ")

r1 = 0
r2 = 0
r3 = 0
r4 = 0
for i in [2, 61, -1, 0, 88, 55, 3, 121, 25, 75]:
    if i in range(0, 26):
        r1 = r1 + 1
    if i in range(26, 51):
        r2 = r2 + 1
    if i in range(51, 76):
        r3 = r3 + 1
    if i in range(76, 101):
        r4 = r4 + 1     
   
    
print("\n1 [0 ,25]: ", r1)
print("2 [26 ,50]: ", r2)
print("3 [51 ,75]: ", r3)
print("4 [76 ,100]: ", r4)
    
#########################################################################



#########################################################################
## Ex 4
#########################################################################

print("\nEx 4: ")

string = "Catching up\n\nStart with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin),\na one-volume introduction ranging from geography and history to wine and poetry,\nand Portugal: A Companion History (4) by José H Saraiva (Carcanet Press), a\nbestselling writer and popular broadcaster in his own country."
print("\nNormal String: ")
print("\n")
print(string)
print("\n")


print("\nEx Capitalize: ")
print(string.capitalize())

print("\nEx Expandtabs: ")
print(string.expandtabs())

print("\nEx Isalnum: ")
print(string.isalnum())

print("\nEx Isalpha: ")
print(string.isalpha())

print("\nEx Isdigit: ")
print(string.isdigit())

print("\nEx Islower: ")
print(string.islower())

print("\nEx Isspace: ")
print(string.isspace())

print("\nEx Istitle: ")
print(string.istitle())

print("\nEx Isupper: ")
print(string.isupper())

print("\nEx Partition(P): ")
print(string.partition("P"))

#########################################################################



#########################################################################
## Ex 5
#########################################################################
print("\nEx 5: ")
number = int(input("Enter a decimal number to convert to binary: "))
binary = int(bin(number)[2:])
print(f"\nBinary number of {number}: ", binary)
#########################################################################


















