# -*- coding: utf-8 -*-

# João Martins


###########################################################
# ex 1
###########################################################


n1 = input("\nEnter first number: ")
n2 = input("\nEnter second number: ")
c = input("\nEnter the operator: ")


while c != '+' and c != '-' and c != '/' and c != '*':
    c = input('\nEnter a valid operator (+-*/): ')


str = n1+c+n2
print(f'\n{n1} {c} {n2}')

res = eval(str)
print("\nRES : ", res)


###########################################################


###########################################################
# ex 2
###########################################################

print("\n1 - Pão Alentejano")
print("2 - Bolo Lêvedo [dos Açores]")
print("3 - Bolo do Caco [da Ilha da Madeira]")
print("4 - Broa")
print("5 - I want to leave")

opcao = int(input("Enter an option from the menu (1 to 5) : "))

while (opcao >= 1 and opcao < 5):
    
    if(opcao == 1):
        print("\nPão Alentejano")
    if(opcao == 2):
        print("\nBolo Lêvedo [dos Açores]")
    if(opcao == 3):
        print("\nBolo do Caco [da Ilha da Madeira]")
    if(opcao == 4): 
        print("\nBroa")
    opcao = int(input("Enter an option from the menu (1 to 5) : "))   
    
print("\nBYE!")        
###########################################################       
        
        
        
###########################################################
# ex 3
###########################################################

x = 5 + ord('J')
y = 0

while True:
    y = (x % 2) + 10 * y
    x = x // 2
    print("x = ", x, "y = ", y)
    if x == 0:
        break 

    while y != 0:
        x = y % 100
        y = y // 10
        print("x = ", x, "y = ", y)
###########################################################


###########################################################
# ex 4 
###########################################################

n = int(input("Enter a number : "))

i = 0        

while i < n:
    
    print(' '*i , i+1)  
    i = i + 1
###########################################################













        
        
        
        
        