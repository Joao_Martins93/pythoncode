# -*- coding: utf-8 -*-

##  João Martins

#############################################################

print("\nExemplos da aula: ")
print("\n")
#############################################################
print("\nSlide 7: ")
def thing():
    print("Hello")
    print("Fun")
    
    
thing()
print('Zip')
thing()
#############################################################

#############################################################

print('\nSlide 9: ')

def music_lyrics():
    print("We are here")
    print("We are here for all of us")
#################################################

print('\nSlide 10: ')

number = int('2', 'Josi', 1.90)

##################################################

print('\nSlide 12: ')

def greet():
    return "Hello"

print(greet(), 'Rodrigo')
print(greet(), 'João')
##################################################

print('\nSlide 13: ')

def my_func(str):
    print('blah')
    print('blah')
    for x in str:
      print('blah')
      print('blah')
    return 'w'

result = my_func('Hello world')
print(result)


#############################################################
print("\n2 ->")
def volume_sphere():
    
    pi = 3.1415
    radius = float(input("Please, Enter radius: "))
    const = 4/3
    power = 3
    volume = (const * pi * (radius**power))
    print(f'{volume:.3f} U')
    
    
volume_sphere()
#############################################################



#############################################################
print("\n3 ->")
import math

def volume_sphere(radius, power, const):    
    
    volume = (const * math.pi * (radius**power))
    print(f'{volume:.3f} U')

const = 1
power = 3   
radius = float(input("Please, Enter radius: "))   
volume_sphere(radius, power, const)
#############################################################

print("\n")

#############################################################
print("\n4 ->")

def food(vegetable):
    if vegetable == 'tomato':
        print('I bought tomato')
    elif vegetable == 'orange':
        print("I bought orange")
    else:
        print("I bought other vegetable")
        
food('tomato')
#############################################################

print("\n")

# ###########################################################
#import math

print("\n5 ->")

def volume_sphere(radius, power, const):    
    
    volume = (const * math.pi * (radius**power))
    return volume

const = 1
power = 3   
radius = float(input("Please, Enter radius: "))   
volume = volume_sphere(radius, power, const)
print(f"{volume:.3f} U")
###############################################################

print("\n")

###############################################################
print("\n6 ->")

def addtwo(a, b):
    add = a + b
    sub = a - b
    mult = a * b
    div = a/ b
    return add, sub, mult, div

x = addtwo(6, 2)
print(x)
print(x[0])
print(x[1])

#################################################################

print("\n")

#################################################################
print("\n7 ->")

def food():
    eggs = 'food local'
    print(eggs)
    
def more_food():
    eggs = 'more_food local'
    print(eggs)
    food()
    print(eggs)
    
eggs = 'global'
more_food()
print(eggs)
#################################################################    

print("\nSlide 16: ")
a = 21 
b = 10 
c = 0

c = a + b
print(c)
c += a
print(c) 
c *= a
print(c)
c /= a 
print(c)
c = 2 
print(c)
c %= a 
print(c)
c **= a
print(c)
c //= a
print(c)

#################################################################

print('\nSlide 17: ')

a = 10           # 10 =  1010 
b = 3            # 3 =   0011 

hifen =30
binary = bin(a & b); 
denary = a & b
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a | b); 
denary = (a | b)
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a << 2)
denary = a << 2;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a >> 2); 
denary = a >> 2;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(~a); 
denary =~a;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)


binary = bin(a ^ b); 
denary =a ^ b;
print ("Line 1 - Value of binary is ", binary)
print ("Line 1 - Value of denary is ", denary)
print("-"*hifen)



#################################################################
## Ex 1
#################################################################

print("\nEx 1: ")

num = int(input("Enter a number: "))
print("\n")

for i in range(num, 0, -1):
    if(i % 2 == 0):
        print("Even number : ", i)
    else:
        print("Odd number  : ", i)
        
print("\n OU : ")
        

while num > 0:
    if(num % 2 == 0):
        print("Even number : ", num)
    else:
        print("Odd number  : ", num)
    num -= 1
#################################################################


#################################################################
## Ex 2
#################################################################

print("\nEx 2: ")
def mean(v):
    soma = 0
    res = 0
    for i in v:
        soma +=  i
    res = soma / len(v)
    return res


res = mean([2, 61, -1, 0, 88, 55, 3, 121, 25, 75])
print("\nAritmathic mean : ", res)


##################### OU ########################

print("\nOU :")
def mean(v):
    soma = 0
    res = 0
    for i in v:
        soma +=  i
    res = soma / len(v)
    return res


list1=[]
size = int(input("\nHow many numbers you want? "))
for i in range(size):
    number = int(input("Enter number: "))
    list1.append(number)


res = mean(list1)
print("\nAritmathic mean : ", res)



