# -*- coding: utf-8 -*-

# João Martins

#print("Name: ")
#name1 = input()
#name2 = input()
#name3 = input()
#print(" ")

#print(name1)
#print(name2)
#print(name3)
######################################################################


######################################################################
# ex 4
######################################################################

print("\nEx 4: ")
name1, name2, name3 = input("Enter three names: ").split()

print(name1)
print(name2)
print(name3)
######################################################################


######################################################################
# ex 5
######################################################################

print("\nEx 5: ")
totalMoney = 1000
quantity = 3
price = 450

statement1 = "I have {1} dollars so i can buy {0} football for {2:.2f} dollars."
print(statement1.format(quantity, totalMoney, price))

print(('I have {0} dollars so i can buy {1} football for {2:.2f} dollars.').format(totalMoney, quantity, price))

print(f'I have {totalMoney} dollars so i can buy {quantity} football for {price:.2f} dollars.')
#######################################################################


#*********************************************
#input:
#    student_name
#    student_age
#outputs
#    <student_name and age>
#This code shows the student name and age
##**********************************************


#######################################################################
# ex 6
#######################################################################

print("\nEx 6: ")

msg = "Lisbon is in Portugal"

print(msg[1])               #i

print(msg[0:6])             #Lisbon
print(msg[3:5])             #bo
print(msg[0:9])             #Lisbon is
print(msg[:9])              #Lisbon is

print(msg[10:14])           #in P
print(msg[10:])             #in Portugal

print(msg[:6])              #Lisbon
print(msg[6:])              # is in Portugal

print(msg[:6] + msg[6:])    #Lisbon is in Portugal
print(msg[:])               #Lisbon is in Portugal
print(msg)                  #Lisbon is in Portugal

print(msg[18] + msg[14] + msg[20]) #gol
country = msg[13:21]
print(country)              #Portugal


#######################################################################


#######################################################################
# ex7
#######################################################################

print("\nEx 7: ")

msg = "Lisbon is in Portugal" #23 lettters

print(msg[-21:-15])  #Lisbon
print(msg[-8:-5])
print(msg[-14:-12])
print(msg[-8:])

print(msg[-11:-8])
print(msg[-23:])

print(msg[-5:-1])
print(msg[7:])

print(msg[:])
#######################################################################


#######################################################################
# ex 8
#######################################################################

print("\nEx 8: ")

a = 12
b = 3

print(a + b)    # 15
print(a - b)    # 9
print(a * b)    # 36
print(a / b)    # 4.0
print(a // b)   # 4 integer division, rounded down towards minus infinity
print(a % b)    # 0 modulo: the remainder after integer division

print()
#######################################################################


#######################################################################
# ex 9
#######################################################################

print("\nEx 9: ")
number1 = int(input("Enter first number: "));
number2 = int(input("Enter second number: "));
print(f"{number1} + {number2} = ",number1 + number2)
#######################################################################


#######################################################################
# ex 10
#######################################################################

print("\nEx 10: ")
input1 = "xxxx----xxxx----5555---"



input2 = input1[input1.index('5') : input1.index('5') + len('5555') ]
input3 = input1[input1.index('5') : input1.index('5') + 4 ]
print(input1[16:20])
print(input2)
print(input3)



import sys
print("\nInteger value information: ",sys.int_info)
print("\nMaximum size of an integer: ",sys.maxsize) 
########################################################################





