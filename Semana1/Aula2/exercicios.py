# -*- coding: utf-8 -*-

# João Martins

#****************************************************
# ex 1
#****************************************************
print("\nEx 1:")
a = 3
b = 2

print("\na / b: ", a / b)
print("a // b: ", a // b)
print("a % b: ", a % b)
print("a ** b: ", a ** b)
#****************************************************


#****************************************************
# ex 2
#****************************************************
print("\n\nEx 2:")
print("\nMedia 1: ", (2 + 4)/2)
print("Media 2: ", (4+8+9)/3)
print("Media 3: ", (12 + (14/6) + 15)/ 3)
#****************************************************


#****************************************************
# ex 3
#****************************************************
print("\n\nEx 3:")
pi = 3.1415
r = 5

print("\nVolume sphere: ", (4/3) * pi * r ** 3)

# ou

raio = 5.3
print(f'{4/3*pi*(raio**3):.3f} U')

# ou

radius = float(input("Please, Enter radius: "))
const = 4/3
power = 3
volume = (const * pi * (radius**power))
print(f'{volume:.3f} U')


#****************************************************


#****************************************************
# ex 4
#****************************************************
print("\n\nEx 4:")
print("\n1 % 2: ", 1 % 2)
print("5 % 2: ", 5 % 2)
print("20 % 2: ", 20 % 2)
print("60/7 % 2: ", (60/7) % 2)
#****************************************************

#####################################################

print('\nExercicios slide 13: ')
print(4==5)
print(6>7)
print(15<100)
print('hello' == 'hello')
print('hello' == 'Hello')
print('dog' != 'cat')
print( True == True)
print(True != False)
print( 42 == 42.0)
print( 42 == '42')
print('apple' == 'Apple') 
print('apple' > 'Apple') 
print('A unicode is', ord('A') ,' ,a unicode is' , ord('a’')) #unicode 

#####################################################

print("\nSlide 14: ")
minimum_age = 18
age = 27
if age > minimum_age:
    print("Congrats, you can get your license")
else:
    print("Please, come back in {0} years" .format(minimum_age - age)) 

#####################################################

print('\nSlide 15: ')
name = 'Josi'
password = 'swordfish'
if name == 'Josi':
   print('Hello Josi')
if password == 'swordfish':
   print('Access granted.')
else:
   print('Wrong password.')

#####################################################

print("\nExemplo slide 16: ")
minimum_age = 18
user_age = 27
if user_age < minimum_age:
    print("\nPlease, come back in {0} years" .format(minimum_age - user_age))
elif user_age == 150 :
    print("\nProbably, you can not drive anymore. Please, make an appointment with your doctor.")
else :
    print("\nCongrats, you can get your license.")  