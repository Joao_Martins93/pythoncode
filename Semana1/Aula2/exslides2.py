# -*- coding: utf-8 -*-

# João Martins

#****************************************************
# ex 1
#****************************************************
print("\nEx 1: ")
print("Exemplo 1: ")
number = 5

guess_number = int(input("\nEnter your guess: "))

if (guess_number < number):
    print("\nPlease, guess higher")
elif(guess_number > number):
    print("\nPlease, guess lower")
else:
    print("\nCongrats, you found the answer!!")

# OU

print('\nOr: ')

print("\nExemplo 2: ")
while (guess_number != number) :
    
    if (guess_number < number):
        print("\nPlease, guess higher")
    elif(guess_number > number):
        print("\nPlease, guess lower")
    guess_number = int(input("Enter another guess: "))

print("\nCongrats, you found the answer!!")
#****************************************************

print('\nExemplo slide 20: ')
print(4 < 5) and (5 < 6)
print(4 < 5) and (9 < 6)
print(1 == 2) or (2 == 2)
print(2 + 2 == 4 and not 2 + 2 == 5 and 2 * 2 == 2 + 2)

#####################################################


#****************************************************
# ex 2
#****************************************************
# a)
print("\nEx 2: ")
print("a-")
age = int(input("How old are you? "))

print("\nYour age: ", age)
#*****************************************************
# b)

print("\nb-")

if(age < 16):
    print("\nYou are too young to work, come back to school")
elif (age > 65):
    print("\nYou have worked enough, Let's Travel now")
else: #or if(age > 16 and age < 65)
    print("\nHave a good day at work")
#****************************************************

print('\nExemplo slide 23: ')
a = 3
b = 2
if a == 5 and b > 0:
    print('a is 5 and',b, 'is greater than zero.')
else:
    print('a is not 5 or',b, 'is not greater than zero.')


#####################################################

print("\nExemplo slide 24 resolvido: ")
day = "Saturday"
temperature = 30
raining = False
                                                    
if (day =="Saturday") and (temperature > 20) and ( raining == False ) :
    print("Go out ")
else:
    print ("Better finishing python programming exercises")

#####################################################

print('\nExemplo slide 26: ')

n = 5
while n > 0 :
    print('Time')
    print('ticking ')
    n -= 1
print('Stopped')

print('O erro neste código era não decrementar o n, ou seja ficava em loop infinito')

########

n = 0
while n > 0 :
    print('Time')
    print('ticking ')
print('Stopped')

print('Neste caso nunca entra no loop pois o valor de n é 0 e o loop só executa enquanto n for maior que 0')

#########################################################

print('\nExemplo slide 28: ')
while True:
    line = input('> ')
    if line == 'done' :
        break
    print(line)
print('Done!')

#########################################################

print('\nExemplo slide 31: ')

while True:
    line = input('> ')
    if line[0] == '#' :
        continue
    if line == 'done' :
        break
    print(line)
print('Done!')
#########################################################





