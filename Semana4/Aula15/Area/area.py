# -*- coding: utf-8 -*-

#######################################


######  Square operations  ######

def squareArea(x):
    print(f"Area of square with side {x}      : ", x * x)
    
def squarePerimeter(x):
    print(f"Perimeter of square with side {x} : ", x * 4)
    
 
    
 
######  Rectangle operations  ######

def retangulArea(h, b):
    print(f"Area of rectangle with base {b} and height {h}      : ", h * b)
    
def retangulPerimeter(h, b):
    print(f"Perimeter of rectangle with base {b} and height {h} : ", (h * 2) + (b * 2))




######  Circle operations  ######

def circleArea(r):
    import math
    print(f"Area of circle with radius {r}      : ", math.pi * (r ** 2))
    
    
def circlePerimeter(r):
    import math
    print(f"Perimeter of circle with radius {r} : ", 2 * math.pi * r)
    
   
 
######  Sphere volume  ######

def sphereVolume(r):
    import math
    print(f"Volume of sphere with radius {r}    : ", (4/3) * math.pi * (r ** 3) )
    