## -*- coding: utf-8 -*-

## João Martins

#################################################

print('\n')

#################################################

print('LISTS: ')

#################################################

print('\n')

#################################################

import sqlite3
from sqlite3 import Error
from numpy import *
import numpy as np

b = [4, 6, 7]
print(np.add(b, 5))

print(dir(np))

#################################################

print('\n')

#################################################


L = [3, True, 'ali', 2.7, [5, 8]]

a = [3, [109, 27], 4, 15]


print(a[1])

#################################################

print('\n')

#################################################


a = [7, 5, 30, 2, 6, 25]
print(a[1:4])

#################################################

print('\n')

#################################################

a = [1, 3, 6, 5, 3]
print(a.count(3))

#################################################

print('\n')

#################################################

a = []
for i  in range(4):
    a.append(i)
print(a)

#################################################

print('\n')

#################################################

print('\nTUPLES: ')

print('\n')


t = ('English', 'History', 'Mathematics')
print(t[1])
print(t.index('English'))

#################################################

print('\n')

#################################################


t = (1, 9, 3, 9)
print(t.count(9))
print(max(t))
print(t + t)

#################################################

print('\n')

#################################################

print((1, 2) == (2, 1))

#################################################

print('\n')

#################################################


a = (1, 2)
b = (3, 4)
c = zip(a, b)
x = list(c)
print(x)

print(x[0])

z = ((1, 3), (2, 4))
u = zip(*z)
print(list(u))

#################################################

print('\n')

#################################################

t = (4, 7, 2, 9, 8)
x = list(t)
x.remove(2)
t = tuple(x)
print(t)

#################################################

print('\n')

#################################################


print('DICTIONARYS: ')

print('\n')


d = {'brand' : 'cherry', 'model' : 'arizo5', 'color' : 'white'}
d['color'] = 'black'
print(d)


x = d.get('model')
print(x)


print(list(d.keys()))


print(list(d.values()))

d.pop('model')
print(d)

#################################################

print('\n')

#################################################


num = {'ali' : [12, 13, 8], 'sara' : [15, 7, 14], 'taha' : [5, 18, 13]}

d = {k : sorted(v) for k, v in num.items()}

print(d)

#################################################

print('\n')

#################################################


k = ['red', 'green']
v = ['#FF0000', '#008000']
z = zip(k, v)
d = dict(z)
print(d)

#################################################

print('\n')

#################################################

print('SETS: ')
print('\n')


f = {'apple', 'orange', 'banana'}
f.add('cherry')
print(f)


f.update(['mango', 'grapes'])
print(f)

f.remove('apple')
print(f)

#################################################

print('\n')

#################################################

x = {1, 2, 3}
y = {2, 3, 4}

print('\nX.Union(Y): ', x.union(y))
print('\nX | Y: ', x | y)

print('\nX.intersection(Y): ', x.intersection(y))
print('\nX & Y: ', x & y)


a = {1, 2, 4}
b = {1, 2, 3, 4, 5}
print('\nA.issubset(B): ', a.issubset(b))

print('\nB.issubset(A): ', b.issubset(a))
#################################################

print('\n')

#################################################


print('FILES: ')
print('\n')

file_path = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\epopeia.txt'
encoding_type = 'utf-8'

epopeia = open(file_path, 'r', encoding = encoding_type)

for line in epopeia:
    print(line)
    if 'tempestade' in line.lower():
        print(line)
epopeia.close()



with open(file_path, 'r', encoding = 'utf-8') as epopeia:
    for line in epopeia:
        if 'tempestade' in line.lower():
            print(line)
print(line)

#################################################

print('\n')

#################################################

file_path = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\epopeia.txt'

with open(file_path, 'r', encoding = encoding_type) as epopeia:
    line = epopeia.readline()
    while line:
        print(line, end = ' ')
        line = epopeia.readline()

print('\n')

file_path = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\epopeia.txt'    
with open(file_path, 'r', encoding = encoding_type) as epopeia:
    poem = epopeia.readlines()
    print(type(poem))
print(poem)

print('\n')        
        
file_path = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\epopeia.txt'        
with open(file_path, 'r', encoding = encoding_type) as epopeia:
    text = epopeia.read()
    print(type(text))
print(text)

print('\n')

for linea in poem[::-1]:
    print(linea, end = ' ')
print(type(poem))

print('\n')

for linea in text[::-1]:
    print(linea, end =' ')
print(type(text))

#################################################

print('\n')

#################################################


# {
# "data": [{
# 	"id": "12341 ",
# 	"name ": "Joseanne Viana",
# 	"address": {
# 		"city": "Lisboa",
# 		"state ": "Lisboa ",
# 		"postal_code":"1000569"
# 	           },
# 	"position ": "PhD Student "
# 		}
#     ]
# } 

import json

file_path = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\dados.json'
encoding_type = 'utf-8'
with open(file_path, encoding= encoding_type) as json_file:
    jsonObject = json.load(json_file)
    name = jsonObject['data'][1]['name']
    address = jsonObject['data'][2]['address']['city']
    
    print(jsonObject)
    print(name)
    print(address)
    
    a = json.dumps(jsonObject, indent = 4)
    print (a)    



#################################################

print('\n')

#################################################


import pandas as pd

a = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\Employee_data.xlsx'
all_data = pd.read_excel(a, index_col = 1)

email = all_data['email'].head()
company_name = all_data['company_name']
xl = pd.ExcelFile(a)
df = xl.parse('b')

#################################################

print('\n')

#################################################

import json
file_path = 'C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula15\\dados.json'

try:
    with open(file_path) as json_file:
        json_object = json.load(json_file)

except:
    print('File not found')
    
else:
    name = json_object['data'][0]['name']
    address = json_object['data'][1]['address']['city']

    print('\nJson_object: ', json_object)
    print('\nName: ', name)
    print('\nAddress: ', address)

finally:
    print('Something happened')

#################################################

print('\n')

#################################################

def power_tree(x):
    return(x ** 3)
print(power_tree(10))

a = lambda x : x ** 3
print(a(10))

def larger_num(num1, num2):
    if num1 > num2:
        return num1
    else:
        return num2
print(larger_num(5, 7))

larger_num = lambda num1, num2 : num1 if num1 > num2 else num2
print(larger_num(5, 7))
    
#################################################

print('\n')

#################################################

text = 'LISBON IS IN PORTUGAL '

def char_lowercase():
    char_lowercase = [char.lower() for char in text]
    return char_lowercase

def map_lowercase():
    map_lowercase = list(map(str.lower, text))
    return map_lowercase

def comp_words():
    words_lowercase = [word.lower() for word in text.split(' ')]
    return words_lowercase

def map_words():
    map_w = list(map(str.lower, text.split(' ')))
    return map_w

print(char_lowercase())
print(map_lowercase())
print(comp_words())
print(map_words())

#################################################

print('\n')

#################################################

vegetables = [
    ['Beets', 'Cauliflower', 'Broccoli'],
    ['Beets', 'Carrot', 'Cauliflower'],
    ['Beets', 'Carrot'],
    ['Beets', 'Broccoli', 'Carrot'],
]


def not_brocoli(food_list: list):
    return 'Broccoli' not in food_list

brocoli_less_list = list(filter(not_brocoli, vegetables))

print(brocoli_less_list)

#################################################

print('\n')

#################################################

class josi:
    a = [2, 4, 6]
    b = [7, 8]
    c = [10, 11, 21]
    
    def __init__(self, val):
        self.val = val
        print(val)
    
    def increase(self):
        self.val += 1
    
    def show(self):
        print(self.val)

x = josi(3)
c = x.increase()
d = x.show()
print(type(x.a))
print(x.a.__add__(x.b))
print(x.a.__eq__(x.b))
print(x.a.__ne__(x.c))

#################################################

print('\n')

#################################################

print('\nExercise 1: ')

from Semana4.Aula15.Area.area import squareArea, squarePerimeter, circleArea, circlePerimeter, sphereVolume 

side = int(input("Enter the side measurement to see operations with square: "))

print('\n')

squareArea(side)
squarePerimeter(side)

print('\n')


radius = int(input("Enter the radius measurement to see operations with circle and sphere volume: "))
print('\n')

circleArea(radius)
circlePerimeter(radius)
sphereVolume(radius)

#################################################

print('\n')

#################################################

print('\nExercise 2: ')

l = [1, 4, 5, 1, 6, 3, 2, 1, 2, 9, 1, 4, 6, 3, 9, 7]


def frequencies(v):
    f1 = []
    f2 = []
    indexMax = 0
    indexMin = 0
    maximum = 0
    minimum = l[0]
    t = ()
    
    for i in range(len(l)):
        l.count(l[i])
        # print(f'{l[i]} = ', l.count(l[i]))
        
        if l.count(l[i]) >= maximum:
            maximum = l.count(l[i])
            indexMax = i   
            
        elif l.count(l[i]) <= minimum:
            minimum = l.count(l[i])
            indexMin = i
            f1.append(l[indexMin])
           
    
    f2.append(l[indexMax])
    t = (f1,) + (f2,)
    return t


print('Output = ', frequencies(l))

















