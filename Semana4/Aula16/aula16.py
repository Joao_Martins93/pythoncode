# ## -*- coding: utf-8 -*-

# ## João Martins


#################################################

print('\n')

#################################################
import numpy as np


students = np.array([[[1, 3, 5], [1, 1, 1]],
                      [[4.5, 4, 5], [4.3, 4.4, 4.5], [4.3, 4.4, 4.6]]])
print(students.shape)

students1 = np.array([[1, 3, 5], [1, 3, 5]])
print(students1.shape)


print(np.__version__)
print(np.version)

print('\n')

print(dir(np))
print(dir(np.array))

#################################################

print('\n')

#################################################

students = np.array([[[1, 3, 5], [1, 1, 1]],
                      [[4.5, 4, 5], [4.3, 4.4, 4.5], [4.3, 4.4, 4.6]]])



print(students.ndim, students.dtype)




y = np.zeros((2, 3, 4))
print(y)

a = np.arange(10)
print(np.arange(10))
#print(np.arrange(10).reshape(6, 5))

x1 = np.array([[-1, 3]])
x2 = np.array([[1, 2]])
x3 = x1 + x2
x4 = x1 * x2
x5 = x1 - x2
print(x1)
print(x2)
print(x3)
print(x4)
print(x5)

print('\n')

x3 = np.power(10, 4, dtype = np.int8)
print(x3)

x4 = np.power(10, 4, dtype = np.int32)
print(x4)

#################################################

print('\n')

#################################################

import numpy as np

print(students.shape)

students1 = np.array([[1, 3, 5], [1, 3, 5]])
print(students1.shape)

print('\n')

print(np.arange(10))

print('\n')

print(np.arange(10).reshape(2, 5))

print('\n')

print((np.resize(np.arange(10), (2, 5))))

print('\n')

print((np.resize(np.arange(10), (2, 7))))


y = np.zeros((2, 3, 4))
print(y)

a = np.arange(10)
print(np.arange(10))
# print(np.arange(10).reshape(6, 5))




#################################################

print('\n')

#################################################

import numpy as np


d = np.arange(2, 5)
print(np.arange(2, 5))
print(d.shape)
print(d[:, np.newaxis])
print(d[:, np.newaxis].shape)


x = np.array([1, 4, 3])
y = x
z = np.copy(x)
x[1] = 2
print(x)
print(y)
print(z)

#################################################

print('\n')


#################################################

################################################
import numpy as np


dtype = [('name', 'S10'), ('grade', float), ('age', int), ('email', 'S10')]
values = [('Joseanne', 5, 31, 'abc@gmail.com'), ('Hamed', 5, 32, 'def@gmail.com'), ('Stefan', 5, 25, 'ghi@gmail.com')]

sorted_data = np.array(values, dtype = dtype)
print(np.sort(sorted_data, order = 'age'))

print('\n')

dtype = [('name', 'U10'), ('grade', float), ('age', int), ('email', 'U10')]
values = [('Joseanne', 5, 31, 'abc@gmail.com'), ('Hamed', 5, 32, 'def@gmail.com'), ('Stefan', 5, 25, 'ghi@gmail.com')]

sorted_data = np.array(values, dtype = dtype)
print(np.sort(sorted_data, order = 'age'))

#################################################

print('\n')

#################################################

import numpy as np

b = np.zeros((1000, 1000, 2), dtype = float)
print(b)
print(b.ndim, b.dtype, b.size)

# b = np.zeros((10, 10))
print('%d bytes' % (b.size * b.itemsize))

print(np.zeros((3)))

print('\n')


print(np.zeros((3, 4)))

b = np.arange(50)
b = b[::-1]
b = b.reshape(5, 10)
print(b)

print('\n', b[4, 1])

print('\n', b[3, :])

print('\n', b[0, 2 :])

print('\n', b[:1, 1:])

print('\n')

b = np.zeros((10, 6), dtype = int)
print(b)
print('\n', b.ndim, b.dtype, b.size)

print('\n')

b = np.zeros(10)
b[8] = 1
print(b)

print('\n')

b = np.zeros((1000, 1000, 2), dtype = float)
print(b)
print(b.ndim, b.dtype, b.size)

print('\n')

b = np.zeros((10, 10))
print('%d bytes' % (b.size * b.itemsize))

print('\n')


b = np.arange(50)
b = b[::-1]
b = b.reshape(5, 10)
print(b)

print('\n', b[4, 1])

print('\n', b[3, :])

print('\n', b[0, 2 :])

print('\n', b[:1, 1:])





print(np.ones((2,1)))

print('\n')

print(np.ones(10))

print('\n')

print(np.ones((1000,1000)))

print('\n')

b = np.arange(50)
print('\n1!: ', b)
      
b = b.reshape(5, 10)
print('\n2!: ', b)

print('\n')

c = np.ones((2, 10))
print(c)
print(np.concatenate((b, c)))

print('\n')

d = np.hstack(c)
print(np.hstack(c))

print('\n')

b = np.eye(3)
print(b)

print('\n')

a= np.full((2,1), np.inf)
print(a)
print('\n')
print(a.dtype, (a.size * a.itemsize))

print('\n')      

print(np.empty((1, 2)))

print('\n')

b = np.random.random((10, 10))



yesterday = np.datetime64('today') - np.timedelta64(1)
today = np.datetime64('today')
tomorrow = np.datetime64('today') + np.timedelta64(1)

print(yesterday, today, tomorrow)

print('\n')

b = np.arange('2019', '2021', dtype= 'datetime64[D]')
print(b.size, b.ndim)


#################################################

print('\n')

#################################################

import numpy as np
import matplotlib.pyplot as plt

values = np.linspace(-(2 * np.pi), 2 * np.pi, 30)
print(values)

cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = 'blue', marker = '*')
plt.plot(sin_values, color = 'red', marker = '*')
plt.show()



#################################################

print('\n')

#################################################

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

my_path = 'C:\\Users\\João\\Desktop\\pythoncode\\Semana4\\Aula16\\image.jpg'
im = Image.open(my_path)
plt.imshow(im)


im2 = Image.open(my_path)
im_p2 = np.array(im2.convert('P'))
plt.imshow(im_p2)


im3 = Image.open(my_path)
im_p3 = np.array(im3.convert('L'))
plt.imshow(im_p3)




#################################################

print('\n')

#################################################

##########  GRAPHS  ###########

print('\nMedias em Portugal')


import matplotlib.pyplot as plt
import pandas as pd


def get_medias(df):
    
    # medias_df = df.groupby(['TIME']).sum() / 2
    medias_df = df.groupby(['TIME']).agg({'Value' : 'sum'}) / 2
    return medias_df

df = pd.read_csv('C:\\Users\\João\\Desktop\\pythoncode\\Semana4\\Aula16\\DADOS_PISA.csv')

df_PRT = df.loc[df['LOCATION'] == 'PRT']


years = df_PRT.TIME.unique()

medias_df = get_medias(df_PRT)
medias = medias_df.Value.unique()

print('YEARS: ', list(years))
print('MEDIAS: ', list(medias))

<<<<<<< HEAD

# x = []
# y = []

# d = {}

# with open('C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula16\\DADOS_PISA.csv','r') as csvfile:
#     plots = csv.reader(csvfile, delimiter=',')
#     plots.__next__()
#     for row in plots:
#         if row[0] == 'PRT':
#             # x.append(row[5])
#             # y.append(row[6])
#             if row[5] in d:
#                 d[row[6]] += d[row[6]]   
#             else:
#                 d.update({row[5]: row[6]})
#     # y.sort(key = float)
    
#     # print('\nDIC: ', d)
    
    
# # plt.plot(x, y, label='Loaded from file!')
# plt.xlabel('x')
# plt.ylabel('y')
# plt.title('PORTUGAL')
# plt.legend()
# plt.bar(d.keys(), d.values())
# plt.show()

from PIL import Image
import numpy as np

# years = list(years)
# medias = list(medias)
# l1 = [2000]
# l2 = [465.0]
=======
years = list(years)
medias = list(medias)
>>>>>>> 9feb4fe75fe51197ea79f3e18372cce9b4ac154c

# d.update({2000 : str(465.0)})
# print(d)
# print('L1: ', l1)
# print('L2: ', l2)
# print('YEARS: ', years)
# for i in range(len(l1)):
#     d.update({l1[i]: str(l2[i])})
    
# print(d)


plt.xlabel('TIME')
plt.ylabel('Médias')
plt.title('PORTUGAL')

plt.plot(list(years), list(medias), color = 'green', marker = '*', label = 'Médias')

plt.legend()
plt.show()

print('\nOr with values: ')

plt.xlabel('TIME')
plt.ylabel('Médias')
plt.title('PORTUGAL')


plt.plot(list(years), list(medias), color = 'green', marker='*', label = 'Médias')
for a,b in zip(list(years), list(medias)): 
    plt.text(a, b, str(b))

plt.legend()
plt.show()

##################################

#######  BAR GRAPH  #######


print('\nOr: ')

d = {}

for i in range(len(years)):
    d.update({years[i]: str(medias[i])})

d = dict(sorted(d.items(), key=lambda x: x[1]))


plt.xlabel('TIME')
plt.ylabel('Médias')

plt.title('PORTUGAL')

plt.bar(d.keys(), d.values(), label = 'Médias')

plt.legend()
plt.show()

print('Or with values: ')

plt.xlabel('TIME')
plt.ylabel('Médias')

plt.title('PORTUGAL')

plt.bar(d.keys(), d.values(), color = 'green', label = 'Médias')
for a,b in zip(d.keys(), d.values()): 
    plt.text(a, b, str(b))

plt.legend()
plt.show()

##################################

print('\nOr: ')

plt.xlabel('TIME')
plt.ylabel('Médias')
plt.title('PORTUGAL')

plt.bar(list(years), list(medias), label = 'Médias')
plt.axis(xmin= 2002, xmax= 2019, ymin=460, ymax=495)
plt.legend()
plt.show()



print('Or with values: ')

plt.xlabel('TIME')
plt.ylabel('Médias')
plt.title('PORTUGAL')



plt.bar(list(years), list(medias), color = 'green', label = 'Médias')
for a,b in zip(list(years), list(medias)): 
    plt.text(a, b, str(b))

plt.axis(xmin= 2002, xmax= 2019, ymin=460, ymax=495)
plt.legend()
plt.show()


#################################################

print('\n')

#################################################

print('\nBOYS / GIRLS: ')


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def get_medias(df):
    
    # medias_df = df.groupby(['TIME']).sum() / 2
    medias_df = df.groupby(['TIME']).agg({'Value' : 'sum'})
    return medias_df

df = pd.read_csv('C:\\Users\\João\\Desktop\\pythoncode\\Semana4\\Aula16\\DADOS_PISA.csv')

df_PRT = df.loc[df['LOCATION'] == 'PRT']


boys_df = df_PRT.loc[df['SUBJECT'] == 'BOY']
# print(boys_df)

girls_df = df_PRT.loc[df['SUBJECT'] == 'GIRL']
# print(girls_df)



df = pd.read_csv('C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula16\\DADOS_PISA.csv')
# print(df)
# print(df['LOCATION'] == 'PRT')
df = df['LOCATION']
print(df)
    
    
# def le_graficos(ficheiro_csv):
#     grafico = []
#     with open(ficheiro_csv) as csvfile:
#         csv_reader = csv.reader(csvfile, delimiter = ',')
#         csv_reader.__next__()        
#         linha1 = csv_reader.__next__()
#         a1 = linha1[0]
#         soma = 0
#         count = 0
        
#         while a1 != 'PRT':
#             linha1 = csv_reader.__next__()
#             a1 = linha1[0]
            
#             while a1 == 'PRT':
                
#                 soma += float(linha1[6])
#                 count += 1
                
#                 linha1 = csv_reader.__next__()
#                 a1 = linha1[0]
               
#                 # for i in range(len(linha1) - 1):
#                 #     print('\nLEN: ', len(linha1))
#                 #     print(f'\nITEM {i}: ', linha1[i])
                
                
#                 # grafico.append((float(item_linha1_com_ponto), float(item_linha2_com_ponto)))
#     return grafico

# print(le_graficos('C:\\Users\\Admin\\Desktop\\pythoncode\\Semana4\\Aula16\\DADOS_PISA.csv'))

#############  BOYS  ##############

years = df_PRT.TIME.unique()
medias_boys = get_medias(boys_df)
medias_boys = list(medias_boys.Value)
# print('Medias_B: ', medias_boys)


#############  GIRLS  ##############

medias_girls = get_medias(girls_df)
medias_girls = list(medias_girls.Value)
# print('Medias_G: ', medias_girls)


#####  GRAPH  #####


barWidth = 0.3

bars1 = medias_boys
bars2 = medias_girls


r1 = np.arange(len(bars1))
r1 = [x + 0.15 for x in r1]
r2 = [x + 0.30 for x in r1]

 
plt.bar(r1, bars1, color='#7f6d5f', width=barWidth, edgecolor='white', label='BOYS')
plt.bar(r2, bars2, color='#557f2d', width=barWidth, edgecolor='white', label='GIRLS')


plt.xlabel('Group', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(bars1))], years)

plt.ylim(420, 520)
plt.legend()
plt.show()



print('\nOr with values: ')

plt.bar(r1, list(medias_boys), color = '#7f6d5f', width=barWidth, edgecolor='white', label='BOYS')
for a,b in zip(r1, list(medias_boys)): 
    plt.text(a, b, str(b))

plt.bar(r2, list(medias_girls), color = '#557f2d', width=barWidth, edgecolor='white', label='GIRLS')
for a,b in zip(r1, list(medias_girls)): 
    plt.text(a, b, str(b))



plt.xlabel('Group', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(bars1))], years)
 

plt.ylim(420, 520)
plt.legend()
plt.show()

#################################################

print('\n')

#################################################


print('\nMédias Globais')


import matplotlib.pyplot as plt
import pandas as pd


<<<<<<< HEAD
=======
def get_mediasDF(df):
    medias_df = df.groupby(['TIME']).agg({'Value' : 'sum'}) / 2
    return medias_df


df = pd.read_csv('C:\\Users\\João\\Desktop\\pythoncode\\Semana4\\Aula16\\DADOS_PISA.csv')

df_PRT = df.loc[df['LOCATION'] == 'PRT']
df_ITA = df.loc[df['LOCATION'] == 'ITA']
df_JPN = df.loc[df['LOCATION'] == 'JPN']
df_KOR = df.loc[df['LOCATION'] == 'KOR']
df_MEX = df.loc[df['LOCATION'] == 'MEX']
df_NZL = df.loc[df['LOCATION'] == 'NZL']

years = df.TIME.unique()

medias_df = get_mediasDF(df_PRT)
medias_PRT = medias_df.Value.unique()

medias_df = get_mediasDF(df_ITA)
medias_ITA = medias_df.Value.unique()

medias_df = get_mediasDF(df_JPN)
medias_JPN = medias_df.Value.unique()

medias_df = get_mediasDF(df_KOR)
medias_KOR = medias_df.Value.unique()

medias_df = get_mediasDF(df_MEX)
medias_MEX = medias_df.Value.unique()

medias_df = get_mediasDF(df_NZL)
medias_NZL = medias_df.Value.unique()



plt.xlabel('TIME')
plt.ylabel('Médias')

plt.title('Médias Globais')

plt.plot(list(years), list(medias_PRT), color = 'green', marker = ('*'), label = 'PRT')  
plt.plot(list(years), list(medias_ITA), color = 'blue', marker = '*', label = 'ITA')  
plt.plot(list(years), list(medias_JPN), color = 'black', marker = '*', label = 'JPN')
plt.plot(list(years), list(medias_KOR), color = 'red', marker = '*', label = 'KOR')
plt.plot(list(years), list(medias_MEX), color = 'brown', marker = '*', label = 'MEX')
plt.plot(list(years), list(medias_NZL), color = 'yellow', marker = '*', label = 'NZL')

plt.ylim(370, 575)
plt.legend(loc = 'upper right')
plt.show()



print('\nOr with values: ')


plt.xlabel('TIME')
plt.ylabel('Médias')

plt.title('Médias Globais')

plt.plot(list(years), list(medias_PRT), marker='o')
for a,b in zip(list(years), list(medias_PRT)): 
    plt.text(a, b, str(b))
    

plt.plot(list(years), list(medias_ITA), color = 'blue', marker = '*', label = 'ITA')
for a,b in zip(list(years), list(medias_ITA)): 
    plt.text(a, b, str(b))
    

plt.plot(list(years), list(medias_JPN), color = 'black', marker = '*', label = 'JPN')
for a,b in zip(list(years), list(medias_JPN)): 
    plt.text(a, b, str(b))
    

plt.plot(list(years), list(medias_KOR), color = 'red', marker = '*', label = 'KOR')
for a,b in zip(list(years), list(medias_KOR)): 
    plt.text(a, b, str(b))
    

plt.plot(list(years), list(medias_MEX), color = 'brown', marker = '*', label = 'MEX')
for a,b in zip(list(years), list(medias_MEX)): 
    plt.text(a, b, str(b))


plt.plot(list(years), list(medias_NZL), color = 'yellow', marker = '*', label = 'NZL')
for a,b in zip(list(years), list(medias_NZL)): 
    plt.text(a, b, str(b))
    

plt.ylim(370, 575)
plt.legend(loc = 'upper right')
plt.show()


>>>>>>> 9feb4fe75fe51197ea79f3e18372cce9b4ac154c
