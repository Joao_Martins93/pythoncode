# -*- coding: utf-8 -*-


#####################################################################

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-5,0,5)

x1 = 0

x2 = np.linspace(0,4,5)

y = x-3

y1 = 1

y2 = x+2

plt.plot(x, y, '-r', label='y=x+2')

plt.plot(x1, y1, '-g', label='y=1')

plt.plot(x2, y2, '-m', label='y=x-3')

plt.title('Linear graphs ')

plt.xlabel('x')

plt.ylabel('y')

plt.legend(loc='upper left')

plt.grid()

plt.show()


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt

import numpy as np

x = [-5,5]

m = [1,2,3,5]

b = [-2,3/4,2,1]

color = ['g', 'r', 'k', 'm']

for i in range(0,len(m)):

    y = m[i]*np.array(x) + b[i]
    plt.plot(x,y,label='y=%sx+%s' % (m[i],b[i]), color=color[i] )

plt.axis('auto')

plt.xlim(x)

plt.ylim(x)

plt.grid()

plt.xlabel('x')

plt.ylabel('y')

axis = plt.gca()

plt.legend(prop={'size':15})

plt.title('Generic plot.')

plt.show()


#####################################################################


print('\n')


#####################################################################


print('Ex 1:')

import matplotlib.pyplot as plt

import numpy as np


x = [-8,8]

m = [1, 4]

b = [-5, 2]

color = ['g', 'r']

for i in range(0,len(m)):

    y = m[i]*np.array(x) + b[i]
    plt.plot(x,y,label='y=%sx+%s' % (m[i],b[i]), color=color[i] )
     

y1 = (m[0] + m[1])*np.array(x) + (b[0] + b[1])
plt.plot(x, y1, label ='y1=%sx+%s' % ((m[0] + m[1]), (b[0] + b[1])), color='b')

plt.axis('auto')

plt.xlim(x)

plt.ylim(x)

plt.grid()

plt.xlabel('x')

plt.ylabel('y')

axis = plt.gca()

plt.legend(prop={'size':15})

plt.title('Ex 1')

plt.show()


#####################################################################


print('\n')


#####################################################################


print('Ex 2:')

import matplotlib.pyplot as plt

import numpy as np


m = [3, 4]

b = [-2, -2]

color = ['g', 'r']

for i in range(0,len(m)):

    y = m[i]*np.array(x) + b[i]
    plt.plot(x,y,label='y=%sx+%s' % (m[i],b[i]), color=color[i] )
     

y1 = (m[0] + m[1])*np.array(x) + (b[0] + b[1])
plt.plot(x, y1, label ='y1=%sx+%s' % ((m[0] + m[1]), (b[0] + b[1])), color='b')

plt.axis('auto')

plt.xlim(x)

plt.ylim(x)

plt.grid()

plt.xlabel('x')

plt.ylabel('y')

axis = plt.gca()

plt.legend(prop={'size':15})

plt.title('Ex 2')

plt.show()


#####################################################################


print('\n')


#####################################################################


print('Ex 3:')

import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(-6, 6, 12)


m = [1, 2]

b = [-2, 5]

color = ['g', 'r']


y = m[0]*np.array(x) ** 2 + b[0]
plt.plot(x,y,label='y=%sx^2+%s' % (m[0],b[0]), color=color[0] )

y1 = m[1]*np.array(x) + b[1]
plt.plot(x,y1,label='y=%sx+%s' % (m[1],b[1]), color=color[1] )
     

y2 = (m[0])*np.array(x) ** 2 + m[1]*np.array(x) + (b[0] + b[1])
plt.plot(x, y2, label ='y1=%sx^2+%sx+%s' % (m[0], m[1], (b[0] + b[1])), color='b')


plt.legend(prop={'size':10})

plt.title('Ex 3')

plt.show()


#####################################################################


print('\n')


#####################################################################


print('Ex 4:')

import matplotlib.pyplot as plt

import numpy as np


x = np.linspace(-5, 10, 20)

m = [1, 3]

b = [5, -5]

color = ['g', 'r']

for i in range(0,len(m)):

    y = m[i]*np.array(x) + b[i]
    plt.plot(x,y,label='y=%sx+%s' % (m[i],b[i]), color=color[i] )    

y1 = ((m[0] * m[1])*np.array(x)** 2) + ((m[0]*b[1]) + (b[0] * m[1]))*np.array(x) + (b[0] * b[1])
plt.plot(x, y1, label ='y1=%sx^2+%sx+%s' % ((m[0] * m[1]), (m[0]*b[1]) + (b[0] * m[1]), (b[0] * b[1])), color='b')


plt.legend(prop={'size':10})
plt.title('Ex 4')
plt.show()


#####################################################################


print('\n')


#####################################################################


print('Ex 5:')

import matplotlib.pyplot as plt

import numpy as np


x = np.linspace(-5, 5, 10)

m = [2, 3]

b = [-1, -5]

color = ['g', 'r']

for i in range(0,len(m)):

    y = m[i]*np.array(x) + b[i]
    plt.plot(x,y,label='y=%sx+%s' % (m[i],b[i]), color=color[i] )    


y1 = (b[0] / b[1])*np.array(x)
plt.plot(x, y1, label ='y1=%s/%s' % (b[0] , b[1]), color='b')


plt.legend(prop={'size':10})
plt.title('Ex 5')
plt.show()


#####################################################################


print('\n')


#####################################################################


print('Ex 6:')

import matplotlib.pyplot as plt
import numpy as np


x = np.linspace(-3, 3, 10)

m = [1, -3]
b = [-3, 6]
color = ['g', 'r']


y = m[0]*np.array(x) + b[0]
plt.plot(x,y,label='y=%sx+%s' % (m[0],b[0]), color=color[0] )    


y1 = m[1]*np.array(x)**3 + b[1]
plt.plot(x, y1, label ='y1=%s^3+%sx' % (m[1] , b[1]), color='b')


y2 = (m[1]*np.array(x)**3) + ((m[0] + b[1])*np.array(x)) + b[0]
plt.plot(x,y2,label='y=%sx^3+%sx+%s' % (m[1], m[0] + b[1], b[0])) 


plt.legend(prop={'size':10})
plt.title('Ex 6')
plt.show()


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt
import numpy as np

print('Polynomial functions types:')


x = np.linspace(-10,10,10)

y1 = x**2
y2 = -3*x**3+6*x
y3 = -3*x**4+6*x+2*x
y4 = -x**2

plt.plot(x, y1, '-r', label = 'y1 = x^2')
plt.title('Quadratic Function')
plt.legend(prop={'size':10})
plt.show()

plt.plot(x, y2, '-b', label = 'y2 = -3x^3+6x')
plt.title('Cubic Function')
plt.legend(prop={'size':10})
plt.show()

plt.plot(x, y3, '-g', label = 'y3 = -3x^4+6x+2x')
plt.title('Quartic Graph')
plt.legend(prop={'size':10})
plt.show()

plt.plot(x, y4, '-y', label = 'y4 = -x^2')
plt.title('Quadratic Function')
plt.legend(prop={'size':10})
plt.show()


#####################################################################


print('\n')


#####################################################################


print('Discrete functions')

import matplotlib.pyplot as plt

x = [-3,-2,-1,0,1,2,3]

y = [8,4,2,0,2,4,8]

color = ['g', 'r', 'k', 'm', 'k', 'r', 'g']

for i in range(0,len(x)):

    plt.plot(x[i],y[i], 'o', color=color[i] )


plt.title('Discrete function')

plt.legend(loc='upper left')

plt.grid()

plt.show()


#####################################################################


print('\n')


#####################################################################


print('Polynomial regression')

import numpy
import matplotlib.pyplot as plt

x = [1,2,3,5,6,7,8,9,10,12,13,14,15,16,18,19,21,22]

y = [100,90,80,60,60,55,60,65,70,70,75,76,78,79,90,99,99,100]

mymodel = numpy.poly1d(numpy.polyfit(x, y, 8))

myline = numpy.linspace(1, 22, 100)

plt.scatter(x, y)

plt.plot(myline, mymodel(myline))
plt.title('Polynomial regression')
plt.grid()
plt.show()


#####################################################################


print('\n')


#####################################################################


# print('Linear regression')

# import numpy as np
# import matplotlib.pyplot as plt
# from sklearn.linear_model import LinearRegression


# x = np.array([2400, 2650, 2350, 4950, 3100, 2500, 5106, 3100, 2900, 1750]).reshape((-1, 1))
# y = np.array([41200, 50100, 52000, 66000, 44500, 37700, 73500, 37500, 56700, 35600])


# model = LinearRegression().fit(x, y)

# print('coefficient of determination:', model.score(x, y))

# print('Slope:', model.coef_)

# print('Intercept:', model.intercept_)




# plt.scatter(x, y, color = 'b')
# plt.plot(x, y)
# plt.title('Linear regression')
# plt.grid()
# plt.show()


####################################################################


print('\n')


#####################################################################


print('Plotting discrete functions using Lists')

import matplotlib.pyplot as plt

x = [1, 2, 3, 4, 5]
y1 = [1, 2, 3, 4, 5]

plt.plot(x, y1, '-b', label= 'y =x')

plt.title(' Discrete function using lists')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc='upper left')
plt.grid()
plt.show()


####################################################################


print('\n')


#####################################################################


print('Plotting discrete functions using Tuples')

import matplotlib.pyplot as plt

x = (1, 2, 3, 4, 5)
y1 = (1, 2, 3, 4, 5)


plt.plot(x,y1,'or' ,label= 'y = x')

plt.plot(x,y1,'b', label= 'y = x')

plt.title(' Discrete function using tuples')
plt.legend(loc='upper left')
plt.grid()
plt.show()


####################################################################


print('\n')


#####################################################################


print('Plotting discrete functions using Tuples')

import matplotlib.pyplot as plt


x1 = {'x':[1, 2, 3, 4, 5], 'y1' : [1, 2, 3, 4, 5]}


plt.plot('x', 'y1', '-b', label= 'y = x', data=x1)
plt.title(' Discrete function using dictionaries')
plt.legend(loc='upper left')
plt.grid()
plt.show()






