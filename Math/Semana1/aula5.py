# # -*- coding: utf-8 -*-

# ####################################################################

# print('Exercises:')

# print('\nWhich graphs show relations that are functions?')
# print('Graphs that are functions: 1, 3, 4')


# #####################################################################


# print('\n')


# #####################################################################

# x = 1
# y = -2

# f = 3*x - 2
# g = 5 - y**2
# h = (-2*y**2) + (3 * y) - 1

# r1 = 3 * f - 4 * g



# x = 7/3
# f = 3*x - 2

# r2 = f - h

# print('1: ', r1)
# print('2: ', r2) 

# #####################################################################


# print('\n')


# #####################################################################

# print('Graphs:')

# import matplotlib.pyplot as plt
# import numpy as np

# x = np.linspace(-10, 10, 200)

# y = x**4 + 3
# y1 = x**5 + 4*x
# y2 = np.sqrt(x) + 6
# y3 = x**(1/3)
# y4 = np.tan(x)
# y5 = -x + 2
# y6 = (-3 * x) + 2
# y7 = (-2 * x**2) + 1
# y8 = np.sin(2*x) + 4

# plt.plot(x, y, label = 'y = x**4 + 3')
# plt.plot(x, y1, label = 'y1 = x**5 + 4*x')
# plt.plot(x, y7, label = 'y7 = (-2 * x**2) + 1')
# plt.legend(prop = {'size' : 8})
# plt.ylim(-10, 20)
# plt.show()


# plt.plot(x, y2, label = 'y2 = sqrt(x) + 6')
# plt.plot(x, y3, label = 'y3 = x**(1/3)')
# plt.legend(prop = {'size' : 8})
# plt.show()


# plt.plot(x, y4, label = 'y4 = tan(x)')
# plt.plot(x, y8, label = 'y8 = sin(2*x) + 4')
# plt.legend(prop = {'size' : 8})
# plt.show()

# plt.plot(x, y5, label = 'y5 = -x + 2')
# plt.plot(x, y6, label = 'y6 = (-3 * x) + 2')
# plt.legend(prop = {'size' : 8})
# plt.show()

# #####################################################################


# print('\n')


# #####################################################################

# import numpy as np

# print('Sequences:')

# x = np.linspace(1, 6, 6).astype(int)

# y = 2*x
# print('a) ', y)


# y1 = 2*x - 1
# print('b) ', y1)


# x1 = np.linspace(0, 5, 6).astype(int)
# y2 = 99 + x1*100
# print('c) ', y2)



# y3 = (3 + (2 * x1)) * (-1)**x1
# print('d) ', y3)



# y4 = (x + 1)/ x
# print('e) ', y4)



# y5 = x**2
# print('f) ', y5)



# y6 = []
# z = 2 * (x1+1)
# y6.append(0)
# for i in range(6):
#     y6.append(y6[i] + z[i]) 

# print('g) ', y6)


# print(x1)
# y7 = (2**x1) / (3**x1)
# print('h) ', y7)

# #####################################################################


# print('\n')


# #####################################################################

# print("Exercise (a): ")

# import matplotlib.pyplot as plt
# import numpy as np

# m = 100
# y = 1000000
# t = np.linspace(1, 12)

# n = m*(np.exp(2 * t))

# res = (np.log(m / y)) / -2

# print(res, ' months')

# plt.yscale('log')
# plt.xscale('log')
# plt.plot(t, n)
# plt.axhline(y)

# plt.show()


# #####################################################################


# print('\n')


# #####################################################################


# print('Exercise (b) i. : ')
# x = 1.74 * 10**19 * 10**(1.44*5)

# print(x, ' Joules')


# print('\nExercise (b) ii. : ')

# x = 1.74 * 10**19 * 10**(1.44*5.2)

# print(x, ' Joules')


# #####################################################################


# print('\n')


# #####################################################################


import matplotlib.pyplot as plt
import numpy as np
x = [2, 30, 70, 100, 150]
y = [4.24, 16.4, 25.1, 30.0, 36.7]


plt.xscale('log')
plt.yscale('log')
plt.plot(x, y, '-o')

logx = np.log10(x)
logy = np.log10(y)

mymodel = np.poly1d(np.polyfit(logx, logy, 1))

y1 = 3 * (np.sqrt(x))
print(mymodel)

plt.plot(x, y, '-o')
plt.plot(x, y1, '-')

































