# -*- coding: utf-8 -*-

#####################################################################

import matplotlib.pyplot as plt
import numpy as np


def calc(a, b, c):
    x = []
    base = b**2 - 4*a*c
    
    if base < 0:
        print('Erro!')
    
    x.append(int(-b-np.sqrt(base)/2*a))
    x.append(int(-b+np.sqrt(base)/2*a))
    
    return x


x = np.linspace(-10, 10, 30)
y = x**2
y1 = x**2 + 2
y2 = 2*x**2 
y3 = -2*x**2
y4 = x**2 - 4

print(calc(1, 0, -4))

plt.grid()
plt.ylim(-50, 50)
plt.plot(x, y)
plt.plot(x, y1)
plt.plot(x, y2)
plt.plot(x, y3)
plt.plot(x, y4)


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt
import numpy as np
import math

print('Exponentials')
lin = np.linspace(-10, 10, 30)



x = int(3**2 /3)
print('Ex 1 = ', x)


print('\n')


print('Ex 2 = m^2')
y = lin**2
plt.plot(lin, y)


print('\n')


print('Ex 3 = (4x^2 * y)/3')


print('\n')


print('Ex 4 = 4x^10 * y^14')


print('\n')


print('Ex 5 = 2x^17 * y^16')


print('\n')


x = int(3**4 / 3)
print('Ex 6 = ', x)


print('\n')


print('Ex 7 = (x * y^3) / 4')


print('\n')


print('Ex 8 = (y^2) / 4')


print('\n')


print('Ex 9 = (8u^18 * v^6)')


print('\n')


print('Ex 10 = 3vu')


#####################################################################


print('\n')


#####################################################################


print('logarithms - examples')

print('Ex1:  2 = log7(x)  =>  x = 7^2')
print('x = ', 7**2)


print('Ex2:  3 = log10(x + 8)  =>  x + 8 = 10^3')
print('x = ', 10**3 - 8)


print('Ex3:  log5(125) = 5  =>  125 = 5^5')
print('x = ', 5**5)


print('Ex1:  2^4 = 16  =>  4 = log2(16)')

print('Ex2:  sqrt(64)(64**(1/2)) = 8  =>  1/2 = log64(8)')

print('Ex3:  e**4 = 54.60  =>  4 = loge(54.60)')


#####################################################################


print('\n')


#####################################################################


print('Logarithms - exercises')

x = 0.6**math.sqrt(3)
print('Ex 1 = ', x)


x = math.exp(3.2)
print('\nEx 2 = ', x)


x = 1.005**400
print('\nEx 3 = ', x)


print('\nEx 4 = ', math.log(64, 4))


print('\nEx 5 = ', math.log(1))


x = math.log(math.sqrt(7))
print('\nEx 6 = ', x)

########################################

x = math.log(25, 5)
print('\nEx 7 = ', x)


x = math.log((1/81), 3)
print('\nEx 8 = ', x)


x = math.log(math.exp(-2))
print('\nEx 9 = ', x)

########################################

x = math.log(3, 7)
print('\nEx 10 = ', x)


x = math.log((1/2), 2)
print('\nEx 11 = ', x)


x = math.log(42, 15)
print('\nEx 12 = ', x)

########################################


print('\nEx 13 = 1 + log(x)')


print('\nEx 14 = ln(⁡x)+ln(⁡y)−ln⁡(z)')


print('\nEx 15 = 1+ 2log4(x)')


print('\nEx 16 = 1/2 * log3⁡(𝑥−2)')


print('\nEx 17 = 1/2 * log⁡(3x) - log(⁡7)')


#####################################################################


print('\n')


#####################################################################


print('Radicals - exercises')

print('\nEx 1: 3*sqrt(12) -> 6*sqrt(3)')

print('\nEx 2: 6*sqrt(128) -> 48*sqrt(2)')

print('\nEx 3: 7*sqrt(128) -> 56*sqrt(2)')

print('\nEx 4: -8*sqrt(392) -> -112*sqrt(2)')

print('\nEx 5: -7*sqrt(63) -> -21*sqrt(7)')

print('\nEx 6: sqrt(192*n) -> 8*sqrt(3n)')

print('\nEx 7: sqrt(343b) -> 7*sqrt(7b)')

print('\nEx 8: sqrt(196v^2) -> 14v')

print('\nEx 9: sqrt(100n^3) -> 10n*sqrt(n)')

print('\nEx 10: sqrt(200a^3) -> 10a*sqrt(2a)')


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0,8,10)
y1 = 2**x - 3
plt.plot(x, y1, '-m')
plt.title('Exponential Function')
plt.grid()
plt.show()


x = np.linspace(0,100,10)
y1 = np.sqrt(x)
plt.plot(x, y1, '-m', label = 'y=sqrt(x)')
plt.title('radial graphs')
plt.grid()
plt.show()


y1 = np.log(x)
plt.plot(x, y1, '-m', label = 'y=log(x)')
plt.title('log graph')
plt.grid()
plt.show()


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt

import numpy as np

x = np.linspace(0,3,10)

y1 = 2**x

y2 = 3**x

y3 = (2**x) +4

y4 = (2**(x-1))

plt.plot(x,y1, 'm', label= '2**x')

plt.plot(x, y2, '-b', label=
'3**x')

plt.plot(x, y3, '-r', label=
'(2**x) +4')

plt.plot(x, y4, label=
'(2**(x-1))')

plt.title(' Exponential function')

plt.xlabel('x')

plt.ylabel('y')

plt.legend(loc='upper left')

plt.grid()

plt.show()


#####################################################################


print('\n')


#####################################################################


import numpy as np
import matplotlib.pyplot as plt
    
x = np.linspace(0,4,5)
y1 = np.sqrt(x)
y2 = np.log(x)
y3 = 2**x 

plt.plot(x, y1,'-m', label='y=sqrt(x)')
plt.plot(x, y2, '-b', label= 'y=log(x)')
plt.plot(x, y3, '-r', label= '(2**x)')

plt.title('Comparison graph (exp, rad and log)')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc='upper left')
plt.grid()
plt.show()














































