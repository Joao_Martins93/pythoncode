# -*- coding: utf-8 -*-


# #####################################################################

import numpy as np

print('A: ')
o = 24
y = 13

x = y / np.sin(np.deg2rad(o))
print(x)



print('\nB: ')

o = 52.3
y = 5

x = y / np.cos(np.deg2rad(o))
print(x)




print('\nC: ')

o = 71
y = 9

x = y * np.tan(np.deg2rad(o))
print(x)




print('\nD: ')

o = 63
y = 7.6

x = y / np.cos(np.deg2rad(o))
print(x)


#####################################################################


print('\n')


#####################################################################


print('A: ') 
hi = 7
ca = 4

o = np.rad2deg(np.arccos(ca/hi))
print(o)



print('\nB: ')

co = 12
ca = 13

o = np.rad2deg(np.arctan(co/ca))
print(o)




print('\nC: ')

co = 16
ca = 10

o = np.rad2deg(np.arctan(co/ca))
print(o)




print('\nD: ')

co = 5.6
ca = 15.3

o = np.rad2deg(np.arctan(co/ca))
print(o)


#####################################################################


print('\n')


#####################################################################


np.set_printoptions(precision=2)
np.set_printoptions(formatter={"float":'{:0.2f}'.format})
a = np.array([0,30,45,60,90])
print('Sine of different angles:')

# Convert to radians by multiplying to pi/180
print(np.sin(a*np.pi/180), "\n")
print("Cosine values for angles in array:")
print(np.cos(a*np.pi/180), "\n")
print('Tangent values for givenangles:')
print(np.tan(a*np.pi/180))


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt

values = np.linspace(-(2*np.pi), 2*np.pi, 20)
cos_values = np.cos(values)
sin_values = np.sin(values)

plt.plot(cos_values, color = 'b', marker='*')
plt.plot(sin_values, color = 'r')
plt.show()


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt
x = np.linspace(-2*np.pi, 2*np.pi, 40)
x1 = np.rad2deg(x)
y = np.cos(x)
plt.plot(x1,y)
plt.xlabel('degree')
plt.ylabel('cos(x)')
plt.show()


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt
x = np.linspace(-2*np.pi, 2*np.pi, 40)
y = np.cos(x)
plt.plot(x,y)
plt.xlabel('radians')
plt.ylabel('cos(x)')
plt.show()


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt

x = np.linspace(0, 2*np.pi, 30)
y = np.sin(x + 1)
y1 = np.sin(x)
y2 = np.sin(x) + 1
y3 = -np.sin(x)
plt.plot(x, y1, label = 'sin(x)')
plt.plot(x, y, label = 'sin(x + 1)')
plt.plot(x, y2, label = 'sin(x) + 1')
plt.plot(x, y3, label = '-sin(x)')
plt.legend(prop = {'size' : 15})
plt.grid()
plt.show()


#####################################################################


print('\n')


#####################################################################

import matplotlib.pyplot as plt

x = np.linspace(-2*np.pi, 2*np.pi, 200)

y = 4 + 1/2 * np.sin(x)

y1 = -4 * np.cos(3*x - np.pi)

plt.plot(x, y, label = 'y = 4 + 1/2 * np.sin(x)')
plt.plot(x, y1, label = 'y1 = -4 * np.cos(3*x - np.pi)')
plt.legend(prop = {'size' : 8}, loc = 'lower right')
plt.title('Radians')
plt.xlabel('Radians')
plt.ylabel('Amplitude')
plt.show()



x1 = np.rad2deg(x)
y = 4 + 1/2 * np.sin(x)

y1 = -4 * np.cos(3*x - np.pi)

plt.plot(x1, y, label = 'y = 4 + 1/2 * np.sin(x)')
plt.plot(x1, y1, label = 'y1 = -4 * np.cos(3*x - np.pi)')
plt.legend(prop = {'size' : 8}, loc = 'lower right')
plt.title('Degrees')
plt.xlabel('Degrees')
plt.ylabel('Amplitude')
plt.show()




y = 4 + 1/2 * np.sin(x)

y1 = -4 * np.cos(3*x - np.pi)

plt.plot(x, y, label = 'y = 4 + 1/2 * np.sin(x)')
plt.plot(x, y1, label = 'y1 = -4 * np.cos(3*x - np.pi)')
plt.legend(prop = {'size' : 8}, loc = 'lower right')
plt.title('Samples')
plt.xlabel('Radians')
plt.ylabel('Amplitude')
plt.show()

































