#-*- coding: utf-8 -*-

#####################################################################

import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(1, 6, 6).astype(int)
y = 2*x -1
y1 = (-1)**x
y2 = x**2 + 1
y3 = 0.25*x
y4 = 3**x


print(y)
print(y1)
print(y2)
print(y3)
print(y4)


plt.yticks(range(-1, 20, 2))
plt.ylim(-2, 20)

plt.xticks(range(1, 8))
plt.xlim(0, 8)

plt.plot(x, y, 'o', label = '2*x -1')
plt.plot(x, y1, 'o', label = '(-1)**x')
plt.plot(x, y2, 'o', label = 'x**2 + 1')
plt.plot(x, y3, 'o', label = '0.25*x')
plt.plot(x, y4, 'o', label = '3**x')
plt.legend(prop = {'size' : 8})



#####################################################################


print('\n')


#####################################################################

x = np.linspace(1, 10, 10).astype(int)

y = 5 + (x - 1) * (-3)

print(y)


#####################################################################


print('\n')


#####################################################################


soma = 0

for i in range(5):
    soma += 2*i + 1
    
print('SOMA: ', soma)
print('MEDIA: ', soma/(i+1))


print('\n')



a = [1, 3, 5, 7, 9]

print('SOMA: ', sum(a))
print('MEDIA: ', np.mean(a))

#     ou

# b = list(range(1, 10, 2))


size = len(a)

x = int((size * (a[0] + a[size - 1]))/ 2)

print('SOMA: ', x)
print('MEDIA: ', x / size)



#####################################################################


print('\n')


#####################################################################

x = 0
for i in range(1, 13):
    x += 4 * (0.3)**i 

print(x)


x = 0
for i in range(0, 13):
    x += 4 * (0.3)**i 

print(x)














































