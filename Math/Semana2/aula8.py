# -*- coding: utf-8 -*-

#####################################################################

import numpy as np
import random
import matplotlib.pyplot as plt

#  CARDS

simulation_number = 10000
club =0;spade=0;diamont=0;heart=0;percentages =[]

for i in range(0, simulation_number):
    card_points =[1,12,11,10,2,3,4,5,6,7,8,9,10]
    card_signs =['Heart','CLUB','DIAMOND','SPADE']
 
    random_point = random.choice(card_points)
    random_sign = random.choice(card_signs)
    random_card = random_point,random_sign
    # print (random_card)
    
    ##Condition
    # if (random_card[0] == 7 and random_card[1] == 'CLUB'):
    #     club+=1;
        
    # # P(AvB)
    # if ((random_card[1] == 'Heart') or (random_card[1] == 'DIAMOND') or 
    #     (random_card[0] == 1 and random_card[1] == 'SPADE') or
    #     (random_card[0] == 1 and random_card[1] == 'CLUB')):
    #     club+=1;
    
    
    # P(A^B)
    if (random_card[0] == 7 and random_card[1] == 'CLUB'):
        club += 1      
    if (random_card[0] == 7 and random_card[1] == 'SPADE'):
        spade+=1;
        
X = ['CLUB']
percentages.append(np.round(club/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(X,percentages)
print(sum(percentages), '%')


#####################################################################


print('\n')


#####################################################################


import numpy as np
import random
import matplotlib.pyplot as plt


#  DICE

simulation_number = 10000
dice =0;#spade=0;diamont=0;heart=0;
percentages =[]

for i in range(0, simulation_number):
    die_points =[1, 2, 3, 4, 5, 6]
    
    
    ## toss coin -> {Head, Tail}
    ## toss die -> {1, 2, 3, 4, 5, 6}
    ## combination -> {(1 ^ Head), (1 ^ Tail), ..., (6 ^ Head), (6 ^ Tail)}
       
    random_point = random.choice(die_points)
    random_dice = random_point #,random_sign

    ##Condition
    
    if (random_dice == 4):
        dice += 1      
        
X = ['DICE']
percentages.append(np.round(dice/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(X,percentages)
print(sum(percentages), '%')


#####################################################################


print('\n')


#####################################################################


# COIN

simulation_number = 10000
coin =0;
percentages =[]

for i in range(0, simulation_number):
    coin1_signs =['Head', 'Tail']
    coin2_signs = coin1_signs
    
    ## toss coin -> {Head, Tail}
    ## toss die -> {1, 2, 3, 4, 5, 6}
    ## combination -> {(1 ^ Head), (1 ^ Tail), ..., (6 ^ Head), (6 ^ Tail)}
       
    random1_point = random.choice(coin1_signs)
    random2_point = random.choice(coin2_signs)
    ##Condition
    
    if (random1_point == 'Head' and random2_point == 'Head' ):
        coin += 1  
 
       
        
X = ['COIN']
percentages.append(np.round(coin/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(X,percentages)
print(sum(percentages), '%')



#####################################################################


print('\n')


#####################################################################


#  DICE

simulation_number = 10000
dice1 = 0;#spade=0;diamont=0;heart=0;
soma = 0
percentages =[]

for i in range(0, simulation_number):
    dice1_points =[1, 2, 3, 4, 5, 6]
    dice2_points = dice1_points
    
    ## toss coin -> {Head, Tail}
    ## toss die -> {1, 2, 3, 4, 5, 6}
    ## combination -> {(1 ^ Head), (1 ^ Tail), ..., (6 ^ Head), (6 ^ Tail)}
       
    random1_point = random.choice(dice1_points)
    random2_point = random.choice(dice2_points)
    # random_dice = random_point #,random_sign

    # print(random_point)
    
    ##Condition
    soma = random1_point + random2_point
    
    if ((soma > 7) or (soma % 2 != 0)):
        dice1 += 1      
        
X = ['DICE']
percentages.append(np.round(dice1/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(X,percentages)
print(sum(percentages), '%')


#####################################################################


print('\n')


#####################################################################


##  CARDS

simulation_number = 10000
club =0;spade=0;diamont=0;heart=0;percentages =[]

for i in range(0, simulation_number):
    
    # Probabilidade de carta ser vermelha sabendo que é um A's
    card_points =[1]
    card_signs =['Heart','CLUB','DIAMOND','SPADE']
 
    random_point = random.choice(card_points)
    random_sign = random.choice(card_signs)
    random_card = random_point,random_sign
    # print (random_card)
    
    ##Condition
    
    # P(A|B)
    if (random_card[0] == 1 and (random_card[1] == 'Heart' or random_card[1] == 'DIAMOND')):
        club+=1;
    
    
X = ['CLUB']
percentages.append(np.round(club/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(X,percentages)
print(sum(percentages), '%')









