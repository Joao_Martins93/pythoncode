## -*- coding: utf-8 -*-

# #####################################################################

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y)
plt.grid()
plt.show()



x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y)
plt.semilogy()
plt.grid()
plt.show()




x = np.linspace(0, 10, 10)
y = x
plt.plot(x, y)
plt.semilogx()
plt.grid()
plt.show()



x = np.linspace(0, 10, 10)
y = x
plt.yscale('log')
plt.xscale('log')
plt.grid()






























