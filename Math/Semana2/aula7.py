# -*- coding: utf-8 -*-

#####################################################################


#  Systematic sanple  ##

import numpy as np


systematic_sample = []
sample = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ,14, 15]
step = 2
print(np.mean(sample))
size = int(len(sample)/step)
for i in range(0,size):
    systematic_sample.append(sample[i*step])
systematic_mean = np.mean(systematic_sample)
print(systematic_mean)



#####################################################################


print('\n')


#####################################################################

import numpy as np

a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
meana = np.mean(a)
print(meana)


b = [2, 3, 6, 7, 10, 11]
meanb = np.mean(b)
print(meanb)

#####################################################################


print('\n')


#####################################################################


#  Cluster Sample  ## 

import numpy as np

systematic_sample = []
sample = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ,14, 15]
step = 3
print(np.mean(sample))
size = int(len(sample)/step)
for i in range(0,size):
    systematic_sample.append(sample[i*step])
    systematic_sample.append(sample[i*step]+1)
systematic_mean = np.mean(systematic_sample)
print(systematic_mean)


#####################################################################


print('\n')


#####################################################################


##  Stratified Sample  ##

import random as rd
import numpy as np

def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts]
            for i in range(wanted_parts) ]

systematic_sample = []
sample = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 ,14,15]
A, B, C = split_list(sample, 3)

total= split_list(sample, 3)
stratified_list =[]
print('mean all values:', np.mean(sample) )
splited_list = [A, B, C]

for i in range(0,len(splited_list)):
    stratified_list.append(splited_list[i][rd.randint(0,4)])
    
systematic_mean = np.mean(stratified_list)
print('stratified mean', systematic_mean)



#####################################################################


print('\n')


#####################################################################


import numpy as np
from scipy import stats
from scipy.stats import gmean
from scipy.stats import hmean


numbers = [ 1, 1, 2, 2, 3, 3, 4, 1, 2 ]
print(numbers)
Mean = np.mean(numbers)
Median = np.median(numbers)
Mode = int(stats.mode(numbers)[0]) # index 1 gives frequency
print("Mean = ", Mean, "\nMedian = ", Median, "\nMode = ", Mode)
Geometric_Mean = gmean(numbers)
Harmonic_mean = hmean(numbers)
print("Geometric Mean = ", Geometric_Mean, "\nHarmonic Mean = ",
Harmonic_mean)
Variance = np.var(numbers)
Standard_Deviation = np.std(numbers)
print("Variance = ", Variance, "\nStandard deviation = ",
Standard_Deviation)
Q1 = np.percentile(numbers, 25)
Q2 = np.percentile(numbers, 50)
Q3 = np.percentile(numbers, 75)
print("Q1 = ", Q1, "\nQ2 = ", Q2, "\nQ3 = ", Q3) # 1 1 1 2 2 2 3 3


#####################################################################


print('\n')


#####################################################################

import numpy as np

s1 = [4, 7, 2, 9, 12, 2, 20, 10, 5, 9]

median = np.median(s1)

upper = np.percentile(s1, 75)
lower = np.percentile(s1, 25)
inter = np.percentile(s1, 50)


print('Median = ', median)
print('Upper Quantile = ', upper)
print('Lower Quantile = ', lower)
print('Inter Quantile = ', inter)

print('\n')

s2 = [2, 5, 10, 11]

mean = np.mean(s2)

variance = np.var(s2)

deviation = np.std(s2)

print('Mean = ', mean)
print('Variance = ', variance)
print('Deviation = ', deviation)


#####################################################################


print('\n')


#####################################################################

import pandas as pd

graph = './DP_LIVE_12012021123743106.csv'

table = pd.read_csv(graph)


def get_stats(filter_id):
    
    PT_DF = table.loc[table['LOCATION'] == filter_id]
    
    # print(PT_DF)
    
    
    ##  BOYS  ##
    boys_df = PT_DF.loc[table['SUBJECT'] == 'BOY'].mean()
    mean_boys = boys_df.loc['Value']
    
    boys_df = PT_DF.loc[table['SUBJECT'] == 'BOY'].median()
    median_boys = boys_df.loc['Value']
    
    boys_df = PT_DF.loc[table['SUBJECT'] == 'BOY'].var()
    variance_boys = boys_df.loc['Value']
    
    boys_df = PT_DF.loc[table['SUBJECT'] == 'BOY'].std()
    deviation_boys = boys_df.loc['Value']
    
    
    
    ##  GIRLS  ##
    
    girls_df = PT_DF.loc[table['SUBJECT'] == 'GIRL'].mean()
    mean_girls = girls_df.loc['Value']
    
    girls_df = PT_DF.loc[table['SUBJECT'] == 'GIRL'].median()
    median_girls = girls_df.loc['Value']
    
    girls_df = PT_DF.loc[table['SUBJECT'] == 'GIRL'].var()
    variance_girls = girls_df.loc['Value']
    
    girls_df = PT_DF.loc[table['SUBJECT'] == 'GIRL'].std()
    deviation_girls = girls_df.loc['Value']
    
    print(f'\n{filter_id} :')
    
    print('\nBOYS STATS: ')
    print('MEAN = ', mean_boys)
    print('MEDIAN = ', median_boys)
    print('VARIANCE = ', variance_boys)
    print('DEVIATION = ', deviation_boys)
    
    print('\n')
    
    print('GIRLS STATS: ')
    print('MEAN = ', mean_girls)
    print('MEDIAN = ', median_girls)
    print('VARIANCE = ', variance_girls)
    print('DEVIATION = ', deviation_girls)

#######################################################


get_stats('PRT')

print('\n')

get_stats('AUS')

print('\n')

get_stats('CAN')

print('\n')

get_stats('CZE')



