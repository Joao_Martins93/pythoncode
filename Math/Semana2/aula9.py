# -*- coding: utf-8 -*-

#  MATRIZES

import numpy as np

a = np.matrix('2 3 1; 4 5 -1; 2 1 0')
c = np.matrix('1 2; 3 4')
d = np.matrix('-1 3; 4 2')
print(a)

b = np.linalg.det(a)
print('\n', b)

inverse_array = np.linalg.inv(c)
print('\n', inverse_array)



A = np.matrix('1 2; 6 -3')
A1 = np.matrix('3 2; 8 -3')
A2 = np.matrix('1 3; 6 8')

det1 = np.linalg.det(A)
det2 = np.linalg.det(A1)
det3 = np.linalg.det(A2)

x = np.linalg.solve(A, A1)
print('\n', x)


#####################################################################


print('\n')


#####################################################################


import matplotlib.pyplot as plt
# import scipy.linalg as la
import numpy as np

l = []; l1 = []; l2 = []
fig, ax = plt.subplots(3, 1)
M = np.array ([[1, 3, -5, 2], [0, 4, -2, 1], [2, -1, 3, -1], [1, 1, 1, 1]])
b = np.array([0, 6, 5, 10])
l.append(b)

x = np.linalg.solve(M, b)
print(x)

l1.append(x)
l2.append(M)
l2.append(l)
l2.append(l1)

for i in range(0, 3):
    ax[i].imshow(l2[i])
    ax[i].axis('off')

a = np.linspace(0, 2*np.pi)
r0 = [0.8, 1.2, 1.4]


for i in r0:
    r = i + np.cos(a)
    x = r * np.cos(a)
    y = r * np.sin(a)
    plt.plot(x, y, 'r')
    
    
#####################################################################


print('\n')


#####################################################################


import numpy as np

print('\nEx1: ')
a = np.matrix('10 20; 20 50')
b = np.array([1, 0])

x = np.linalg.solve(a, b)
print(x)


a = np.matrix('10 20; 20 50')
b = np.array([0, 1])

x = np.linalg.solve(a, b)
print('\n', x)


#####################################################################


print('\n')


#####################################################################


print('\n\nEx2: ')
A = np.matrix('0 0 0 2; 0 0 3 0; 0 4 0 0; 5 0 0 0')
B = np.matrix('3 2 0 0; 4 3 0 0; 0 0 6 5; 0 0 7 6')

invA = np.linalg.inv(A)
invB = np.linalg.inv(B)

print('A = \n', A)
print('\nInv A = \n', invA)

print('\n\nB = \n', B)
print('\nInv B = \n', invB)


#####################################################################


print('\n')


#####################################################################


print('\n\nEx3: ')

A = np.matrix('0 3; 4 0')
print('A = \n', A)

invA = np.linalg.inv(A)
print('\nInvA = \n', invA)



B = np.matrix('2 0; 4 2')
print('\n\nB = \n', B)

invB = np.linalg.inv(B)
print('\nInvB = \n', invB)



C = np.matrix('3 4; 5 7')
print('\n\nC = \n', C)

invC = np.linalg.inv(C)
print('\nInvC = \n', invC)


#####################################################################


print('\n')


#####################################################################

import numpy as np

print('\n\nEx4: ')


a = np.ones((4, 4))

b = 5 * np.eye(4)

print(a)
print(b)

print(b - a)








