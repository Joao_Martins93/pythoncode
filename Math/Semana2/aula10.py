# -*- coding: utf-8 -*-

#####################################################################

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(-10, 10, 20)
y = x**2

plt.hist(y, 100, density = 1)
plt.show()
plt.plot(x, y, 'o')
plt.show()


y1 = x**3

plt.hist(y1, 100, density = 1)
plt.show()
plt.plot(x, y1, 'o')
plt.show()


#####################################################################


print('\n')


#####################################################################

from numpy import random
import matplotlib.pyplot as plt


x2 = random.normal(loc = 0, scale = 5, size = 10000)
x = random.normal(loc = 0, scale = 1, size = 10000)
plt.vlines(x = 0, ymin = 0, ymax = 3000, color= 'k')

x1 = random.normal(loc = 1, scale = 2, size = 10000)
plt.vlines(x = 1, ymin = 0, ymax = 3000, color= 'r')


plt.hist(x2)
plt.hist(x1)
plt.hist(x)
plt.show()


#####################################################################


print('\n')


#####################################################################


import numpy as np
import random
import matplotlib.pyplot as plt

#  CARDS

simulation_number = 10000
two =0; three=0;percentages =[]

for i in range(0, simulation_number):
   
    dice_1 =[1,2, 3, 4, 5, 6]
    dice_2 =[1,2, 3, 4, 5, 6]

    random_dice_1 = random.choice(dice_1)
    random_dice_2 = random.choice(dice_2)

    ##Condition
    
    random_sum = random_dice_1 + random_dice_2

    if (random_sum == 2):
        two += 1;
    elif (random_sum == 3):
        three +=1;
        
X = ['two', 'three']
percentages.append(np.round(two/simulation_number*100,2))
percentages.append(np.round(three/simulation_number*100,2))
fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(X,percentages)
print(sum(percentages), '%')


#####################################################################


print('\n')


#####################################################################


import numpy as np 
import matplotlib.pyplot as plt
import seaborn as sns
import random as rd


def random_samples (population, sample_qty, sample_size):
    sample_means = []
    
    for i in range(sample_qty):
        sample = rd.sample(population, sample_size)
        sample_mean = np.array(sample).mean()
        sample_means.append(sample_mean)
    return sample_means

uniform = np.random.rand(100000)

print('mean =', np.mean(uniform))
plt.figure(figsize = (7, 4))
plt.title('Distribution', fontsize = 15)
plt.hist(uniform, 100)

samples_from_normal = random_samples(list(uniform), 10, 10)
plt.figure()
plt.title('Distribution Results', fontsize = 15)
sns.distplot(samples_from_normal, color = 'g')















